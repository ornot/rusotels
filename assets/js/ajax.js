$(document).ready(function(){
    $(".remove-all-viewed").click(function(){
        $.get('/ajax/remove_viewed');
        $('.block-viewed-hotels').remove();

        return false;
    });

    $(".remove-viewed").click(function(){
        $.get('/ajax/remove_viewed/' + $(this).data('id'));
        $(this).closest('li').remove();

        return false;
    });
});