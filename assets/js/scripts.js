$(document).ready(function() {

    setTimeout(function(){
        $('.block-results .sort').after($('<p>').text('Уважаемый турист, выбранные Вами отели еще не подключили возможность забронировать номер онлайн, но Вы можете попробовать связаться с отелем по указанным телефонам и уточнить наличие свободных мест и возможность забронировать номер на указанные даты'));
    }, 500);

    // Placeholder
    $('input[placeholder], textarea[placeholder]').placeholder();

    // Слайдер
    if ($("#slider-container").length) {
        $("#slider-container").carouFredSel({
            circular: true,
            infinite: false,
            responsive: false,
            width: 660,
            height: 436,
            auto: { play: true },
            items: { visible: 1, minimum: 1 },
            prev: {  button: '#left-slider-btn'   },
            next: {  button: '#right-slider-btn'  },
            pagination: {  container: "#slider-pager"  },
            scroll: { items: 1, fx: "crossfade", easing: "swing", duration: 500, pauseOnHover: true }
        });
    }

    if ($(".photos-wrap .thumbs").length){
        $(".photos-wrap .thumbs li img").click(function(){
            $(".big img").attr("src", $(this).data("large"));
        });

        if($(".photos-wrap .thumbs li").length > 4) {
            $(".photos-wrap .thumbs").carouFredSel({
                circular: false,
                infinite: false,
                responsive: false,
                width: 598,
                height: 78,
                auto: { play: false },
                items: { visible: 5, minimum: 1 },
                prev: {  button: '.ctrl.left'   },
                next: {  button: '.ctrl.right'  },
                pagination: {  container: "#slider-pager"  },
                scroll: { items: 2, fx: "slide", easing: "swing", duration: 700, pauseOnHover: true }
            });
        }
    }

    $('.section .label').click(function(){
        $(this).toggleClass('closed');
        $(this).parent().find('.body').toggle();
    });

    // Тень для текста в IE
    if ($.browser.msie && $.browser.version != "10.0") {
        $(".ts-bk1, .ts-bk2, .ts-bk4, .ts-gr1, .ts-wh1, .ts-grn1, .ts-grn2").textShadow();
    }

    // Левая навигация в личном кабинете
    if ($(".left-nav").length)
        $(".left-nav").accordion();


    $("select").chosen({ max_selected_options: 3 });

    $('.spinner').spinner();
    $('#number-of-rooms').spinner({ min: 1 });
    $('#number-of-stars').spinner({ min: 2, max: 5 });
    $('#number-of-stars').on( "spin", function( event, ui ) {
        var $stars = $(this).parent().parent().parent().find(".big-stars");
        var text = "";
        for (var i = 0; i < ui.value; i++) { text += "<i></i>"; }
        $stars.html(text);
    });

    $("form").validationEngine('attach');

    $('.block-search #date-in').datepicker({
        maxDate: $( ".block-search #date-out").val(),
        onClose: function( selectedDate ) {
            $( ".block-search #date-out" ).datepicker( "option", "minDate", selectedDate );
        }
    }).datepicker($.datepicker.regional[ "ru" ]);

    $('.block-search #date-out').datepicker({
        minDate: $( ".block-search #date-in").val(),
        onClose: function( selectedDate ) {
            $( ".block-search #date-in" ).datepicker( "option", "maxDate", selectedDate );
        }
    }).datepicker($.datepicker.regional[ "ru" ]);

    $('.block-search .search').typeahead({
        source: function (query, process) {
            return $.getJSON( '/ajax/search', { query: query }, function (data) {
                return process(data);
            });
        }
    });
    
    // валидация rusotels.ru/signup (поле "*Наименование:"   name="hotel[title]")
    $("#signup_hotel_title").keyup(function (e)  
    {  
        str=this.value;
        if ( /[^a-zа-яA-ZА-Я0-9 $ _ ё Ё -]/.test(str) ) {
             alert('В названии отеля разрешено вводить только буквы, цифры, пробелы, тире и символ подчёркивания!');
        }
    });

    $('td.name .icon-question-sign').popover({
        html: true,
        trigger: 'hover',
        placement: 'right',
        delay: { show: 0, hide: 3000 }
    });

});