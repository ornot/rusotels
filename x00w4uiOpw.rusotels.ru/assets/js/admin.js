$(document).ready(function(){
    $('.region-filter').change(function() {
        $.getJSON('/ajax/city/' + $(this).val(), function(data){
            $('.city-filter').html('').append($('<option>').text('Любой город').val(0));
            $(data).each(function(index, elem) {
                $('.city-filter').append($('<option>').text(elem.name).val(elem.id));
            });
        });
    }).change();
    
    $('.m_check_all').click(function(event) {
       
        if (this.checked) {
            $('.m_checkbox').each(function() {
                this.checked = true;
            });
        }
        else {
            $('.m_checkbox').each(function() {
                this.checked = false;
            });
        }
    });
    return true;
    
});