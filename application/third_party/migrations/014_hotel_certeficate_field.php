<?php

class Migration_Hotel_Certeficate_Field extends CI_Migration {
    public function up() {
        $this->load->dbforge();

        $fields = array(
            "certificate" => array("type" => "VARCHAR(255)"),
        );

        $this->dbforge->add_column('hotels', $fields);
    }

    public function down() {
        $this->load->dbforge();

        $this->dbforge->drop_column('hotels', 'certificate');
    }
}