<?php

class Migration_Notice_Date_Field extends CI_Migration {
    public function up() {
        $this->load->dbforge();

        $fields = array(
            "created" => array("type" => "INT"),
            "processed" => array("type" => "INT"),
        );

        $this->dbforge->add_column('notices', $fields);
    }

    public function down() {
        $this->load->dbforge();

        $this->dbforge->drop_column('notices', 'created');
        $this->dbforge->drop_column('notices', 'processed');
    }
}