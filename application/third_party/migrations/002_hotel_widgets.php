<?php

class Migration_Hotel_Widgets extends CI_Migration {
    public function up() {
        $this->load->dbforge();
        $columns = array(
            'rooms_widget' => array('type' => 'TEXT'),
            'book_widget' => array('type' => 'TEXT'),
            'revoke_widget' => array('type' => 'TEXT'),
        );

        $this->dbforge->add_column('hotels', $columns);
    }

    public function down() {
        $this->load->dbforge();
        $this->dbforge->drop_column('hotels', 'rooms_widget');
        $this->dbforge->drop_column('hotels', 'book_widget');
        $this->dbforge->drop_column('hotels', 'revoke_widget');
    }
}