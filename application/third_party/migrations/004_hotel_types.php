<?php

class Migration_Hotel_Types extends CI_Migration {
    public function up() {
        $valid_types = array(
            "гостиничный комплекс",
            "мини-отель",
            "санаторий",
            "хостел",
            "бизнес-отель",
            "пансион",
            "квартирная гостиница",
            "загородный отель",
            "котеджный комплекс",
            "cпа и оздоровительные комплексы",
        );


        $this->load->dbforge();

        $fields = array(
            "id" => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            "type" => array(
                'type' => "VARCHAR",
                'constraint' => 100,
            ),
            "active" => array(
                'type' => 'INT',
                'constraint' => 1,
                'unsigned' => TRUE,
            ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table("hotel_types");

        $this->db->distinct();
        $this->db->select('type');
        $query = $this->db->get('hotels');

        foreach($query->result() as $row) {
            $pos = array_search(mb_strtolower($row->type), $valid_types);

            if($pos !== false){
                unset($valid_types[$pos]);
                $active = 1;
            } else {
                $active = 0;
            }

            $data = array(
                'type' =>  mb_strtolower($row->type),
                'active' => $active,
            );

            $this->db->insert('hotel_types', $data);
            $id = $this->db->insert_id();

            $this->db->update('hotels', array("type" => $id,  "published" => $active), "type = '$row->type'");
        }

        foreach($valid_types as $type) {
            $data = array(
                'type' =>  mb_strtolower($type),
                'active' => 1,
            );

            $this->db->insert('hotel_types', $data);
        }
    }

    public function down() {
        $this->load->dbforge();

        $query = $this->db->get('hotel_types');
        foreach($query->result() as $row) {
            $this->db->update('hotels', array("type" => $row->type), "type = " . $row->id);
        }

        $this->dbforge->drop_table('hotel_types');
    }
}