<?php

class Migration_Hotel_Photos extends CI_Migration {
    public function up() {

        $this->load->dbforge();
        $fields = array(
            "id" => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            "hotel" => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
            ),
            "photo" => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
            ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table("hotel_photos");

        foreach($this->hotel->findAll() as $hotel) {
            for($i = 1; $i < 6; $i++) {
                if($hotel["photo_" . $i] != "") {
                    $data = array(
                        'photo' =>  $hotel["photo_" . $i],
                        'hotel' => $hotel["id"],
                    );

                    $this->db->insert("hotel_photos", $data);
                }
            }
        }

        $file = fopen("hotels.json", "r");

        while($str = fgets($file)) {
            $data = json_decode($str);

            if(isset($data->city) && isset($data->title) && isset($data->photos)) {

                $hotel = $this->hotel->find(array(
                    'city_id' => $data->city,
                    '%email' => $data->email,
                ));

                if(isset($hotel["id"])) {
                    foreach($data->photos as $photo) {

                        preg_match_all("/[0-9]+/", $photo, $parts);

                        if(count($parts[0]) == 2) {

                            $name = $parts[0][0] . "_" . $parts[0][1] . ".jpg";

                            $data = array(
                                'photo' =>  $name,
                                'hotel' => $hotel["id"],
                            );

                            $this->db->insert("hotel_photos", $data);
                        }
                    }
                }
            }
        }

        fclose($file);
    }

    public function down() {
        $this->load->dbforge();

        $this->dbforge->drop_table("hotel_photos");
    }
}