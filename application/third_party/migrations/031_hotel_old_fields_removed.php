<?php

class Migration_Hotel_Old_Fields_Removed extends CI_Migration {
    public function up() {
        $this->load->dbforge();

        $this->dbforge->drop_column('hotels', 'title');
        $this->dbforge->drop_column('hotels', 'content');
        $this->dbforge->drop_column('hotels', 'address');
        $this->dbforge->drop_column('hotels', 'email');
        $this->dbforge->drop_column('hotels', 'skype');
        $this->dbforge->drop_column('hotels', 'phone');
        $this->dbforge->drop_column('hotels', 'site');
        $this->dbforge->drop_column('hotels', 'room_count');
        $this->dbforge->drop_column('hotels', 'price_min');
        $this->dbforge->drop_column('hotels', 'price_max');
    }

    public function down() {
        $this->load->dbforge();

        $fields = array(
            "title" => array(
                "type" => "VARCHAR(255)",
            ),
            "content" => array(
                "type" => "TEXT",
            ),
            "address" => array(
                "type" => "VARCHAR(255)",
            ),
            "email" => array(
                "type" => "VARCHAR(255)",
            ),
            "skype" => array(
                "type" => "VARCHAR(255)",
            ),
            "phone" => array(
                "type" => "VARCHAR(255)",
            ),
            "site" => array(
                "type" => "VARCHAR(255)",
            ),
            "room_count" => array(
                "type" => "INT",
            ),
            "price_min" => array(
                "type" => "INT",
            ),
            "price_max" => array(
                "type" => "INT",
            )
        );

        $this->dbforge->add_column('hotels', $fields);

        $revisions = $this->hotel_revision->findAll(array('active' => 1));
        foreach($revisions as $revision) {
            $data = array(
                "title"      => $revision['title'],
                "content"    => $revision['content'],
                "address"    => $revision['address'],
                "email"      => $revision['email'],
                "skype"      => $revision['skype'],
                "phone"      => $revision['phone'],
                "site"       => $revision['site'],
                "room_count" => $revision['room_count'],
                "price_min"  => $revision['price_min'],
                "price_max"  => $revision['price_max'],
            );

            $this->db->where('id', $revision['hotel']);
            $this->db->update($data, 'hotels');
        }
    }
}