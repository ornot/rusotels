<?php

class Migration_Hotel_Is_Client_Field extends CI_Migration {
    public function up() {
        $this->load->dbforge();

        $fields = array(
            "is_client" => array("type" => "INT"),
        );

        $this->dbforge->add_column('hotels', $fields);
    }

    public function down() {
        $this->load->dbforge();

        $this->dbforge->drop_column('hotels', 'is_client');
    }
}