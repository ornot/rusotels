<?php

class Migration_Users_Dates_Fields extends CI_Migration {
    public function up() {
        $this->load->dbforge();

        $fields = array(
            "activated" => array("type" => "INT"),
            "last_active" => array("type" => "INT"),
        );

        $this->dbforge->add_column('users', $fields);
    }

    public function down() {
        $this->load->dbforge();

        $this->dbforge->drop_column('users', 'activated');
        $this->dbforge->drop_column('users', 'last_active');
    }
}