<?php

class Migration_City_Major_Field_Fix extends CI_Migration {
    public function up() {
        $this->load->dbforge();

        $fields = array(
            "major" => array("type" => "INT", "default" => 0),
        );

        $this->dbforge->modify_column('citys', $fields);
    }

    public function down() {

    }
}