<?php

class Migration_Messages_Table extends CI_Migration {
    public function up() {
        $this->load->dbforge();
        
        $fields = array(
            "id" => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            "user" => array(
                'type' => "INT",
                'constraint' => 10,
            ),
            "type" => array(
                'type' => "INT",
                'constraint' => 3,
            ),
            "message" => array(
                'type' => "TEXT",
            ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table("messages");

    }

    public function down() {
        $this->load->dbforge();
        $this->dbforge->drop_table('messages');
    }
}