<?php

class Migration_Hotel_Terms extends CI_Migration {
    public function up() {
        $this->load->dbforge();
        $columns = array(
            'terms' => array('type' => 'TEXT'),
        );

        $this->dbforge->add_column('hotels', $columns);
    }

    public function down() {
        $this->load->dbforge();
        $this->dbforge->drop_column('hotels', 'terms');
    }
}