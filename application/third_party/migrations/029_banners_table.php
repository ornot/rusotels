<?php

class Migration_Banners_Table extends CI_Migration {
    public function up() {
        $this->load->dbforge();
        
        $fields = array(
            "id" => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            "hotel" => array(
                'type' => "INT",
                'constraint' => 10,
            ),
            "active" => array(
                'type' => "INT"
            ),
            "type" => array(
                'type' => "INT"
            ),
            "banner" => array(
                'type' => "VARCHAR(255)"
            )
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table("banners");

    }

    public function down() {
        $this->load->dbforge();
        $this->dbforge->drop_table('banners');
    }
}