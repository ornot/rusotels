<?php

class Migration_Hotel_Photo_Status_Field extends CI_Migration {
    public function up() {
        $this->load->dbforge();

        $fields = array(
            "status" => array("type" => "INT"),
        );

        $this->dbforge->add_column('hotel_photos', $fields);
    }

    public function down() {
        $this->load->dbforge();

        $this->dbforge->drop_column('hotel_photos', 'status');
    }
}