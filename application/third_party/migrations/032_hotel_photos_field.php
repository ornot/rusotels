<?php

class Migration_Hotel_Photos_Field extends CI_Migration {
    public function up() {
        $this->load->dbforge();

        $fields = array(
            "photo_count" => array("type" => "INT", "constraint" => 0),
        );

        $this->dbforge->add_column('hotels', $fields);

        $hotels = $this->hotel->findAll();
        foreach($hotels as $hotel) {
            $h = new Hotel();
            $h->id = $hotel['id'];
            $h->photo_count = $this->hotel_photo->count(array('hotel' => $hotel['id']));
            $h->save(1);
        }
    }

    public function down() {
        $this->load->dbforge();

        $this->dbforge->drop_column('hotels', 'photo_count');
    }
}