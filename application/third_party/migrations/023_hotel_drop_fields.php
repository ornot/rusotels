<?php

class Migration_Hotel_Drop_Fields extends CI_Migration {
    public function up() {
        $this->load->dbforge();

        for($i = 1; $i < 41; $i++) {
            if ($i < 10) {
                $preffix = "0";
            } else {
                $preffix = "";
            }

            $this->dbforge->drop_column('hotels', 'service_'.$preffix.$i);
        }

        for($i = 1; $i < 6; $i++) {
            $this->dbforge->drop_column('hotels', 'photo_'.$i);
        }

        $this->dbforge->drop_column('hotels', 'offer_date_active');
        $this->dbforge->drop_column('hotels', 'offer_short');
        $this->dbforge->drop_column('hotels', 'offer_long');
        $this->dbforge->drop_column('hotels', 'offer_image');
        $this->dbforge->drop_column('hotels', 'pay_date_active');
    }

    public function down() {
        $this->load->dbforge();

        $fields[] = array(
            'offer_date_active' => array('type' => "INT"),
            'offer_short' => array('type' => "TEXT"),
            'offer_long' => array('type' => "TEXT"),
            'offer_image' => array('type' => "VARCHAR(255)"),
            'pay_date_active' => array('type' => "INT"),
        );

        for($i = 1; $i < 41; $i++) {
            if ($i < 10) {
                $preffix = "0";
            } else {
                $preffix = "";
            }

            $fields[] = array (
                'service_'.$preffix.$i => array("type" => "INT"),
            );
        }

        for($i = 1; $i < 6; $i++) {
            $fields[] = array (
                'photo_'.$i => array("type" => "VARCHAR(255)"),
            );
        }

        $this->dbforge->add_column('hotels', $fields);
    }
}