<?php

class Migration_Hotels_Revision_Table extends CI_Migration {
    public function up() {
        $this->load->dbforge();
        
        $fields = array(
            "id" => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            "date" => array(
                "type" => "INT",
            ),
            "active" => array(
                "type" => "INT(1)",
            ),
            "hotel" => array(
                'type' => "INT",
                'constraint' => 10,
            ),
            "title" => array(
                "type" => "VARCHAR(255)",
            ),
            "content" => array(
                "type" => "TEXT",
            ),
            "address" => array(
                "type" => "VARCHAR(255)",
            ),
            "email" => array(
                "type" => "VARCHAR(255)",
            ),
            "skype" => array(
                "type" => "VARCHAR(255)",
            ),
            "phone" => array(
                "type" => "VARCHAR(255)",
            ),
            "site" => array(
                "type" => "VARCHAR(255)",
            ),
            "room_count" => array(
                "type" => "INT",
            ),
            "price_min" => array(
                "type" => "INT",
            ),
            "price_max" => array(
                "type" => "INT",
            )

        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table("hotel_revisions");

        foreach($this->hotel->findAll() as $hotel) {
            $data = array(
                "date"       => time(),
                "active"     => 1,
                "hotel"      => $hotel['id'],
                "title"      => $hotel['title'],
                "content"    => $hotel['content'],
                "address"    => $hotel['address'],
                "email"      => $hotel['email'],
                "skype"      => $hotel['skype'],
                "phone"      => $hotel['phone'],
                "site"       => $hotel['site'],
                "room_count" => $hotel['room_count'],
                "price_min"  => $hotel['price_min'],
                "price_max"  => $hotel['price_max'],
            );

            $this->db->insert('hotel_revisions', $data);
        }

    }

    public function down() {
        $this->load->dbforge();
        $this->dbforge->drop_table('hotel_revisions');
    }
}