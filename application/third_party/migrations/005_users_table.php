<?php

class Migration_Users_Table extends CI_Migration {
    public function up() {
        $this->load->dbforge();

        $this->dbforge->drop_table('users');
        
        $fields = array(
            "id" => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            "email" => array(
                'type' => "VARCHAR",
                'constraint' => 100,
            ),
            "password" => array(
                'type' => "VARCHAR",
                'constraint' => 32,
            ),
            "role" => array(
                'type' => "VARCHAR",
                'constraint' => 32,
            ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table("users");

        $admin_data = array(
            "email" => "admin@rusotels.ru",
            "password" => "7661bae50a0b63778e3f9f10ad9b1a32",
            "role" => "admin",
        );

        $this->db->insert('users', $admin_data);

    }

    public function down() {
        $this->load->dbforge();
        $this->dbforge->drop_table('users');
    }
}