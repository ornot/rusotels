<?php

class Migration_Hotel_Description_Field extends CI_Migration {
    public function up() {
        $this->load->dbforge();

        $fields = array(
            "description" => array("type" => "TEXT"),
        );

        $this->dbforge->add_column('hotels', $fields);
    }

    public function down() {
        $this->load->dbforge();
        $this->dbforge->drop_column('hotels', 'description');
    }
}