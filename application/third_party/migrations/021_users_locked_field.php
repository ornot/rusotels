<?php

class Migration_Users_Locked_Field extends CI_Migration {
    public function up() {
        $this->load->dbforge();

        $fields = array(
            "lock" => array("type" => "INT"),
        );

        $this->dbforge->add_column('users', $fields);
    }

    public function down() {
        $this->load->dbforge();

        $this->dbforge->drop_column('users', 'lock');
    }
}