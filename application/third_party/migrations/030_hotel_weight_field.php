<?php

class Migration_Hotel_Weight_Field extends CI_Migration {
    public function up() {
        $this->load->dbforge();

        $fields = array(
            "weight" => array("type" => "INT", "default" => 0),
        );

        $this->dbforge->add_column('hotels', $fields);
    }

    public function down() {
        $this->load->dbforge();

        $this->dbforge->drop_column('hotels', 'weight');
    }
}