<?php

class Migration_Hotel_Title_Field extends CI_Migration {
    public function up() {
        $this->load->dbforge();

        $fields = array(
            "meta_title" => array("type" => "TEXT"),
        );

        $this->dbforge->add_column('hotels', $fields);
    }

    public function down() {
        $this->load->dbforge();

        $this->dbforge->drop_column('hotels', 'meta_title');
    }
}