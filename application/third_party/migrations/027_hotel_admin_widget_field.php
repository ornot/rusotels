<?php

class Migration_Hotel_Admin_Widget_Field extends CI_Migration {
    public function up() {
        $this->load->dbforge();

        $fields = array(
            "admin_widget" => array("type" => "TEXT"),
        );

        $this->dbforge->add_column('hotels', $fields);
    }

    public function down() {
        $this->load->dbforge();

        $this->dbforge->drop_collumn('hotels', 'admin_widget');
    }
}