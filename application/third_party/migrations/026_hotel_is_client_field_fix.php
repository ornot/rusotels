<?php

class Migration_Hotel_Is_Client_Field_Fix extends CI_Migration {
    public function up() {
        $this->load->dbforge();

        $fields = array(
            "is_client" => array("type" => "INT", "default" => 0),
        );

        $this->dbforge->modify_column('hotels', $fields);
    }

    public function down() {

    }
}