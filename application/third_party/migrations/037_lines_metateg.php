<?php

class Migration_Lines_Metateg extends CI_Migration {
    public function up() {
        $this->db->query(
        "
              CREATE TABLE IF NOT EXISTS `metateg_lines` (
              `id` int(11) NOT NULL auto_increment,
              `page` varchar(50) collate utf8_unicode_ci NOT NULL,
              `info` varchar(255) collate utf8_unicode_ci NOT NULL,
              `title` text NOT NULL,
              `description` text NOT NULL,
              `keywords` text NOT NULL,
              PRIMARY KEY  (`id`)
            ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2515 ;
        ");  
        
        $this->db->query(
        "
            INSERT INTO `metateg_lines` (`id`, `page`,`info`, `title`, `description`, `keywords`) VALUES
            (1,'main','главная страница (без шаблонов)','','',''),
            (2,'catalog','страница отеля ( {москва} {москвы} {москве}   {гостиница} {гостинице} {моей_гостиницы} {мои_гостиницы} {гостиницу}   {имя})','','',''),
            (3,'catalog2','страница отелей по типу ( {москва} {москвы} {москве}   {гостиница} {гостинице} {моей_гостиницы} {мои_гостиницы} {гостиницу} )','','',''),
            (4,'search','страницы формируются при поиске отелей по городу({москва} {москвы} {москве})','','','');
            
        ");
    }

    public function down() {
        $this->db->query("TRUNCATE TABLE `metateg_lines`");
        $this->db->query("DROP TABLE IF EXISTS `metateg_lines`");
    }
}