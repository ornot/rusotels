<?php

class Migration_Hotel_To_Type_Table extends CI_Migration {
    public function up() {
        $this->load->dbforge();
        
        $fields = array(
            "id" => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            "hotel" => array(
                'type' => "INT",
                'constraint' => 10,
            ),
            "type" => array(
                'type' => "INT",
                'constraint' => 10,
            )
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table("hotel_to_type");

        foreach($this->hotel->findAll() as $hotel) {
            $data = array(
                'type' => $hotel['type'],
                'hotel' => $hotel['id'],
            );

            $this->db->insert('hotel_to_type', $data);
        }

        $this->dbforge->drop_column('hotels', 'type');

    }

    public function down() {
        $this->load->dbforge();

        $fields = array(
            'type' => array('type' => 'INT')
        );

        $this->dbforge->add_column('hotels', $fields);


        foreach($this->hotel->findAll() as $hotel) {
            $query = $this->db->get_where('hotel_to_type', array('hotel' => $hotel['id']));
            $type = $query->row_array();

            $data = array(
                "type" => $type['type'],
            );

            $this->db->where('id', $hotel['id']);
            $this->db->update($data, 'hotels');
        }

        $this->dbforge->drop_table('hotel_to_type');
    }
}