<?php

class Migration_Hotel_Type_Order_Field extends CI_Migration {
    public function up() {
        $this->load->dbforge();

        $fields = array(
            "position" => array("type" => "INT"),
        );

        $this->dbforge->add_column('hotel_types', $fields);
    }

    public function down() {
        $this->load->dbforge();

        $this->dbforge->drop_column('hotel_types', 'position');
    }
}