<?php

class Migration_Requisites_Table extends CI_Migration {
    public function up() {
        $this->load->dbforge();
        
        $fields = array(
            "id" => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            "user" => array(
                'type' => 'INT',
                'constraint' => 5,
            ),
            "name" => array(
                'type' => "VARCHAR(255)",
            ),
            "addr" => array(
                'type' => "VARCHAR(255)",
            ),
            "post_addr" => array(
                'type' => "VARCHAR(255)",
            ),
            "email" => array(
                'type' => "VARCHAR(255)",
            ),
            "phone" => array(
                'type' => "VARCHAR(255)",
            ),
            "ogrn" => array(
                'type' => "VARCHAR(255)",
            ),
            "kpp" => array(
                'type' => "VARCHAR(255)",
            ),
            "inn" => array(
                'type' => "VARCHAR(255)",
            ),
            "cor_account" => array(
                'type' => "VARCHAR(255)",
            ),
            "bank_account" => array(
                'type' => "VARCHAR(255)",
            ),
            "bik" => array(
                'type' => "VARCHAR(255)",
            ),
            "bank_name" => array(
                'type' => "VARCHAR(255)",
            ),
            "position" => array(
                'type' => "VARCHAR(255)",
            ),
            "position_basement" => array(
                'type' => "VARCHAR(255)",
            )
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table("requisites");
    }

    public function down() {
        $this->load->dbforge();
        $this->dbforge->drop_table('requisites');
    }
}