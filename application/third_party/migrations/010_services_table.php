<?php

class Migration_Services_Table extends CI_Migration {
    public function up() {
        $this->load->dbforge();
        
        $fields = array(
            "id" => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            "hotel" => array(
                'type' => "INT",
                'constraint' => 10,
            ),
            "user" => array(
                'type' => "INT",
                'constraint' => 10,
            ),
            "type" => array(
                'type' => "INT",
                'constraint' => 3,
            ),
            "status" => array(
                'type' => "INT",
                'constraint' => 3,
            ),
            "start" => array(
                'type' => "INT",
            ),
            "end" => array(
                'type' => "INT",
            ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table("services");

    }

    public function down() {
        $this->load->dbforge();
        $this->dbforge->drop_table('services');
    }
}