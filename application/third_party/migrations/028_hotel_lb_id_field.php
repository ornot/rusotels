<?php

class Migration_Hotel_Lb_Id_Field extends CI_Migration {
    public function up() {
        $this->load->dbforge();

        $fields = array(
            "lb_id" => array("type" => "VARCHAR(32)"),
        );

        $this->dbforge->add_column('hotels', $fields);
    }

    public function down() {
        $this->load->dbforge();

        $this->dbforge->drop_collumn('hotels', 'lb_id');
    }
}