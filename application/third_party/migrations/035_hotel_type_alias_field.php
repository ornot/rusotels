<?php

class Migration_Hotel_Type_Alias_Field extends CI_Migration {
    public function up() {
        $this->load->dbforge();

        $fields = array(
            "alias" => array("type" => "VARCHAR(255)"),
        );

        $this->dbforge->add_column('hotel_types', $fields);

        $data = array(
            array('alias' => 'hostel', 'id' => 11),
            array('alias' => 'mini-hotel', 'id' => 16),
            array('alias' => 'zagorodniu-hotel', 'id' => 21),
            array('alias' => 'kvartirna', 'id' => 51),
            array('alias' => 'gostinichnuu-kompleks', 'id' => 61),
            array('alias' => 'sanatoriu', 'id' => 181),
            array('alias' => 'biznes-hotel', 'id' => 186),
            array('alias' => 'pansion', 'id' => 191),
            array('alias' => 'kottedgniu-kompleks', 'id' => 196),
            array('alias' => 'spa-kompleks', 'id' => 201),
            array('alias' => 'apartamentu', 'id' => 206),
            array('alias' => 'shale', 'id' => 211),
            array('alias' => 'villa', 'id' => 216),
            array('alias' => 'gostevou-dom', 'id' => 221),
            array('alias' => 'kapsylnuu-hotel', 'id' => 226),
            array('alias' => 'kyrortnuu-hotel',	'id' => 231),
            array('alias' => 'kvartira', 'id' => 236),
            array('alias' => 'detskiu-lager', 'id' => 241),
            array('alias' => 'obschegitie', 'id' => 246),
            array('alias' => 'flotel', 'id' => 251),
            array('alias' => 'dom-prestareluh', 'id' => 256),
            array('alias' => 'ysadba', 'id' => 261),
            array('alias' => 'pridorogniu-hotel', 'id' => 266),
            array('alias' => 'hotel-postel-zavtrak', 'id' => 271),
            array('alias' => 'hotel', 'id' => 276),
            array('alias' => 'mini-hotel-domashniu', 'id' => 281),
            array('alias' => 'dacha', 'id' => 286),
            array('alias' => 'baza-otduha', 'id' => 291),
            array('alias' => 'sanatoriu-profilaktoriu', 'id' => 296),
            array('alias' => 'ohotnichiu-dom', 'id' => 301),
            array('alias' => 'komnata-na-vokzale', 'id' => 306),
            array('alias' => 'byngalo', 'id' => 311),
            array('alias' => 'baza-otduha-na-sezon', 'id' => 316),
            array('alias' => 'mini-hotel-restoran',	'id' => 321),
        );

        foreach($data as $up) {
            $this->db->update('hotel_types', array('alias' => $up['alias']), "id = " . $up['id']);
        }
    }

    public function down() {
        $this->load->dbforge();

        $this->dbforge->drop_column('hotel_types', 'alias');
    }
}