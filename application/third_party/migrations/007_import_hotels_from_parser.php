<?php

class Migration_Import_Hotels_From_Parser extends CI_Migration {
    public function up() {
        $file = fopen("hotels.json", "r");

        while($str = fgets($file)) {
            $data = json_decode($str);

            $hotel = new Hotel();
            $hotel->title = isset($data->title) ? $data->title : "";
            $hotel->type = isset($data->type) ? $data->type : 0;
            $hotel->city_id = isset($data->city) ? $data->city : 0;
            $hotel->room_count = isset($data->room_count) ? $data->room_count : 0;
            $hotel->address = isset($data->address) ? $data->address : "";
            $hotel->content = isset($data->content) ? $data->content : "";

            $hotel->published = 2;

            $hotel->price_min = isset($data->price_min) ? $data->price_min : 0;
            $hotel->price_max = isset($data->price_max) ? $data->price_max : 0;

            $hotel->email = isset($data->email) ? $data->email : "";
            $hotel->phone = isset($data->phone) ? $data->phone : "";
            $hotel->skype = isset($data->skype) ? $data->skype : "";

            $hotel->save();
        }
    }

    public function down() {
        $this->load->dbforge();

        $this->db->where("published", 2);
        $this->db->delete("hotels");
    }
}