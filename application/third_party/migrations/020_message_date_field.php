<?php

class Migration_Message_Date_Field extends CI_Migration {
    public function up() {
        $this->load->dbforge();

        $fields = array(
            "date" => array("type" => "INT"),
        );

        $this->dbforge->add_column('messages', $fields);
    }

    public function down() {
        $this->load->dbforge();

        $this->dbforge->drop_column('messages', 'date');
    }
}