<?php

class Migration_Hotel_Certificate_Status_Field extends CI_Migration {
    public function up() {
        $this->load->dbforge();

        $fields = array(
            "certificate_status" => array("type" => "INT"),
        );

        $this->dbforge->add_column('hotels', $fields);
    }

    public function down() {
        $this->load->dbforge();

        $this->dbforge->drop_column('hotels', 'certificate_status');
    }
}