<?php

class Migration_User_Additional_Fields_And_Defaults extends CI_Migration {
    public function up() {
        $this->load->dbforge();

        $query = $this->db->get('users');

        $users = $query->result_array();
        $this->dbforge->drop_column('users', 'role');


        $new_columns = array(
            'status' => array('type' => 'INT', 'default' => 0),
            'name' => array('type' => 'VARCHAR', 'constraint' => '100'),
            'phone' => array('type' => 'VARCHAR', 'constraint' => '100'),
            'role' => array('type' => 'VARCHAR', 'constraint' => 32, 'default' => 'user'),
            'token' => array('type' => 'VARCHAR', 'constraint' => '32')
        );

        $this->dbforge->add_column('users', $new_columns);

        foreach($users as $user) {
            if(isset($user['role']) && $user['role'] != "") {
                $this->db->update('users', array('role' => $user['role']));
            }
        }

    }

    public function down() {
        $this->load->dbforge();

        $query = $this->db->get('users');

        $users = $query->result_array();
        $this->dbforge->drop_column('users', 'role');

        $restore_columns = array(
            'role' => array('type' => 'VARCHAR', 'constraint' => 32),
        );

        $this->dbforge->add_column('users', $restore_columns);

        foreach($users as $user) {
            if(isset($user['role']) && $user['role'] != "") {
                $this->db->update('users', array('role' => $user['role']));
            }
        }

        $this->dbforge->drop_column('users', 'status');
        $this->dbforge->drop_column('users', 'name');
        $this->dbforge->drop_column('users', 'phone');
    }
}