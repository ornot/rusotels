<?php

class RO_Model extends CI_Model {

    protected $_data_source = null;
    protected $_table;
    protected $_rules;
    protected $_data = array();
    protected $_key = "id";

    public function __construct() {
        parent::__construct();

        $this->_rules = $this->rules();
    }

    public function __set($name, $value) {
        $this->_data[$name] = $value;
    }

    protected function rules() {
        return array();
    }

    public function find($conditions = array(), $limit = array(), $sort = array('id', 'desc')) {
        $this->addConditions($conditions);

        if(isset($limit['count'])) {
            $this->db->limit($limit['count'], $limit['start']);
        }

        $this->db->order_by($sort[0], $sort[1]);

        $query = $this->db->get($this->_table);

        return $query->row_array();
    }

    public function count($conditions = array()) {
        $this->addConditions($conditions);
        $this->db->from($this->_table);

        return $this->db->count_all_results();
    }

    public function findAll($conditions = array(), $limit = array(), $sort = array('id', 'desc')) {
        $this->addConditions($conditions);

        if(isset($limit['count'])) {
            $this->db->limit($limit['count'], $limit['start']);
        }

        if(is_array($sort)) {
            $this->db->order_by($sort[0], $sort[1]);
        } else {
            $this->db->order_by($sort);
        }

        $query = $this->db->get($this->_table);

        return $query->result_array();
    }

    public function setData($key = null) {
        if($key) {
            $this->_data_source = $key;
        }
    }

    public function validate($key, $scenario = false) {
        if(is_array($this->_rules)) {
            $rules = array();
            if(!$scenario || !isset($this->_rules[$scenario])){
                $scenario = reset(array_keys($this->_rules));
            }

            foreach($this->_rules[$scenario] as $name => $rule) {
                $rules[] = array(
                    "field" => $key . "[" . $name . "]",
                    "label" => $rule["label"],
                    "rules" => isset($rule["rules"]) ? $rule["rules"] : array(),
                );

                if(isset($rule['message'])) {
                    foreach($rule['message'] as $item => $message) {
                        $this->form_validation->set_message($item, $message);
                    }
                }
            }

            $this->form_validation->set_rules($rules);
            if($this->form_validation->run()) {
                foreach($this->_rules[$scenario] as $name => $rule) {
                    if(!isset($rule["ignore"]) || $rule["ignore"] != true) {
                        if(!isset($rule["null"]) || (!$rule["null"] && $_POST[$key][$name]) ) {
                            $value = $_POST[$key][$name];
                            if(isset($rule["modify"]) && function_exists($rule["modify"])) {
                                $value = call_user_func($rule["modify"], $value);
                            }

                            $this->_data[$name] = $value;
                        }
                    }
                }
                return true;
            }
        }

        return false;

    }

    public function save() {
        foreach($this->_data as $field => $value) {
            $this->db->set($field, $value);
        }

        if(isset($this->_data[$this->_key])) {
            $this->db->where($this->_key, $this->_data[$this->_key]);
            $this->db->update($this->_table);

            return $this->_data[$this->_key];
        } else {
            if($this->db->insert($this->_table)){
                return $this->db->insert_id();
            }
        }

        return false;
    }

    public function remove($id) {
        $this->db->delete($this->_table, array('id' => $id));
    }

    private function addConditions($conditions = array()) {
        foreach($conditions as $field => $value) {
            $action = "";
            if(is_array($value) && isset($value['#op'])) {
                $action = $value['#op'] . "_";
                unset($value['#op']);
            }

            switch($field[0]) {
                case "%":
                    $action .= "like";
                    $this->db->{$action}(substr($field, 1), $value);
                    break;
                case "@":
                    $action .= "where_in";
                    $this->db->{$action}(substr($field, 1), $value);
                    break;
                default:
                    $action .= "where";
                    $this->db->{$action}($field, $value);
            }
        }
    }
}