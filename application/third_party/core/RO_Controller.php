<?php

/**
 * Class RO_Controller
 *
 * @property Notice notice
 * @property Hotel hotel
 * @property Service service
 * @property City city
 * @property Region region
 */
class RO_Controller extends CI_Controller {
    protected $layout = 'layout/main';
    public $rules = array();

    public $title = "";
    public $keywords = "";
    public $description = "";
    public $breadcrumbs = array();

    public function __construct() {
        parent::__construct();

        session_start();

        $this->rules = array_merge($this->rules, $this->setRules());
    }

    public function setRules() {
        return array();
    }

    public function render($view, $data = array()) {
        $regions['content'] = $this->load->view($view, $data, true);

        $this->load->view($this->layout, $regions);
    }
}