<?php

class RO_ACL {

    public function check() {
        $CI =& get_instance();

        $action = $CI->router->fetch_method();

        if( ( isset($CI->rules['*']) && !$CI->user->hasRole($CI->rules['*']) )
            || ( isset($CI->rules[$action]) && !$CI->user->hasRole($CI->rules[$action]) ) ){
            redirect($CI->rules['#fallback']);
        }
    }
}