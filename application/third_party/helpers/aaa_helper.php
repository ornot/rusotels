<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


//processing_helper


		//$root_url = $_SERVER['DOCUMENT_ROOT']; 
// ------------------------------------------------------------------------



if ( ! function_exists('root_url'))
{
	function root_url()
	{
		//$url = '/Applications/MAMP/htdocs';
		$url = '/home/p/paulinarus';
		return $url;
	}
}

if ( ! function_exists('cp_path'))
{
	function cp_path()
	{
		//$url = 'cp';
		$url = 'x00w4uiOpw.rusotels.ru/public_html';
		return $url;
	}
}

if ( ! function_exists('edu_path'))
{
	function edu_path()
	{
		//$url = 'edu';
		$url = 'edu.rusotels.ru/public_html';
		return $url;
	}
}

if ( ! function_exists('catalog_path'))
{
	function catalog_path()
	{
		//$url = 'ro';
		$url = 'rusotels.ru/public_html';
		return $url;
	}
}

if ( ! function_exists('edu_url'))
{
	function edu_url()
	{
		$url = 'http://edu.rusotels.ru/';
		//$url = 'http://localhost:8888/edu/';
		return $url;
	}
}

if ( ! function_exists('catalog_url'))
{
	function catalog_url()
	{
		$url = 'http://rusotels.ru/';
		//$url = 'http://localhost:8888/ro/';
		return $url;
	}
}

if ( ! function_exists('cp_url'))
{
	function cp_url()
	{
		$url = 'http://x00w4uiOpw.rusotels.ru/';
		//$url = 'http://localhost:8888/cp/';
		return $url;
	}
}









if ( ! function_exists('tot'))
{
	function tot($date_in = "0000-00-00")
	{
		$date_start=date('d.m.Y', strtotime($date_in));
		return $date_start;
	}
}




if ( ! function_exists('db2form'))
{
	function db2form($str = "")
	{
		$return_str = str_replace("<p>", '', $str);
		$return_str = str_replace("</p>", '', $return_str);

		$return_str = str_replace("<strong>", '', $return_str);
		$return_str = str_replace("</strong>", '', $return_str);

		$return_str = preg_replace('=<br */?>=i', "", $return_str); //БР в интер
		$return_str = htmlspecialchars($return_str, ENT_QUOTES); //ковычки и тп в спецсимволы


		return $return_str;
	}
}




if ( ! function_exists('form2db'))
{
	function form2db($str = "")
	{
		//$str = htmlspecialchars($str, ENT_QUOTES); //ковычки и тп в спецсимволы
		$str = nl2br($str);
		return $str;
	}
}




if ( ! function_exists('br2nl'))
{
	function br2nl($str = "")
	{
		return preg_replace('=<br */?>=i', "\n", $str);
	}
}





if ( ! function_exists('format_alias'))
{
	function format_alias($alias = "")
	{
        $alias = str_replace("  ", " ", $alias);
        $alias = str_replace(" ", "-", $alias);
        $alias = str_replace("(", "", $alias);
        $alias = str_replace(")", "", $alias);
        $alias = str_replace("_", "-", $alias);
        $alias = str_replace("'", "", $alias);
        $alias = str_replace("\"", "", $alias);
        $alias = str_replace("&", "and", $alias);
        $alias = str_replace("$", "", $alias);
        $alias = str_replace(".", "", $alias);
        $alias = str_replace(",", "", $alias);
        $alias = str_replace(";", "", $alias);
        $alias = str_replace(":", "", $alias);
        $alias = str_replace("?", "", $alias);
        $alias = str_replace("/", "", $alias);

        $alias = str_replace("----", "-", $alias);
        $alias = str_replace("---", "-", $alias);
        $alias = str_replace("--", "-", $alias);

		return $alias;
	}
}




if ( ! function_exists('format_site'))
{
	function format_site($site = "")
	{
        $site = str_replace("https://", "", $site);
        $site = str_replace("http://", "", $site);

		return $site;
	}
}





if ( ! function_exists('visual_published'))
{
	function visual_published($published = "0")
	{
		if ($published == 1)
		{
			$html = "<span class='label label-success'>Вкл.</span>";
		}
		else
		{
			$html = "<span class='label label-important'>Выкл.</span>";
		}

		return $html;
	}
}


if ( ! function_exists('visual_photo'))
{
	function visual_photo($published = "")
	{
		if ($published != '')
		{
			$html = "<div style='text-align: right'><span class='label label-success'>Есть</span></div>";
		}
		else
		{
			$html = "<div style='text-align: right'><span class='label label-important'>Нет</span></div>";
		}

		return $html;
	}
}




/* End of file string_helper.php */
/* Location: ./system/helpers/string_helper.php */