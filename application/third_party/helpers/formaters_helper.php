<?php


if (!function_exists('f_phone')) {
    function f_phone($string) {
        $chunks = explode('|', $string);
        if(count($chunks) == 2) {
            return "+7 (" . $chunks[0] . ") " . $chunks[1];
        } else {
            return $string;
        }
    }
}

if (!function_exists('f_addr')) {
    function f_addr($string) {
        $chunks = explode('|', $string);
        if(count($chunks) > 1) {
            $result = "ул." . $chunks[0] . " д." . $chunks[1];
            if(isset($chunks[2]) && trim($chunks[2]) != "") {
                $result .= " стр." . $chunks[2];
            }
            return $result;
        } else {
            return $string;
        }
    }
}

if (!function_exists('or_empty')) {
    function or_empty(&$var) {
        return isset($var) ? $var : "";
    }
}

if (!function_exists('l_notice')) {
    function l_notice($code) {
        $type = "info";
        $text = "еще одно скучное проишествие";

        if($code > 99 && $code < 200) {
            $type = "warning";

            switch($code) {
                case 101: $text = "подключить букинг"; break;
                case 102: $text = "карточка отеля изменена"; break;
                case 103: $text = "добавлен сертификат"; break;
                case 104: $text = "добавлено фото"; break;
                case 109: $text = "поднять отель"; break;
                case 110: $text = "разместить баннер"; break;
            }
        }

        if($code > 199 && $code < 300) {
            $type = "success";

            switch($code) {
                case 201: $text = "новый отель"; break;
            }
        }

        return "<span class='label label-" . $type . "'>" . $text . "</span>";
    }
}

if (!function_exists('l_service')) {
    function l_service($type) {
        switch($type) {
            case 1:  $text = "Система управления бронями"; break;
            case 10: $text = "Размещение баннера"; break;
        }

        return $text;
    }
}

if (!function_exists('l_service_status')) {
    function l_service_status($type) {
        switch($type) {
            case 0: $text = "<span class='label'>Архив</span>"; break;
            case 1: $text = "<span class='label label-success'>Активна</span>"; break;
            case 2: $text = "<span class='label label-warning'>В очереди</span>"; break;
        }

        return $text;
    }
}