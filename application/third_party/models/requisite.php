<?php

class Requisite extends RO_Model {

    protected $_table = "requisites";

    protected function rules() {
        return array(
            "default" => array(
                "name" => array('label' => "Название организации"),
                "addr" => array('label' => "Юридический адрес"),
                "post_addr" => array('label' => "Почтовый адрес"),
                "email" => array('label' => "Адрес электронной почты"),
                "phone" => array('label' => "Телефон"),
                "ogrn" => array('label' => "ОГРН", 'rules' => 'integer'),
                "kpp" => array('label' => "КПП", 'rules' => 'integer'),
                "inn" => array('label' => "ИНН", 'rules' => 'integer'),
                "cor_account" => array('label' => "Корреспондентский счет", 'rules' => 'integer'),
                "bank_account" => array('label' => "Р/С", 'rules' => 'integer'),
                "bik" => array('label' => "БИК", 'rules' => 'integer'),
                "bank_name" => array('label' => "Наименование банка"),
                "position" => array('label' => "Должность лица, подписывающего договор от имени Организатора"),
                "position_basement" => array('label' => "Основания полномочий лица, подписывающего договор")
            ),
        );
    }
}