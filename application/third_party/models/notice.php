<?php

class Notice extends RO_Model {

    protected $_table = "notices";


    public function save(){
        if($this->_data['status'] != 0) {
            $hotel = $this->hotel->find(array('id' => $this->_data['hotel']));

            $this->email->clear();

            $this->email->to(array("skorbenko@digitalwill.ru", "support@rusotels.ru", "ornot.work@gmail.com","ribets@digitalwill.ru"));//рабочий
            //$this->email->to(array("ribets@digitalwill.ru"));//тестовый
            $this->email->from('system@rusotels.ru');
            $this->email->subject('Новый запрос на Rusotels');


            switch($this->_data['type']) {
                case 201: $this->email->message("Зарегистрирован новый отель " . $hotel['title']); break;
                case 101: $this->email->message("Получен новый запрос на подключение букинга для отеля " . $hotel['title']); break;
                case 102: $this->email->message("Отель " . $hotel['title'] . " изменил данные"); break;
                case 103: $this->email->message("Отель " . $hotel['title'] . " добавил сертификат"); break;
                case 104: $this->email->message("Отель " . $hotel['title'] . " добавил фото"); break;
                case 109: $this->email->message("Получен новый запрос на поднятие отеля в каталоге - " . $hotel['title']); break;
                case 110: $this->email->message("Получен новый запрос на размещение баннера от отеля" . $hotel['title']); break;
            }

            $this->email->send();
        }

        parent::save();
    }
}