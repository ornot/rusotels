<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Metateg_model extends CI_Model { 
    
    const TABLE_CITYS = 'metateg_citys';
    const TABLE_TYPE = 'hotel_types';
    const TABLE_LINES = 'metateg_lines';

    //кого?  Москвы (если города нет в списке то без изменений) "лучьшие номера Москвы"
     public function genitive_city($city_name)
    {
        $this->db->limit(1);
        $this->db->where('name', $city_name);
        $qw = $this->db->get(self::TABLE_CITYS);
        $arr = $qw->result_array();
        if (count($arr) > 0) {
            return $arr[0]['genitiveСase'];
        }
        return $city_name;
    }
    
    // где? в Москве  (если города нет в списке то без изменений) "снять комнату в Москве"
      public function prepositional_city($city_name)
    {
        $this->db->limit(1);
        $this->db->where('name', $city_name);
        $qw = $this->db->get(self::TABLE_CITYS);
        $arr = $qw->result_array();
        if (count($arr) > 0) {
            return $arr[0]['prepositionalCase'];
        }
        return $city_name;
    }
    
    //кого?  Отеля  "лучьшие номера Отеля"
     public function genitive_type($type)
    {
        $this->db->limit(1);
        $this->db->where('type', $type);
        $qw = $this->db->get(self::TABLE_TYPE);
        $arr = $qw->result_array();
        if (count($arr) > 0) {
            return $arr[0]['genitive'];
        }
        return $type;
    }
    
    // где? в Отеле   "снять комнату в Отеле"
      public function prepositional_type($type)
    {
        $this->db->limit(1);
        $this->db->where('type', $type);
        $qw = $this->db->get(self::TABLE_TYPE);
        $arr = $qw->result_array();
        if (count($arr) > 0) {
            return $arr[0]['prepositional'];
        }
        return $type;
    }
    // множественное число  "лучшие отели москвы"
      public function multiply_type($type)
    {
        $this->db->limit(1);
        $this->db->where('type', $type);
        $qw = $this->db->get(self::TABLE_TYPE);
        $arr = $qw->result_array();
        if (count($arr) > 0) {
            return $arr[0]['multiply'];
        }
        return $type;
    }
    
      // Венительный падеж Кого? Что?  число  "Снять гостиницу"
      public function accusative_type($type)
    {
        $this->db->limit(1);
        $this->db->where('type', $type);
        $qw = $this->db->get(self::TABLE_TYPE);
        $arr = $qw->result_array();
        if (count($arr) > 0) {
            return $arr[0]['accusative'];
        }
        return $type;
    }
    
    
    // вставляем данные по id
    public function edit($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update(self::TABLE_LINES, $data);
    }
    
    // возвращает все наборы метатегов
     public function get_all_meta()
    {  
        //$this->db->order_by('page', 'desc');
        $qw = $this->db->get(self::TABLE_LINES);
        return $qw->result_array();
    }
    
    // возвращает все набор метатегов для страницы 
     public function get_meta($page)
    {  
        $this->db->limit(1);
        $this->db->where('page', $page);
        $qw = $this->db->get(self::TABLE_LINES);
        return $qw->result_array();
    }
    
    // возвращает колличество
    public function size()
    {
        return $this->db->count_all_results(self::TABLE_LINES);
    }
    
    
    // функция с строке $str  находит строку шаблон $template  и заменяет его на 
    // значение $property
    public function castling($str,$template,$property)
    {
        
        $arr = explode($template, $str);
        $count = count($arr);
        if( $count>1){
            $new_str="";
            //вставляем после каждой строки из масива $property 
            //кроме последней
            for ( $i=0;  $i<($count-1); $i++) {
               $new_str.=($arr[$i].$property);
            }
            //добавляем последнюю за шаблоном
            $new_str.=$arr[$count-1];
        }else{
          $new_str=$str; 
        }
        return $new_str;
        
    }
    
    
    
    //меняет шаблоны на значения переменных
     public function mega_castling($str, $city ,$type,$name)
    {
          
        if($city!==''){
           $str= $this->castling( $str , '{москва}' , $city );
           $str= $this->castling( $str , '{москве}' , $this->prepositional_city($city) );
           $str= $this->castling( $str , '{москвы}' , $this->genitive_city($city) );  
        }
        
        if($name!==''){
               $str= $this->castling( $str , '{имя}' , $name );
        }
        
        if($type!==''){
        
            if(is_array($type)){
                foreach($type as $item) {
                    $str= $this->castling( $str , '{гостиница}' , $item['type'] );
                    $str= $this->castling( $str , '{гостинице}' , $this->prepositional_type($item['type']) );
                    $str= $this->castling( $str , '{моей_гостиницы}' , $this->genitive_type($item['type']) );
                    $str= $this->castling( $str , '{мои_гостиницы}' , $this->multiply_type($item['type']) );
                    $str= $this->castling( $str , '{гостиницу}' , $this->accusative_type($item['type']) );
                    
                }
            }else{

                $str= $this->castling( $str , '{гостиница}' , $type );
                $str= $this->castling( $str , '{гостинице}' , $this->prepositional_type($type) );
                $str= $this->castling( $str , '{моей_гостиницы}' , $this->genitive_type($type) );
                $str= $this->castling( $str , '{мои_гостиницы}' , $this->multiply_type($type) );
                $str= $this->castling( $str , '{гостиницу}' , $this->accusative_type($type) );
            }
         }
        
         //  {гостиница}{гостинице}{моей_гостиницы}{мои_гостиницы}{гостиницу}
        
       
        
       
       return $str;
    }
    
     public function title($page, $city='' ,$type='',$name='')
    {  
 /**Array
(
    [0] => Array
        (
            [id] => 1
            [page] => main
            [title] => Снять номер в гостинице, хостеле. Самостоятельно забронировать отель. Узнать цену, посмотреть фото и отзывы на Rusotels.ru
            [description] => На rusotels Вы найдете список отелей, каталог гостиниц их адреса и телефоны. У нас Вы сможете самостоятельно забронировать номер в мини-отеле, или место в хостеле. Снять номер в гостинице можно на сайте самостоятельно.
            [keywords] => aaa , main
        )
)
**/
       $arr=  $this->get_meta($page) ; 
       $str=  $arr[0]['title'];
       $str= $this->mega_castling($str, $city, $type, $name);
       return $str;
    }
    
     public function description($page, $city='' ,$type='',$name='')
    {   
       $arr=  $this->get_meta($page) ; 
       $str=  $arr[0]['description'];
       $str= $this->mega_castling($str, $city, $type, $name);
       return $str;
    }
    
     public function keywords($page, $city='' ,$type='',$name='')
    {    
       $arr=  $this->get_meta($page) ; 
       $str=  $arr[0]['keywords'];
       $str= $this->mega_castling($str, $city, $type, $name);
       return $str;
    }
    
     
    
}