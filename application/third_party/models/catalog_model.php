<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Catalog_model extends CI_Model {  

    function getTopCitiesWithTypes() {
        $res = mysql_query("SELECT h.city_id as cid, h.city_name, count(h.id) as total, c.alias  FROM hotels as h
                            INNER JOIN citys c ON c.id = city_id
                            WHERE h.published = 1
                            GROUP BY city_id ORDER BY total DESC LIMIT 4");

        $result = array();
        while($arr = mysql_fetch_array($res)) {

            $query = $this->db->query(
                "SELECT count(h.id) as num, t.type as name
                 FROM hotels h
                 INNER JOIN hotel_to_type t ON t.hotel = h.id
                 WHERE h.city_id = ? and published = 1 GROUP BY type",
            array($arr['cid']));
            $arr['types'] = $query->result();

            $result[] = $arr;
        }

        return $result;
    }

	//++++++
	//получение id-города по алиасу-города
	function getCityIdByAlias($alias)
    {
		$res = mysql_query("SELECT * FROM citys WHERE alias='$alias' LIMIT 1");
		$mas = ($mas = mysql_fetch_array($res)) ? $mas : null;
		//return $mas;
		$id = $mas['id'];
		return $id;
	}



	//+++++++++
	//получение id отеля по id-города и алиасу-отеля
	function getHotelIdByCityIdAndHotelAlias($city_id, $alias)
    {
		$res = mysql_query("SELECT * FROM hotels WHERE city_id=$city_id and alias='$alias' LIMIT 1");
		$mas = ($mas = mysql_fetch_array($res)) ? $mas : null;
		//return $mas;
		$id = $mas['id'];
		return $id;
	}

	//+++++++++++++
	//отель по id
	function getHotelById($id)
    {
		$res = mysql_query("SELECT * FROM hotels WHERE id=$id LIMIT 1");
		$mas = ($mas = mysql_fetch_array($res)) ? $mas : null;
		return $mas;
	}

	//++++++++++++
	//получение всех отелей с id города
	function getAllHotelsWithCityId($city_id, $sort='id') {

    	if ($sort == 'name') { $res = mysql_query("SELECT * FROM hotels WHERE published=1 and (city_id=$city_id) ORDER BY title ASC"); }
    	elseif ($sort == 'rank') { $res = mysql_query("SELECT * FROM hotels WHERE published=1 and (city_id=$city_id) ORDER BY rank DESC"); }
    	elseif ($sort == 'price') { $res = mysql_query("SELECT * FROM hotels WHERE published=1 and (city_id=$city_id) ORDER BY price_min DESC"); }
    	elseif ($sort == 'type') { $res = mysql_query("SELECT * FROM hotels WHERE published=1 and (city_id=$city_id) ORDER BY type DESC"); }
		elseif ($sort == 'rooms') { $res = mysql_query("SELECT * FROM hotels WHERE published=1 and (city_id=$city_id) ORDER BY room_count DESC"); }

    	elseif ($sort == 'all') { $res = mysql_query("SELECT * FROM hotels WHERE published=1 and (city_id=$city_id) ORDER BY id DESC"); }
    	elseif ($sort == '2') { $res = mysql_query("SELECT * FROM hotels WHERE published=1 and (city_id=$city_id) and rank=2 ORDER BY id DESC"); }
    	elseif ($sort == '3') { $res = mysql_query("SELECT * FROM hotels WHERE published=1 and (city_id=$city_id) and rank=3 ORDER BY id DESC"); }
		elseif ($sort == '4') { $res = mysql_query("SELECT * FROM hotels WHERE published=1 and (city_id=$city_id) and rank=4 ORDER BY id DESC"); }
		elseif ($sort == '5') { $res = mysql_query("SELECT * FROM hotels WHERE published=1 and (city_id=$city_id) and rank=5 ORDER BY id DESC"); }

		else { $res = mysql_query("SELECT * FROM hotels WHERE published=1 and (city_id=$city_id) ORDER BY id DESC"); }

		$array = array();
		while ($mas = mysql_fetch_array($res)) {
			$array[] = $mas;
		}

		return $array;
	}



	//+++++++++++++++++++
	//случайные 4 спец предложения
	function getRandomOffers($count=4, $city=0) {
        $and_city = "";
        if($city > 0) {
            $and_city = " AND c.id = " . intval($city);
        }
		$res = mysql_query("select
                            res.*,
                            p.photo as photo


                            from
                                (SELECT r.title as name,
                                        c.name as city,
                                        h.rank as stars,
                                        h.id as id,
                                        r.price_min as text,
                                        CONCAT(c.alias, '/', h.alias) as link
                                FROM hotels h
                                INNER JOIN hotel_revisions r ON r.hotel = h.id
                                INNER JOIN citys as c ON h.city_id = c.id
                                WHERE r.active = 1 $and_city
                                GROUP BY h.id ORDER BY RAND() LIMIT ".$count."
                                ) as res

                            LEFT JOIN hotel_photos p ON p.hotel = res.id
                            GROUP by res.id
                            ORDER by p.position ASC"); //DESC ASC

		$array = array();
		while ($mas = mysql_fetch_array($res)) {
			$array[] = $mas;
		}
		return $array;
	}

	//+++++++
	//получение всех спецпредложений
	function getAllOffers()
    {
		//$res = mysql_query("SELECT * FROM teachers ORDER BY RAND() LIMIT 3"); //DESC ASC
		$res = mysql_query("SELECT * FROM hotels WHERE offer_date_active != '0000-00-00' LIMIT 15"); //DESC ASC

		$array = array();
		while ($mas = mysql_fetch_array($res))
		{
			$array[] = $mas;
		}
		return $array;
	}

	//+++++++++++++
	//получение рандомных отелей по id-города (для "отели из этого же города")
	function getRandomHotelsByCityId($city_id)
	{
		$res = mysql_query("SELECT * FROM hotels WHERE published=1 and (city_id=$city_id) ORDER BY RAND() LIMIT 4");

		$array = array();
		while ($mas = mysql_fetch_array($res))
		{
			$array[] = $mas;
		}
		return $array;
	}




	//++++++++++++++
	//получение всех округов Areas
	function getAllAreas()
	{
		$res = mysql_query("SELECT * FROM areas ORDER BY id ASC");

		$array = array();
		while ($mas = mysql_fetch_array($res))
		{
			$array[] = $mas;
		}
		return $array;
	}


	//++++++++++++++
	//получение всех городов
	function getAllCitys()
	{
		$res = mysql_query("SELECT * FROM citys ORDER BY id ASC");

		$array = array();
		while ($mas = mysql_fetch_array($res))
		{
			$array[] = $mas;
		}
		return $array;
	}



	//++++++++++++++
	//получение всех городов КРУПНЫХ
	function getAllCitysWithMajor()
	{
		$res = mysql_query("SELECT * FROM citys WHERE major=1 ORDER BY name ASC");

		$array = array();
		while ($mas = mysql_fetch_array($res))
		{
			$array[] = $mas;
		}
		return $array;
	}



	//получение всех регионов Regions
	function getAllRegions()
	{
		# code...
	}


	//получение всех регионов по id-округа (id-area)
	function getRegionsWithAreaId($area_id)
    {
		$res = mysql_query("SELECT * FROM regions WHERE area_id=$area_id ORDER BY name ASC");

		$array = array();
		while ($mas = mysql_fetch_array($res))
		{
			$array[] = $mas;
		}
		return $array;
	}


	//++++++++++
	//получение id-округа (id-Area) по его алиасу
	function getAreaIdByAlias($alias) {
		$res = mysql_query("SELECT * FROM areas WHERE alias='$alias' LIMIT 1");
		$mas = ($mas = mysql_fetch_array($res)) ? $mas : null;
		$id = $mas['id'];
		return $id;
	}

	//++++++++
	//получение всех городов по id-региона
	function getCitysWithRegionId($region_id) {
		$res = mysql_query("SELECT * FROM citys WHERE region_id=$region_id ORDER BY name ASC");

		$array = array();
		while ($mas = mysql_fetch_array($res)) {
			$array[] = $mas;
		}

		return $array;
	}

	//+++++++
	//получение id региона по его алиасу
	function getRegionIdByAlias($alias) {
		$res = mysql_query("SELECT * FROM regions WHERE alias='$alias' LIMIT 1");
		$mas = ($mas = mysql_fetch_array($res)) ? $mas : null;
		$id = $mas['id'];
		return $id;
	}

	//получение городов по alias-региона
	function getCitysWithRegionAlias($region_alias) {
		# code...
	}

	//+++++++++++++
	//получение города по id
	function getCityById($id) {
		$res = mysql_query("SELECT * FROM citys WHERE id=$id LIMIT 1");
		$mas = ($mas = mysql_fetch_array($res)) ? $mas : null;
		return $mas;
	}

	//+++++++++++++
	//получение города по alias
	function getCityByAlias($alias) {
		$res = mysql_query("SELECT * FROM citys WHERE alias='$alias' LIMIT 1");
		$mas = ($mas = mysql_fetch_array($res)) ? $mas : null;
		return $mas;
	}

	//+++++++++++++
	//получение города по name (ДЛЯ ПОИСКА)
	function getCityByName($name)
	{
		$res = mysql_query("SELECT * FROM citys WHERE name='$name' or searchname1='$name' or searchname2='$name' or searchname3='$name' LIMIT 1");
		$mas = ($mas = mysql_fetch_array($res)) ? $mas : null;
		return $mas;
	}

	//++++++++++++
	//получение всех отелей по поисковой фразе (ДЛЯ ПОИСКА)
	function getAllHotelsLikeTitle($title)
    {
		$res = mysql_query("SELECT * FROM hotels WHERE title LIKE '%$title%' LIMIT 30");

		$array = array();
		while ($mas = mysql_fetch_array($res))
		{
			$array[] = $mas;
		}
		return $array;
	}

	//+++++++++
	//получение страницы по ключу
    function getPageByKey($keyx)
    {
		$res = mysql_query("SELECT * FROM pages WHERE keyx='$keyx' LIMIT 1");
		$mas = ($mas = mysql_fetch_array($res)) ? $mas : null;
		return $mas;
    }



    //получение текста для боковых столбцов
	function getColumnTextsById($id)
    {
		$res = mysql_query("SELECT * FROM column_texts WHERE id=$id LIMIT 1");
		$mas = ($mas = mysql_fetch_array($res)) ? $mas : null;
		return $mas;
	}




	function getAllPostsCatalog()
    {
		$res = mysql_query("SELECT * FROM blog_posts WHERE type='catalog' ORDER BY id DESC"); //DESC ASC

		$array = array();
		while ($mas = mysql_fetch_array($res))
		{
			$array[] = $mas;
		}
		return $array;
	}






}