<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends RO_Model {

    protected $_table = "pages";

    protected function rules() {
        return array(
            "admin" => array(
                "title" => array ("label" => "Заголовок", "rules" => "required"),
                "keyx" => array ("label" => "Alias", "rules" => "required|alpha_dash"),
                "content" => array ("label" => "Тело страницы", "rules" => ""),
            ),
        );
    }
}