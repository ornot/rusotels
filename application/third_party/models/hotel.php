<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hotel extends RO_Model {

    protected $_table = "hotels";

    public $_active_rev = 1;

    protected function rules() {
        return array(
            "register" => array(
                "title" => array ("label" => "Наименование", "rules" => "required"),
                "room_count" => array ("label" => "Количество номеров"),
                "price_min" => array ("label" => "Минимальная цена"),
                "price_max" => array ("label" => "Максимальная цена"),
                "city_id" => array ("label" => "Город", "rules" => "integer"),
                "content" => array ("label" => "Описание"),
                "email" => array ("label" => "E-Mail", "rules" => "valid_email"),
                "skype" => array ("label" => "Skype"),
                "site" => array ("label" => "Сайт"),
            ),
            "admin" => array(
                "title" => array ("label" => "Наименование", "rules" => "required"),
                "alias" => array ("label" => "Наименование", "rules" => "alpha_dash"),
                "keywords" => array ("label" => "Ключевые слова"),
                "description" => array ("label" => "Мета описание"),
                "meta_title" => array ("label" => "Заголовок страницы"),
                "rank" => array ("label" => "Количество звезд"),
                "room_count" => array ("label" => "Количество номеров"),
                "price_min" => array ("label" => "Минимальная цена"),
                "price_max" => array ("label" => "Максимальная цена"),
                "city_id" => array ("label" => "Город", "rules" => "integer"),
                "content" => array ("label" => "Описание"),
                "email" => array ("label" => "E-Mail"),
                "skype" => array ("label" => "Skype"),
                "site" => array ("label" => "Сайт"),
                "terms" => array ("label" => "Условия проживания"),
                "published" => array ("label" => "Опубликовано", "rules" => "integer"),
                "rooms_widget" => array ("label" => "Виджет номеров"),
                "admin_widget" => array ("label" => "Виджет Админки"),
                "is_client" => array("label" => "таки клиент", "rules" => "integer"),
                "lb_id" => array("label" => "Litebooking id")
            ),
        );
    }

    public function save($active = 0) {

        $revision_fields = array("title", "content", "address", "email", "skype", "phone", "site", "room_count", "price_min", "price_max");

        $rev = new Hotel_Revision();
        foreach($this->_data as $key => $value) {
            if(in_array($key, $revision_fields)) {
                $rev->$key = $value;
                unset($this->_data[$key]);
            }
        }

        if(!isset($this->_data['id'])) {
            $rev->active = 1;
        }

        $hid = parent::save();

        if($hid) {

            $filter = array('hotel' => $hid);


            $rev->hotel = $hid;

            $rev->date = time();


            if($active == 1) {
                $filter['active'] = 1;
                $r = $this->hotel_revision->find($filter, array(), array('id', 'desc'));

                if($r) {
                    $rev->id = $r['id'];
                }
            } else {
                $r = $this->hotel_revision->find($filter, array(), array('id', 'desc'));

                if(!$r['active']) {
                    $rev->id = $r['id'];
                }
            }

            $rev->save();
        }

        return $hid;
    }
    
    public function save_passive() {
        $revision_fields = array("title", "content", "address", "email", "skype", "phone", "site", "room_count", "price_min", "price_max");
        $rev = new Hotel_Revision();
        foreach ($this->_data as $key => $value) {
            if (in_array($key, $revision_fields)) {
                $rev->$key = $value;
                unset($this->_data[$key]);
            }
        }
        $hid = parent::save();
        if ($hid) {
            $filter = array('hotel' => $hid);
            $rev->hotel = $hid;
            $rev->date = time();
            $r = $this->hotel_revision->find($filter, array(), array('id', 'desc'));
            if (!$r['active']) {
                $rev->id = $r['id'];
            }
            $rev->save();
        }
        return $hid;
    }
    
    public function find($conditions = array(), $limit = array(), $sort = array('id', 'desc')) {
        $data = parent::find($conditions, $limit, $sort);
        if($data) {
            if($this->_active_rev == 1) {
                $rev = $this->hotel_revision->find(array("hotel" => $data['id'], "active" => 1));
            } else {
                $rev = $this->hotel_revision->find(array('hotel' => $data['id']), array(), array('id', 'desc'));
            }

            unset($rev['id']);
            unset($rev['date']);
            unset($rev['hotel']);
            unset($rev['active']);

            $data = array_merge($data, $rev);
        }

        return $data;
    }

    public function findAll($conditions = array(), $limit = array(), $sort = array('id', 'desc')) {

        $this->db->select('hotel_revisions.title,
                           hotel_revisions.price_min,
                           hotel_revisions.price_max,
                           hotel_revisions.room_count,
                           hotel_revisions.address,
                           hotel_revisions.content,
                           hotel_revisions.phone,
                           hotel_revisions.email,
                           hotel_revisions.skype,

                           hotel_revisions.id AS rid, hotels.*');
        $this->db->join('hotel_revisions', 'hotel_revisions.hotel = hotels.id');
        $this->db->where('hotel_revisions.active = 1');

        if(is_array($sort)) {
            $sort = array('hotels.photo_count DESC, hotels.' . $sort[0], $sort[1]);
        } else {
            $sort= 'hotels.photo_count DESC, ' . $sort;
        }

        $data = parent::findAll($conditions, $limit, $sort);

//        die($this->db->last_query());

//        foreach($data as $key => $hotel) {
//            $rev = $this->hotel_revision->find(array("hotel" => $hotel['id'], "active" => 1));
//
//            unset($rev['id']);
//            unset($rev['date']);
//            unset($rev['hotel']);
//            unset($rev['active']);
//
//            $data[$key] = array_merge($hotel, $rev);
//        }

//        die(print_r($data));

        return $data;
    }

    public function count($conditions = array()) {
        $this->db->select('hotel_revisions.title,
                           hotel_revisions.price_min,
                           hotel_revisions.price_max,
                           hotel_revisions.room_count,
                           hotel_revisions.address,
                           hotel_revisions.phone,
                           hotel_revisions.email,
                           hotel_revisions.skype,

                           hotel_revisions.id AS rid, hotels.*');

        $this->db->join('hotel_revisions', 'hotel_revisions.hotel = hotels.id');
        $this->db->where('hotel_revisions.active = 1');

        $data = parent::count($conditions);

//        die($this->db->last_query());

        return $data;
    }

    public function latest() {
        $model = new Hotel();
        $model->_active_rev = 0;

        return $model;
    }

    public function get_type($id) {
        $this->db->select('hotel_types.*');
        $this->db->from('hotel_types');
        $this->db->join('hotel_to_type', "hotel_to_type.type = hotel_types.id");
        $this->db->where('hotel_to_type.hotel', $id);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function add_types($types, $hotel) {
        $this->db->delete('hotel_to_type', array('hotel' => $hotel));

        foreach($types as $type) {
            $data = array(
                'type' => $type,
                'hotel' => $hotel,
            );
            $this->db->insert('hotel_to_type', $data);
        }
    }

//    public function get_weight($hotel) {
//        $query = $this->db->query("select @pos:=@pos+1 as pos from ( select id, weight from hotels where id = ". $hotel ." order by weight desc, id asc ) t1, ( select @pos:=0 ) t2");
//
//    }
    
    
    public function find_passive($hotel_id) {
        $data = parent::find(array("id" => $hotel_id));
        if ($data) { 
            $rev = $this->hotel_revision->get_passive($hotel_id);
            unset($rev['id']);
            unset($rev['date']);
            unset($rev['hotel']);
            unset($rev['active']);

            $data = array_merge($data, $rev);
        }

        return $data;
    }

}