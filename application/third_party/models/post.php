<?php

class Post extends RO_Model {

    protected $_table = "blog_posts";

    protected function rules() {
        return array(
            "edit" => array(
                "title" => array ("label" => "Наименование", "rules" => "required"),
                "alias" => array ("label" => "Количество звезд", "rules" => "required|alpha_dash"),
                "publish_date" => array ("label" => "Город", "rules" => "required"),
                "content" => array ("label" => "Город"),
            ),
        );
    }
}