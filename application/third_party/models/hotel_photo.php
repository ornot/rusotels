<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hotel_Photo extends RO_Model {

    protected $_table = "hotel_photos";

    function save() {

        if(isset($this->_data['hotel'])) {
            $hotel = $this->hotel->find(array('id' => $this->_data['hotel']));

            $h = new Hotel();
            $h->id = $hotel['id'];
            $h->photo_count = $hotel['photo_count'] + 1;

            $h->save(1);
        }

        return parent::save();
    }

    function remove($id) {
        $photo = $this->find(array('id' => $id));
        if(isset($photo['hotel'])) {
            $hotel = $this->hotel->find(array('id' => $photo['hotel']));

            $h = new Hotel();
            $h->id = $hotel['id'];
            $h->photo_count = $hotel['photo_count'] - 1;

            $h->save(1);
        }

        return parent::remove($id);
    }
}