<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hotel_Revision extends RO_Model {

    protected  $_table = "hotel_revisions";
    
    
    //возвращает активную модерированную  ревизию ту что видят все
    /**[id] => 30368
    [date] => 1413362912
    [active] => 1
    [hotel] => 30382
    [title] => r3
    [content] => фффффффффффффффффффф
    [address] => Сув|34|
    [email] => vimbaslim@mail.ru
    [skype] => aaaa
    [phone] => 111|11111111111111111
    [site] => 
    [room_count] => 500
    [price_min] => 1
    [price_max] => 2
     * **/
    public function get_active($hotel_id) {
        if (isset($hotel_id)) {
            $this->db->limit(1);
            $this->db->where('hotel', $hotel_id);
            $this->db->where('active', 1);
            $qw = $this->db->get($this->_table);
            $arr = $qw->result_array();
            if (count($arr) > 0) {
                return $arr[0];
            }
        }
        return null;
    }

    //возвращает немодерированную ревизию ту что видно при редактировании отеля
    public function get_passive($hotel_id) {
        //
        if (isset($hotel_id)) {
            $this->db->limit(1);
            $this->db->where('hotel', $hotel_id);
            $this->db->where('active', 0);
            $qw = $this->db->get($this->_table);
            $arr = $qw->result_array();
            if (count($arr) > 0) {
               
                
                $dt=$arr[0];
                
                /**
                   [id] => 30377
                   [date] => 1413441651
                   [active] => 0
                   [hotel] => 30385
                   [title] => 
                   [content] => 
                   [address] => 
                   [email] => 
                   [skype] => 
                   [phone] => 
                   [site] => 
                   [room_count] => 0
                   [price_min] => 0
                   [price_max] => 0
                **/
                //ловим случай когда немодерированная ревизия пустая
                
                if($dt['title']===""){
                    return $this->hotel_revision->get_active($hotel_id);
                }
                return $dt;
                
            }else{
                return $this->hotel_revision->get_active($hotel_id);
            }
            
        }
        return null;
    }

    public function is_modifet($hotel_id){
        if (isset($hotel_id)) {
            $this->db->limit(1);
            $this->db->where('hotel', $hotel_id);
            $this->db->where('active', 0);
            $qw = $this->db->get($this->_table);
            $arr = $qw->result_array();
            if (count($arr) > 0) {
                return true;
            }
        }
        return false;
    }
    
    //удаляет все нулевые ревизии для отеля
    public function del_all_passive($hotel_id)        
    {
        $this->db->where('hotel', $hotel_id);
        $this->db->where('active', 0);
        $this->db->delete($this->_table);
    }
    
    //удаляет все нулевые ревизии для всех отелей
    public function global_all_clear_passive_revision() {
        $this->db->where('active', 0);
        $this->db->delete($this->_table);   
    }  
}