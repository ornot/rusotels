<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Cp_model extends CI_Model {  





	function getAllTeachers()
    {
		$res = mysql_query("SELECT * FROM teachers ORDER BY position ASC"); //DESC ASC

		$array = array();
		while ($mas = mysql_fetch_array($res))
		{
			$array[] = $mas;
		}
		return $array;
	}

    function getTeacherById($id)
    {
		$res = mysql_query("SELECT * FROM teachers WHERE id='$id' LIMIT 1");
		$mas = ($mas = mysql_fetch_array($res)) ? $mas : null;
		return $mas;
    }


    function deleteTeacher($id)
    {
    	mysql_query("DELETE FROM teachers WHERE id=$id LIMIT 1");
    	mysql_query("DELETE FROM webinars WHERE teacher_1=$id LIMIT 999");
    }



 	function updateTeacher($id=0, $alias='', $firstname='', $lastname='', $middlename='', $email='', $content='', $content_short='', $position='')
    {
    	if (empty($alias)) { $alias = substr(md5(rand()), 0, 8); }
        $alias = format_alias($alias);

    	$data['alias'] = $alias;
    	$data['firstname'] = $firstname;
    	$data['lastname'] = $lastname;
    	$data['middlename'] = $middlename;
    	$data['email'] = $email;
    	//$data['photo_small'] = $photo_small;
    	//$data['photo_big'] = $photo_big;
    	$data['content'] = $content;
    	$data['content_short'] = $content_short;
    	$data['position'] = $position;

		$this->db->where('id', $id);
		$this->db->update('teachers', $data);
    }


    function addTeacher($alias, $firstname, $lastname, $middlename, $email, $photo_small, $photo_big, $content, $content_short, $position)
    {
    	if (empty($alias)) { $alias = substr(md5(rand()), 0, 8); }
        $alias = format_alias($alias);

    	$data['alias'] = $alias;
    	$data['firstname'] = $firstname;
    	$data['lastname'] = $lastname;
    	$data['middlename'] = $middlename;
    	$data['email'] = $email;
    	$data['photo_small'] = $photo_small;
    	$data['photo_big'] = $photo_big;
    	$data['content'] = $content;
    	$data['content_short'] = $content_short;
    	$data['position'] = $position;

		//$this->db->where('id', $id);
		$this->db->insert('teachers', $data);

    }









	function getAllWebinars()
    {
		$res = mysql_query("SELECT * FROM webinars ORDER BY date_start DESC"); //DESC ASC

		$array = array();
		while ($mas = mysql_fetch_array($res))
		{
			$array[] = $mas;
		}
		return $array;
	}


	function getWebinarById($id)
    {
		$res = mysql_query("SELECT * FROM webinars WHERE id=$id LIMIT 1");
		$mas = ($mas = mysql_fetch_array($res)) ? $mas : null;
		return $mas;
	}


    function deleteWebinar($id)
    {
    	mysql_query("DELETE FROM webinars WHERE id=$id LIMIT 1");
    }










	function getCityById($id)
	{
		$res = mysql_query("SELECT * FROM citys WHERE id=$id LIMIT 1");
		$mas = ($mas = mysql_fetch_array($res)) ? $mas : null;
		return $mas;
	}


	//получение всех городов КРУПНЫХ
	function getAllCitysWithMajor()
	{
		$res = mysql_query("SELECT * FROM citys WHERE major=1 ORDER BY name ASC");

		$array = array();
		while ($mas = mysql_fetch_array($res))
		{
			$array[] = $mas;
		}
		return $array;
	}


    function getAllCitys()
    {
        $res = mysql_query("SELECT * FROM citys ORDER BY name ASC");

        $array = array();
        while ($mas = mysql_fetch_array($res))
        {
            $array[] = $mas;
        }
        return $array;
    }



	function getAllRegions()
	{
		$res = mysql_query("SELECT * FROM regions ORDER BY name ASC");

		$array = array();
		while ($mas = mysql_fetch_array($res))
		{
			$array[] = $mas;
		}
		return $array;
	}


    function getCitysWithRegionId($region_id)
    {
        $res = mysql_query("SELECT * FROM citys WHERE region_id=$region_id ORDER BY name ASC");

        $array = array();
        while ($mas = mysql_fetch_array($res))
        {
            $array[] = $mas;
        }
        return $array;
    }


    function getAllHotelsWithCityId($city_id)
    {
        $res = mysql_query("SELECT * FROM hotels WHERE city_id=$city_id ORDER BY id DESC");

        $array = array();
        while ($mas = mysql_fetch_array($res))
        {
            $array[] = $mas;
        }
        return $array;
    }






    public function getCountHotelsWithCityId($id)
    {
		$this->db->where('city_id', $id);
		$this->db->where('published', 1);
		$this->db->from('hotels');
		return $this->db->count_all_results();
    }


    public function updateCity($id=0, $name='', $alias='', $content='')
    {
        if (empty($alias)) { $alias = substr(md5(rand()), 0, 8); }
        $alias = format_alias($alias);

        $data['name'] = $name;
        $data['alias'] = $alias;
        $data['content'] = $content;

    
        // $data['title'] = $title;

        $this->db->where('id', $id);
        $this->db->update('citys', $data);
    }







    public function updateWebinar($id=0, $title='', $alias='', $date_start='0000-00-00', $date_end='0000-00-00', $time_length='', $price='', $content='', $content_b1='', $content_b2='', $teacher_1='0', $time_start='', $document='', $keywords='')
    {
    	if (empty($alias)) { $alias = substr(md5(rand()), 0, 8); }
    	if (empty($teacher_1)) { $teacher_1 = 0; }
        $alias = format_alias($alias);

    	$data['title'] = $title;
    	$data['alias'] = $alias;
    	$data['date_start'] = $date_start;
    	$data['date_end'] = $date_end;
    	$data['time_length'] = $time_length;
    	$data['price'] = $price;
    	$data['content'] = $content;
    	$data['content_b1'] = $content_b1;
    	$data['content_b2'] = $content_b2;
    	$data['teacher_1'] = $teacher_1;
        $data['time_start'] = $time_start;
        $data['document'] = $document;

        $data['keywords'] = $keywords; //ключевики

    
    	// $data['title'] = $title;

		$this->db->where('id', $id);
		$this->db->update('webinars', $data);
    }





    public function updateWebinarImg($id=0, $image='')
    {
        //if (empty($alias)) { $alias = substr(md5(rand()), 0, 8); }
        //if (empty($teacher_1)) { $teacher_1 = 0; }
        //$alias = format_alias($alias);

        $data['image'] = $image;
        //$data['alias'] = $alias;

        $this->db->where('id', $id);
        $this->db->update('webinars', $data);
    }



    public function addWebinar($title='', $alias='', $date_start='0000-00-00', $date_end='0000-00-00', $time_length='', $price='', $content='', $content_b1='', $content_b2='', $teacher_1='0', $time_start='', $document='', $keywords='')
    {
    	if (empty($alias)) { $alias = substr(md5(rand()), 0, 8); }
    	if (empty($teacher_1)) { $teacher_1 = 0; }
        $alias = format_alias($alias);

    	$data = array();
    	$data['title'] = $title;
    	$data['alias'] = $alias;
    	$data['date_start'] = $date_start;
    	$data['date_end'] = $date_end;
    	$data['time_length'] = $time_length;
    	$data['price'] = $price;
    	$data['content'] = $content;
    	$data['content_b1'] = $content_b1;
    	$data['content_b2'] = $content_b2;
    	$data['teacher_1'] = $teacher_1;
        $data['time_start'] = $time_start;
        $data['document'] = $document;

        $data['keywords'] = $keywords; //ключевики
      
    	// $data['title'] = $title;

		
		$this->db->insert('webinars', $data);
    }












    public function getAllPosts()
    {
		$res = mysql_query("SELECT * FROM blog_posts ORDER BY id ASC"); //DESC ASC
		$array = array();
		while ($mas = mysql_fetch_array($res))
		{
			$array[] = $mas;
		}
		return $array;
    }


    function getPostById($id)
    {
		$res = mysql_query("SELECT * FROM blog_posts WHERE id='$id' LIMIT 1");
		$mas = ($mas = mysql_fetch_array($res)) ? $mas : null;
		return $mas;
    }


    function deletePost($id)
    {
    	mysql_query("DELETE FROM blog_posts WHERE id=$id LIMIT 1");
    }



    public function addPost($title='', $alias='', $publish_date='', $type='', $content='')
    {
    	if (empty($alias)) { $alias = substr(md5(rand()), 0, 8); }
    	if (empty($type)) { $type = 'catalog'; }
        $alias = format_alias($alias);

    	$data = array();
    	$data['title'] = $title;
    	$data['alias'] = $alias;
    	$data['publish_date'] = $publish_date;
    	$data['type'] = $type;
    	$data['content'] = $content;
		
		$this->db->insert('blog_posts', $data);
    }
    

    public function updatePost($id=0, $title='', $alias='', $publish_date='', $type='', $content='')
    {
    	if (empty($alias)) { $alias = substr(md5(rand()), 0, 8); }
    	if (empty($type)) { $type = 'catalog'; }
        $alias = format_alias($alias);

		$data = array();
    	$data['title'] = $title;
    	$data['alias'] = $alias;
    	$data['publish_date'] = $publish_date;
    	$data['type'] = $type;
    	$data['content'] = $content;

		$this->db->where('id', $id);
		$this->db->update('blog_posts', $data);
    }








    function getAllHotels($start = 0, $limit=50)
    {
        $res = mysql_query("SELECT * FROM hotels ORDER BY id DESC LIMIT $start, $limit"); //DESC ASC

        $array = array();
        while ($mas = mysql_fetch_array($res))
        {
            $array[] = $mas;
        }
        return $array;
    }


    function getAllHotelsWithPublished($published=0)
    {
        $res = mysql_query("SELECT * FROM hotels WHERE published=$published ORDER BY id DESC LIMIT 50"); //DESC ASC

        $array = array();
        while ($mas = mysql_fetch_array($res))
        {
            $array[] = $mas;
        }
        return $array;
    }


    function getHotelById($id)
    {
        $res = mysql_query("SELECT * FROM hotels WHERE id=$id LIMIT 1");
        $mas = ($mas = mysql_fetch_array($res)) ? $mas : null;
        return $mas;
    }

    public function updateHotel($id=0, $city_id=0, $alias='', $title='', $content='', $address='', $email='',
                                $skype='', $phone='', $site='', $rank='', $type='', $room_count=0, $price_min=0,
                                $price_max=0, $published=1, $keywords='', $rooms_widget='', $book_widget='',
                                $revoke_widget='', $terms='') {
        if (empty($alias)) { $alias = substr(md5(rand()), 0, 8); }
        $alias = format_alias($alias);
        $site = format_site($site);

        $data = array();
        $data['city_id'] = $city_id;
        $data['alias'] = $alias;
        $data['title'] = $title;
        $data['content'] = $content;
        $data['address'] = $address;
        $data['email'] = $email;
        $data['skype'] = $skype;
        $data['phone'] = $phone;
        $data['site'] = $site;
        $data['rank'] = $rank;
        $data['type'] = $type;
        $data['room_count'] = $room_count;
        $data['price_min'] = $price_min;
        $data['price_max'] = $price_max;
        $data['published'] = $published;
        $data['keywords'] = $keywords;
        $data['terms'] = $terms;

        $data['rooms_widget'] = $rooms_widget;
        $data['book_widget'] = $book_widget;
        $data['revoke_widget'] = $revoke_widget;

        $this->db->where('id', $id);
        $this->db->update('hotels', $data);
    }

    //обновление фотографий отеля (Ид отеля, номер фото, назв файла). Также исп для удаления фото при пустом послед параметре
    public function updateHotelImg($id=0, $num='1', $image='')
    {
        if ($num==0) { $data['photo_1'] = $image; }
        if ($num==1) { $data['photo_1'] = $image; }
        else if ($num==2) { $data['photo_2'] = $image; }
        else if ($num==3) { $data['photo_3'] = $image; }
        else if ($num==4) { $data['photo_4'] = $image; }
        else if ($num==5) { $data['photo_5'] = $image; }
        else { $data['photo_5'] = $image; }


        $this->db->where('id', $id);
        $this->db->update('hotels', $data);
    }


    public function sortHotelImg($id=0)
    {
        //сортируем фотки. поднимаем все вверх

        $res = mysql_query("SELECT * FROM hotels WHERE id=$id LIMIT 1");
        $mas = ($mas = mysql_fetch_array($res)) ? $mas : null;
        //return $mas;

        $hotel = $mas;

        $box = array();
        if ($hotel['photo_1'] != '') { $box[] = $hotel['photo_1']; }
        if ($hotel['photo_2'] != '') { $box[] = $hotel['photo_2']; }
        if ($hotel['photo_3'] != '') { $box[] = $hotel['photo_3']; }
        if ($hotel['photo_4'] != '') { $box[] = $hotel['photo_4']; }
        if ($hotel['photo_5'] != '') { $box[] = $hotel['photo_5']; }


        if (!empty($box['0'])) { $data['photo_1']=$box['0']; } else { $data['photo_1'] = ''; }
        if (!empty($box['1'])) { $data['photo_2']=$box['1']; } else { $data['photo_2'] = ''; }
        if (!empty($box['2'])) { $data['photo_3']=$box['2']; } else { $data['photo_3'] = ''; }
        if (!empty($box['3'])) { $data['photo_4']=$box['3']; } else { $data['photo_4'] = ''; }
        if (!empty($box['4'])) { $data['photo_5']=$box['4']; } else { $data['photo_5'] = ''; }

        $this->db->where('id', $id);
        $this->db->update('hotels', $data);

        //echo "<hr>good<hr>";
    }


    public function getCountHotelImg($id=0)
    {
        $res = mysql_query("SELECT * FROM hotels WHERE id=$id LIMIT 1");
        $mas = ($mas = mysql_fetch_array($res)) ? $mas : null;
        //return $mas;

        $hotel = $mas;
        $count = 0;
        if ($hotel['photo_1'] != '') { $count = $count + 1; }
        if ($hotel['photo_2'] != '') { $count = $count + 1; }
        if ($hotel['photo_3'] != '') { $count = $count + 1; }
        if ($hotel['photo_4'] != '') { $count = $count + 1; }
        if ($hotel['photo_5'] != '') { $count = $count + 1; }

        return $count;
    }
    




    public function addHotel($city_id=0, $user_id=0, $alias='', $title='', $content='', $address='', $email='', $skype='', $phone='', $site='', $rank='', $type='', $room_count=0, $price_min=0, $price_max=0, $published=1, $keywords='', $terms='')
    {
        if (empty($alias)) { $alias = substr(md5(rand()), 0, 8); }
        $alias = format_alias($alias);
        $site = format_site($site);

        $data = array();
        $data['city_id'] = $city_id;
        $data['user_id'] = $user_id;
        $data['alias'] = $alias;
        $data['title'] = $title;
        $data['content'] = $content;
        $data['address'] = $address;
        $data['email'] = $email;
        $data['skype'] = $skype;
        $data['phone'] = $phone;
        $data['site'] = $site;
        $data['rank'] = $rank;
        $data['type'] = $type;
        $data['room_count'] = $room_count;
        $data['price_min'] = $price_min;
        $data['price_max'] = $price_max;
        $data['published'] = $published;
        $data['keywords'] = $keywords;
        $data['terms'] = $terms;

        $this->db->insert('hotels', $data);
    }


    function deleteHotel($id)
    {
        mysql_query("DELETE FROM hotels WHERE id=$id LIMIT 1");
    }


    function searchHotel($q='')
    {
        $res = mysql_query("SELECT * FROM hotels WHERE title LIKE '%$q%' LIMIT 40");

        $array = array();
        while ($mas = mysql_fetch_array($res))
        {
            $array[] = $mas;
        }
        return $array;
    }









//4ex
    public function getCountWebinars()
    {
		//$this->db->where('template', 10);
		//$this->db->where('published', 1);
		$this->db->from('webinars');
		return $this->db->count_all_results();
    }

    public function getCountHotels($pub=0)
    {
		if ($pub==1) { $this->db->where('published', 1); }
		$this->db->from('hotels');

		return $this->db->count_all_results();
    }
//end 4ex



}