<?php

class User extends RO_Model {

    protected $_table = "users";

    protected function rules() {
        return array(
            "register" => array(
                "email" => array("label" => "E-mail", "rules" => "required|valid_email|is_unique[users.email]",
                                 "message" => array("is_unique" => "Поле E-mail должно содержать уникальное значение. Возможно вы уже зарегистрированы, либо Вам была отправлена ссылка для активации. Свяжитесь с администрацией по почте support@rusotels.ru укажите: название гостиницы, E-mail на который вы хотите зарегистрировать гостиницу, контактное лицо и телефон для обратной связи.")),
                "password" => array("label" => "Пароль", "rules" => "required", "modify" => "md5"),
            ),
            "update_user" => array(
                "old" => array("label" => "Ваш текущий пароль", "rules" => "callback_old_password", "ignore" => true),
                "password" => array("label" => "Новый пароль", "rules" => "", "modify" => "md5", "null" => false),
                "repeat" => array("label" => "Повтор пароля", "rules" => "callback_match_password", "ignore" => true),
            ),
            "update_admin" => array(
                "status" => array("label" => "Статус", "rules" => "integer"),
                "role" => array("label" => "Роль"),
            ),
        );
    }

    public function hasRole($role) {
        if(!is_array($role)) {
            $role = array($role);
        }

        if(isset($_SESSION['user'])) {
            return in_array($_SESSION['user']['role'], $role);
        } else {
            return in_array('guest', $role);
        }
    }
}
