<?php

class City extends RO_Model {

    protected $_table = "citys";

    protected function rules() {
        return array(
            "edit" => array(
                "name" => array ("label" => "Наименование", "rules" => "required"),
                "alias" => array ("label" => "Количество звезд", "rules" => "required|alpha_dash"),
                "region_id" => array ("label" => "Город", "rules" => "integer"),
                "major" => array ("label" => "Город"),
            ),
        );
    }
}