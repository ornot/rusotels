<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>404 страница не найдена</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . "/" ?>assets/js/chosen/chosen.min.css" media="screen">
    <link href="/assets/css/validationEngine.jquery.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . "/" ?>/assets/css/ui-lightness/jquery-ui-1.10.4.custom.min.css" media="screen">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . "/" ?>assets/_css/style.css" media="screen">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . "/" ?>assets/css/style.css" media="screen">

    <script type="text/javascript" src="/assets/js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="/assets/js/jquery-ui-1.10.4.custom.min.js"></script>
    <script type="text/javascript" src="/assets/js/jquery.ui.datepicker-ru.js"></script>
    <script type="text/javascript" src="<?php echo base_url() . "/" ?>assets/js/jquery.placeholder.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() . "/" ?>assets/js/jquery.carouFredSel-6.0.4.js"></script>

    <script type="text/javascript" src="<?php echo base_url() . "/" ?>assets/js/text-shadow.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url() . "/" ?>assets/js/jquery.formstyler.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() . "/" ?>assets/js/jquery.accordion.js"></script>
    <script type="text/javascript" src="<?php echo base_url() . "/" ?>assets/js/ajax.js"></script>
    <script type="text/javascript" src="<?php echo base_url() . "/" ?>assets/js/chosen/chosen.jquery.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url() . "/" ?>assets/js/bootstrap.min.js"></script>

    <script src="/assets/js/jquery.validationEngine-ru.js"></script>
    <script src="/assets/js/jquery.validationEngine.js"></script>

    <script src="/assets/js/ckeditor/ckeditor.js"></script>
    <script src="/assets/js/ckeditor/adapters/jquery.js"></script>

    <script type="text/javascript" src="<?php echo base_url() . "/" ?>assets/js/scripts.js"></script>
</head>
<body>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter20928640 = new Ya.Metrika({id:20928640,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/20928640" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->


<div id="header" class="wrap" style="text-align: center">
    <a class="logo" href="<?php echo base_url(); ?>"><img src="<?php echo base_url();?>/assets/images/logo.png" /></a>
</div>

<div id="content" class="wrap">
    <p style="text-align: center; font-size: 172px; line-height: 2px; color: #038BB2">
        404
    </p>
    <p style="color: #999999; font-size: 18px; text-align: center; line-height: 40px">
        Страница, на которую Вы перешли, удалена или перенесена
    </p>
    <p style="color: #999999; font-size: 18px; text-align: center; line-height: 40px">
        Предлагаем Вам перейти на <a href="/" style="color: #038BB2;">Главную страницу</a> сайта РусОтелс, и приступить к выбору отеля
    </p>
    <p><br/></p>
    <p><br/></p>
    <p><br/></p>
</div>

<div id="footer">
    <div class="wrap">
        <div class="block block-menu menu-company">
            <div class="title">Компания Rusotels</div>
            <div class="content">
                <ul class="menu">
                    <li><a href="<?php echo base_url('about'); ?>">О компании</a></li>
                    <li><a href="<?php echo base_url('projects'); ?>">Проекты</a></li>
                    <li><a href="<?php echo base_url('blog'); ?>">Блог</a></li>
                    <li><a href="<?php echo base_url('contacts'); ?>">Контакты</a></li>
                </ul>
            </div>
        </div>
        <div class="block block-menu menu-hotels">
            <div class="title">Для гостиниц, отелей, мини-отелей и других средств размещения</div>
            <div class="content">
                <ul class="menu">
                    <li><a href="http://edu.rusotels.ru">Обучение персонала</a></li>
                    <li><a href="<?php echo base_url('signup'); ?>">Размещение в каталоге</a></li>
                </ul>
            </div>
        </div>
        <div class="block block-menu menu-additions">
            <div class="title">Дополнительная информация</div>
            <div class="content">
                <ul class="menu">
                    <li><a href="<?php echo base_url('sitemap'); ?>">Карта сайта</a></li>
                    <li><a href="<?php echo base_url('agreement'); ?>">Соглашение</a></li>
                    <!-- <li><a href="<?php echo base_url('partners'); ?>">Партнеры Rusotels</a></li> -->
                </ul>
            </div>
        </div>
        <div class="block block-contacts">
            <div class="content">
                <!-- Соц сети -->
                <div class="phone">+7 812 404 6524</div>
                <ul class="social">
                    <li><a class="fb" href="https://www.facebook.com/rusotels"></a></li>
                    <li><a class="tw" href="https://twitter.com/rusotels"></a></li>
                    <li><a class="vk" href="http://vk.com/rusotels"></a></li>
                </ul>
                <div class="copyright">© 2012-2014. Все права защищены.</div>
                <div style="font-size: 10px">Сайт имеет возрастное ограничение 12+</div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 979936934;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/979936934/?value=0&amp;guid=ON&amp;script=0"/>
    </div>
</noscript>

</body>
</html>