<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

//статические страницы
//$route['about'] = 'pages/about';
//$route['about/team'] = 'pages/team';
//$route['contacts'] = 'pages/contacts';
//$route['projects'] = 'pages/projects';
//$route['agreement'] = 'pages/agreement';
//$route['sitemap'] = 'pages/sitemap';


$route['signup'] = 'signup';
$route['signup/(:any)'] = 'signup/$1';
$route['login'] = 'signup/login';
$route['logout'] = 'signup/logout';
$route['recovery'] = 'signup/recovery';
$route['token/(:any)'] = 'signup/token/$1';
$route['signup/success'] = 'signup/success';

$route['panel'] = 'panel';
$route['panel/(:any)'] = 'panel/$1';

$route['ajax/(:any)'] = 'ajax/$1';


$route['blog'] = 'blog';
$route['blog/(:any)'] = 'blog/view/$1';

$route['reserveplus'] = 'pages/reserveplus';
$route['whatoffers'] = 'pages/whatoffers';


$route['search'] = 'search'; //поиск

$route['offers'] = 'offers/all'; //спецпредложения


$route['cache/thumbs/hotels/(:any)/(:any)'] = 'cache/thumbs/$1/$2'; //кеш картинок


$route['zone/(:any)'] = "zone/listall/$1"; //города и регионы


$route['cities/(:any)'] = "catalog/cities/$1";
$route['cities'] = "catalog/cities";
$route['go/(:any)'] = "catalog/go/$1";
$route['hotels/(:any)/(:any)$'] = "catalog/info/$1/$2"; //для каталога
$route['hotels/(:any)$'] = "search/index/$1"; //для каталога

$route['(:any)$'] = "pages/show/$1"; //для каталога

$route['default_controller'] = "main";
$route['404_override'] = 'page/not_found';


/* End of file routes.php */
/* Location: ./application/config/routes.php */