<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Manage extends RO_Controller {
    
    function __construct() {
        parent::__construct();
        
        $this->load->library('migration');
    }
    
    
    function index() {
        
    }

    function generateUsers() {
        $hotels = $this->hotel->findAll(array("user_id" => 0, "email !=" => ""));

        foreach($hotels as $hotel) {
            $user = new User();
            $user->email = $hotel['email'];

            $token = md5(time() . $hotel['title'] . rand(1, 9999));
            $user->token = $token;

            $uid = $user->save();

            $h = new Hotel();
            $h->id = $hotel['id'];
            $h->user_id = $uid;
            $h->save();

            echo $hotel['email'] . ";http://rusotels.ru/token/" . $token . "\n";
        }
    }

    function drop_broken() {
        $photos = $this->hotel_photo->findAll();
        foreach ($photos as $photo) {
            $filename = BASEPATH . "../upload/hotel_photos/" . $photo['photo'];
            if(!file_exists($filename)) {
                $this->hotel_photo->remove($photo['id']);
            }
        }
    }

    function drop_vertical() {
        $photos = $this->hotel_photo->findAll();
        foreach ($photos as $photo) {
            $filename = BASEPATH . "../upload/hotel_photos/" . $photo['photo'];
            if(file_exists($filename)) {
                $size = getimagesize($filename);
                if($size[1] > $size[0]) {
                    $this->hotel_photo->remove($photo['id']);
                    echo $filename . "[".$size[0].":".$size[1]."] \n";
                    unlink($filename);
                }
            }
        }
    }

    function cleanup_photo() {
        $hotels = $this->hotel->findAll(array("published" => 1, "is_client" => 0));
        foreach ($hotels as $hotel) {
            $photos = $this->hotel_photo->findAll(array("hotel" => $hotel['id']));
            if(count($photos) > 3) {
                for($i = 0; $i < count($photos) - 3; $i++) {
                    $this->hotel_photo->remove($photos[$i]['id']);
                    $filename = BASEPATH . "../upload/hotel_photos/" . $photos[$i]['photo'];

                    unlink($filename);
                }
            }
        }
    }

    function migrate($type = 'info', $num = NULL) {
        $num = intval($num);
        switch ($type) {
            case 'info':
                echo "Some information about migrations\n";
                break;
            case 'latest':
                if (!$this->migration->latest()) {
                    echo "\n" . strip_tags($this->migration->error_string()) . "\n\n";
                } else {
                    echo "migration finished! \n";
                }
                break;
            case 'to': 
                if( $num > 0) {
                    if (!$this->migration->version($num)) {
                        echo "\n" . strip_tags($this->migration->error_string()) . "\n\n";
                    }
                } else {
                    echo "wrong parameter <revision: $num>\n";
                }
                break;
        }
    }

    function add_tmp_photos() {
        foreach($this->hotel->findAll() as $h) {
            $photos = $this->hotel_photo->findAll(array('hotel' => $h['id']));
            if(!$photos) {
                $photo = new Hotel_Photo();
                $photo->hotel = $h['id'];
                $photo->photo = 'tmp_' . rand(1, 49) . ".jpg";

                $photo->save();
            }
        }
    }

    function cut($last) {
        $k = 0;
        foreach (glob("/tmp/hotel_photos/*.jpg") as $filename) {

            if($k < $last) { $k++; continue; }

            $image = imagecreatefromjpeg($filename);

            $width = imagesx($image);
            $height = imagesy($image);

            $thumb = imagecreatetruecolor( $width, $height - 10 );

            imagecopyresampled($thumb,
                $image,
                0, // Center the image horizontally
                0, // Center the image vertically
                0, 0,
                $width, $height - 10,
                $width, $height - 10);

            imagejpeg($thumb, $filename, 80);

            echo ++$k . "\n";
        }
    }

    function crawlPhotos() {
        $file = fopen("hotels.json", "r");

        $k = 0;
        while($hotel = json_decode(fgets($file))) {

            if($k < 5448) { $k++; continue; }

            if(isset($hotel->photos)) {
                foreach($hotel->photos as $photo) {
                    $data = file_get_contents("http://otels.ru/" . $photo);

                    preg_match_all("/[0-9]+/", $photo, $parts);

                    $name = "/tmp/hotel_photos/" . $parts[0][0] . "_" . $parts[0][1] . ".jpg";

                    file_put_contents($name, $data);
                }
            }
            echo ++$k . "\n";
        }

    }

    function crawl() {

        $file = fopen("/tmp/hotels.json", "w");


        $k = 0;

        foreach($this->crawlCities() as $city) {

            $hotels = $this->crawlHotels($city['link']);

            foreach ($hotels as $item) {

                $hotel = $this->crawlHotel($item['link']);
                $hotel['title'] = $item['hotel'];

                $c = $this->city->find(array("%name" => $city['city']));
                if($c) {
                    $hotel['city'] = $c['id'];
                }

                $data = json_encode($hotel);

                fputs($file, $data . "\n");

                $k++;

                echo $k . "\n";
            }

        }

        fclose($file);
    }

    function crawlHotel($uri = "") {

        $raw = file_get_contents("http://otels.ru/" . $uri);
        $raw = mb_convert_encoding($raw, "utf-8", "windows-1251");

        preg_match_all('/<td width="511" [^>]+>([\W\w]*)<\/td>[\s]*<td width="1"/', $raw, $data);

        $data = mb_substr($data[1][0], mb_strpos($data[1][0], "</h1>") + 5);

        $sections = explode("<h2>", html_entity_decode($data));

        $hotel = array();

        foreach($sections as $key => &$section) {
            $pos =  mb_strpos($section, "</h2>");

            $raw_title = trim(mb_substr($section, 0, $pos));

            if($raw_title == "") {
                $rows = explode("<br>", $section);

                foreach($rows as $row) {
                    $key_value = explode(":", strip_tags($row));
                    if(count($key_value) > 1) {
                        $key = trim($key_value[0]);
                        $value = trim($key_value[1]);

                        if($key != "") {

                            if($key == "Тип") {
                                $type = $this->hotel_type->find(array("%type" => mb_strtolower($value)));
                                if($type) {
                                    $hotel['type'] = $type['id'];
                                }
                            }

                            if($key == "Номеров") {
                                $hotel['room_count'] = $value;
                            }

                            if($key == "E-mail") {
                                $hotel['email'] = $value;
                            }

                            if(mb_strpos($key, "Телефон") !== FALSE) {
                                $hotel['phone'] = $value;
                            }

                            if(mb_strpos($key, "Адрес") !== FALSE) {
                                $hotel['address'] = $value;
                            }

                        }
                    }
                }

            } elseif(mb_strpos($raw_title, "О гостинице") !== FALSE) {

                $hotel['content'] = strip_tags(mb_substr($section, $pos + 5));

            } elseif(mb_strpos($raw_title, "Фотографии") !== FALSE) {
                preg_match_all('/<a href="([^"]+)"/', $section, $images);
                $hotel['photos'] = $images[1];
            } elseif(mb_strpos($raw_title, "Цены") !== FALSE) {
                preg_match_all('/\d\d\d+/', $section, $prices);

                if(is_array($prices[0]) && count($prices[0]) > 0) {
                    $hotel['price_min'] = min($prices[0]);
                    $hotel['price_max'] = max($prices[0]);
                }
            }
        }

        return $hotel;
    }

    function crawlHotels($uri = "") {
        $raw = file_get_contents("http://otels.ru/" . $uri);
        $raw = mb_convert_encoding($raw, "utf-8", "windows-1251");

        preg_match_all('/<b><a href="([0-9]+\.htm)"\stitle="[^"]+">([^<]+)</', $raw, $links);

        $hotels = array();

        foreach($links[1] as $key => $value ) {
            $hotels[] = array("hotel" => html_entity_decode(mb_strtolower($links[2][$key])), "link" => $value);
        }

        return $hotels;
    }

    function crawlCities() {
        $raw = file_get_contents("http://www.otels.ru/");
        $raw = mb_convert_encoding($raw, "utf-8", "windows-1251");

        preg_match_all('/<a href="([^"]+)"\stitle="[^>]+">([^<]+)</', $raw, $links);

        $cities = array();

        foreach($links[1] as $key => $value ) {
            $cities[] = array("city" => $links[2][$key], "link" => $value);
        }

        return $cities;
    }

    function exportHotelsToLB() {
        $hotels = $this->hotel->findAll(array('published' => 1, 'lb_id' => ''));

        foreach($hotels as $hotel) {

            $data = array();
            $data['title'] = $hotel['title'];
            $data['description'] = $hotel['content'];
            $data['stars'] = intval($hotel['rank']);
            $data['total_rooms'] = intval($hotel['room_count']);

            $types = $this->hotel->get_type($hotel['id']);
            $data['types'] = array();
            foreach($types as $item) {
                $data['types'][] = $item['type'];
            }

            $city = $this->city->find(array('id' => $hotel['city_id']));
            $data['city'] = array();
            $data['city']['name'] = $city['name'];

            $region = $this->region->find(array('id' => $city['region_id']));
            $data['city']['region'] = $region['name'];

            $photos = $this->hotel_photo->findAll(array('hotel' => $hotel['id']));
            $data['photos'] = array();
            foreach($photos as $photo) {
                $data['photos'][] = 'http://rusotels.ru/cache/thumbs/hotels/113x72/' . $photo['photo'];
            }

            $ch = curl_init('http://127.0.0.1:3000/hotel');
            curl_setopt_array($ch, array(
                CURLOPT_POST => TRUE,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json'
                ),
                CURLOPT_POSTFIELDS => json_encode($data)
            ));

            $response = curl_exec($ch);

            if($response === FALSE){
                echo curl_error($ch);
            } else {

                $res = json_decode($response);
                if(isset($res->id)) {
                    $h = new Hotel();
                    $h->id = $hotel['id'];
                    $h->lb_id = $res->id;
                    $h->save();
                }

                echo $response;
            }



            echo "\n\n ===== \n\n";

//            echo json_encode($data) . "\n\n ==== \n\n";
        }
    }
}
