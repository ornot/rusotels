<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Catalog extends RO_Controller {

	public function info($city_alias, $hotel_alias='none') {
        
         $this->load->model('metateg_model');

        $city = $this->city->find(array("alias" => $city_alias));

        $hotel = array();
        if(isset($city["id"])) {
            $hotel = $this->hotel->find(array("alias" => $hotel_alias, "city_id" => $city["id"]));
        }

        if(isset($hotel['title'])) {

            if(!isset($_SESSION['viewed_hotels'])) {
                $_SESSION['viewed_hotels'] = array();
            }

            $viewed = $_SESSION['viewed_hotels'];

            $photos = $this->hotel_photo->findAll(array('hotel' => $hotel['id']), array(), array('position', 'asc'));
            $viewed[$hotel["id"]] = array(
                'uri' => $city_alias . "/" .$hotel_alias,
                'name' => $hotel['title'],
                'photo' => isset($photos[0])? $photos[0]['photo'] : "http://placehold.it/58x50",
                'stars' => $hotel['rank'],
                'city' => $city['name'],
                'price' => $hotel['price_min'],
            );

            if(count($viewed) > 4) {
                array_shift($viewed);
            }

            $_SESSION['viewed_hotels'] = $viewed;

            $data['hotel'] = $hotel;
            $data['photos'] = $this->hotel_photo->findAll(array("hotel" => $hotel['id']), array(), array('position', 'asc'));

            foreach($this->service->findAll(array("hotel" => $hotel['id'], "status" => 1)) as $service) {
                $data['services'][$service['type']] =  $service;
            }

            if(isset($data['services'][1])) {
                $data['rooms_widget'] = file_get_contents('http://api.litebooking.ru/widgets/rooms/?hotel=' . $hotel['lb_id']);
                //костыль на лайтбукинг
                $data['rooms_widget']= str_replace("lite-form-width","lite_form_width", $data['rooms_widget']);
            }

            /**
            $types = "";
            foreach($this->hotel->get_type($hotel['id']) as $type) {
                $types .= $type['type'] . " ";
            }
            **/
            
            $tipes_arr=$this->hotel->get_type($hotel['id']);

            $data['city'] = $city;
            $data['offers'] = $this->catalog_model->getRandomOffers(3, $city['id']);
            $data['viewed_hotels'] = isset($_SESSION['viewed_hotels'])?$_SESSION['viewed_hotels'] : array();

            //$this->title = 'Забронировать номер в гостинице - ' . trim($types) . ' в ' . $city['name'] . ' цена, телефон, отзывы - ' . $data['hotel']['title'];
            //$this->keywords = $hotel['keywords'];
            //$this->description = $hotel['description'];
           
            //страница отеля ({москва} {москвы} {москве} {тип} {имя})
            $this->title = $this->metateg_model->title('catalog', $city['name'] ,$tipes_arr,$data['hotel']['title']);
            $this->description = $this->metateg_model->description('catalog', $city['name'] ,$tipes_arr,$data['hotel']['title']);
            $this->keywords = $this->metateg_model->keywords('catalog', $city['name'] ,$tipes_arr,$data['hotel']['title']);
            
            
            //переопределить заголовок по полю в отеле минуя шаблон
            if( $hotel['meta_title']!==""){
                $this->title=$hotel['meta_title'];
            } 
            // ключи в отеле
            if( $hotel['keywords']!==""){
               $this->keywords=$hotel['keywords'];
            }
            
            //метатег в отеле
            if( $hotel['description']!==""){
                $this->description=$hotel['description'];
            } 

            $this->render('hotel_view', $data);
        } else {
            $type = $this->hotel_type->find(array('alias' => $hotel_alias));

            if($type) {

                $this->breadcrumbs = array(
                    array('title' => 'Каталог отелей', 'link' => '/'),
                    array('title' => $city['name'], 'link' => '/hotels/' . $city['alias']),
                    array('title' => $type['type'], 'link' => '/hotels/' . $city['alias'] . '/' . $type['alias']),
                );

                //$this->title = "Снять " . $type['type'] . " в " . $city['name'] . " цена, телефон, отзывы, фото - Rusotels.ru ";
                //$this->description = "Забронировать " . $type['type'] . " в " . $city['name'] . " недорого, Вы можете по телефону. Отель можно снять на сутки или на ночь. Только у нас, Вы сможете найти лучшие хостелы по вкусу и карману";
                
                //страница отеля с незаполненым названием отеля ({москва} {москвы} {москве} {тип})
                $this->title = $this->metateg_model->title('catalog2', $city['name'] , $type['type']);
                $this->description = $this->metateg_model->description('catalog2', $city['name'] , $type['type']);
                $this->keywords = $this->metateg_model->keywords('catalog2', $city['name'] , $type['type']);
                
                
                $this->db->join('hotel_to_type', 'hotel_to_type.hotel = hotels.id');
                $this->db->where('hotel_to_type.type = ' . $type['id']);
                $data['hotels'] = $this->hotel->findAll(array('published' => 1, 'city_id' => $city['id']));
                $data['viewed_hotels'] = isset($_SESSION['viewed_hotels'])?$_SESSION['viewed_hotels']:array();
                $data['offers'] = $this->catalog_model->getRandomOffers(3);
                $data['type'] = $type;
                $data['city'] = $city;
                $data['types'] = array();

                $this->db->join('hotel_to_type', 'hotel_to_type.type = hotel_types.id');
                $this->db->join('hotels', 'hotel_to_type.hotel = hotels.id');
                $this->db->where('hotels.city_id = '. $city['id']);
                $this->db->group_by('hotel_types.id');
                $this->db->select('hotel_types.*');

                foreach($this->hotel_type->findAll(array('active' => 1), array(), array('position', 'asc')) as $item) {

                    $data['types'][$item['id']] = $item;
                }
                foreach($data['hotels'] as &$hotel) {
                    $hotel['photos'] = $this->hotel_photo->findAll(array('hotel' => $hotel['id']), array(), array('position', 'asc'));
                    $hotel['type'] = $this->hotel->get_type($hotel['id']);
                }

                $data['links_view'] = true;

                $this->render('search_view', $data);
            } else {
                show_404();
            }
        }
    }

    public function go($hid) {
        $hotel = $this->hotel->find(array('id' => $hid));
        if($hotel && $hotel['site']) {
            redirect('http://'.$hotel['site']);
        } else {
            show_404();
        }
    }

    public function cities($type = 'alphabetic') {
        $data['cities'] = array();

        $data['type'] = $type;

        if($type == 'regions') {
            foreach($this->city->findAll(array(), array(), array('name', 'asc')) as $city) {
                if($this->hotel->count(array('city_id' => $city['id'], 'published' => 1))) {
                    $region = $this->region->find(array('id' => $city['region_id']));

                    if(!isset($data['cities'][$region['name']])) {
                        $data['cities'][$region['name']] = array();
                    }

                    $data['cities'][$region['name']][] = $city;
                    ksort($data['cities']);
                }
            }
        } else {
            foreach($this->city->findAll(array(), array(), array('name', 'asc')) as $city) {
                if($this->hotel->count(array('city_id' => $city['id'], 'published' => 1))) {

                    if(!isset($data['cities'][strtoupper(mb_substr($city['name'], 0, 1))])) {
                        $data['cities'][strtoupper(mb_substr($city['name'], 0, 1))] = array();
                    }

                    $data['cities'][strtoupper(mb_substr($city['name'], 0, 1))][] = $city;
                }
            }
        }


        $this->render('catalog/cities', $data);

    }

}