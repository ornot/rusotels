<?php

class Ajax extends RO_Controller {

    public function remove_viewed($id = NULL) {
        if($id) {
            unset($_SESSION['viewed_hotels'][$id]);
        } else {
            $_SESSION['viewed_hotels'] = array();
        }
    }

    public function search() {
        if(isset($_GET['query'])) {
            $q = $this->db->query('SELECT c.name, count(h.id) as num FROM citys c INNER JOIN hotels h ON c.id = h.city_id WHERE c.name LIKE "%'.$this->db->escape_like_str($_GET['query']).'%" GROUP BY c.id ORDER BY num DESC LIMIT 5');

            $res = array();
            foreach($q->result_array() as $city) {
                $res[] = $city['name'];
            }

            $this->db->select('title');
            $this->db->like('title', $_GET['query']);
            $this->db->where('active', 1);
            $this->db->limit(5);
            $q = $this->db->get('hotel_revisions');

            foreach($q->result_array() as $hotel) {
                $res[] = $hotel['title'];
            }

            die(json_encode($res));
        }
    }
}