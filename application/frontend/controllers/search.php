<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Search extends RO_Controller
{

    public function index($city_alias = null) {
        
        $this->load->model('metateg_model');
        

        $data['query_string'] = $_SERVER['QUERY_STRING'];

        if(isset($_GET['date_in'])){
            $_SESSION['date_in'] = $_GET['date_in'];
        }

        if(isset($_GET['date_out'])){
            $_SESSION['date_out'] = $_GET['date_out'];
        }

        if(isset($_GET['page'])) {
            $page = $_GET['page'];
            $data['query_string'] = str_replace(array("&page=".$page, "page=".$page), array("", ""),  $data['query_string']);
            $data['query_string'] = str_replace(array("?&", "&&"), array("?", "&"),  $data['query_string']);
        } else {
            $page = 1;
        }

        $sort = isset($_GET['sort'])?array($_GET['sort'], 'desc'):array('id', 'desc');

        $filter = array('published' => 1);
        if(isset($_GET['filter'])) {
            foreach($_GET['filter'] as $key => $value) {
                if($key == "type") {
                    continue;
                }

                if($value) {
                    if(is_array($value)) {
                        $filter['@'.$key] = $value;
                    } else {
                        $filter[$key] = $value;
                    }
                }
            }
        }

        $data['viewed_hotels'] = isset($_SESSION['viewed_hotels'])?$_SESSION['viewed_hotels']:array();
        $data['offers'] = $this->catalog_model->getRandomOffers(3);

        $data['types'] = array();
        foreach($this->hotel_type->findAll(array('active' => 1), array(), array('position', 'asc')) as $item) {
            $data['types'][$item['id']] = $item;
        }

        if($city_alias) {
            $data['city'] = $this->city->find(array('alias' => $city_alias));

            $filter['city_id'] = $data['city']['id'];

            //$this->title = 'Снять гостиницу в ' . $data['city']['name'] . ' цена, телефон, отзывы, фото - Rusotels.ru';
            //$this->description = 'Забронировать гостиницу в ' . $data['city']['name'] . ' недорого Вы можете по телефону. Отель можно снять на сутки или на ночь. Только у нас, Вы сможете найти лучший отель по вкусу и карману.'; 
            
            //страницы формируются при поиске отелей по городу({москва} {москвы} {москве})
            $this->title = $this->metateg_model->title('search',$data['city']['name']);
            $this->description = $this->metateg_model->description('search',$data['city']['name']);
            $this->keywords = $this->metateg_model->keywords('search',$data['city']['name']);
                
                
            $this->breadcrumbs = array(
                array('title' => 'Каталог отелей', 'link' => '/'),
                array('title' => $data['city']['name'], 'link' => '/hotels/' . $data['city']['alias']),
            );

            $data['links_view'] = true;
            $this->db->join('hotel_to_type', 'hotel_to_type.type = hotel_types.id');
            $this->db->join('hotels', 'hotel_to_type.hotel = hotels.id');
            $this->db->where('hotels.city_id = '. $data['city']['id']);
            $this->db->group_by('hotel_types.id');
            $this->db->select('hotel_types.*');

            $data['types'] = array();
            foreach($this->hotel_type->findAll(array('active' => 1), array(), array('position', 'asc')) as $item) {
                $data['types'][$item['id']] = $item;
            }

            $data['hotels_by_city'] = $this->hotel->findAll($filter, array("start" => (($page - 1) * 20), "count" => 20),  "hotels.weight desc, hotels." . $sort[0] . " " . $sort[1]);

            foreach($data['hotels_by_city'] as &$hotel) {
                $hotel['photos'] = $this->hotel_photo->findAll(array('hotel' => $hotel['id']), array(), array('position', 'asc'));
                $hotel['type'] = $this->hotel->get_type($hotel['id']);
            }
        } elseif(isset($_GET['q'])) {
            $query = urldecode($_GET['q']);
            $city = $this->city->find(array('name' => $query)); //получаем город по запросу

            $data['hotels_by_title'] = $this->hotel->findAll(array('%title' => $query), array(),  "hotels.weight desc, hotels." . $sort[0] . " " . $sort[1]);

            foreach($data['hotels_by_title'] as &$hotel) {
                $hotel['photos'] = $this->hotel_photo->findAll(array('hotel' => $hotel['id']), array(), array('position', 'asc'));
                $hotel['type'] = $this->hotel->get_type($hotel['id']);
            }

            if ($city) {

                $data['city'] = $city; //получаем город по алиасу из урл

                $filter['city_id'] = $city['id'];

               // $this->title = 'Снять гостиницу в ' . $data['city']['name'] . ' цена, телефон, отзывы, фото - Rusotels.ru';
               // $this->description = 'Забронировать гостиницу в ' . $data['city']['name'] . ' недорого Вы можете по телефону. Отель можно снять на сутки или на ночь. Только у нас, Вы сможете найти лучший отель по вкусу и карману.';
                
            //страницы формируются при поиске отелей по городу({москва} {москвы} {москве})
            $this->title = $this->metateg_model->title('search',$data['city']['name']);
            $this->description = $this->metateg_model->description('search',$data['city']['name']);
            $this->keywords = $this->metateg_model->keywords('search',$data['city']['name']);
                
                $data['hotels_by_city'] = $this->hotel->findAll($filter, array("start" => (($page - 1) * 20), "count" => 20),  "hotels.weight desc, hotels." . $sort[0] . " " . $sort[1]);

                foreach($data['hotels_by_city'] as &$hotel) {
                    $hotel['photos'] = $this->hotel_photo->findAll(array('hotel' => $hotel['id']), array(), array('position', 'asc'));
                    $hotel['type'] = $this->hotel->get_type($hotel['id']);
                }
            }

        }


        $data['total'] = $this->hotel->count($filter);

        $this->render('search_view', $data);
    }
}