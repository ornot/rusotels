<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Signup extends RO_Controller {
   

    public function index() {

        $this->load->helper(array('form', 'url'));
        
        $data['post'] = $_POST;

        $data['types'] = $this->hotel_type->findAll(array('active' => 1), array(), array('position', 'asc'));
        $data['citys'] = $this->city->findAll(array(), array(), array('name', 'asc'));

        foreach($data['citys'] as &$city) {
            $city['region'] = $this->region->find(array('id' => $city['region_id']));
        }
        
        $data['validate']='';
        //в названии отеля допустимы только(цифры, буквы, тире, пробелы, подчёркивания)
        if(isset($_POST['hotel']['title'])){
           $data['validate']=$this->_hotelname_validate($_POST['hotel']['title']);
           $is_valid=false;
           if((string)$data['validate']===''){ $is_valid=true;}
        }
         
        if(isset($_POST['user']) && isset($_POST['hotel']) &&  $is_valid) {
            $user = new User();
            $hotel = new Hotel();
            
            if($user->validate("user", "register") && $hotel->validate("hotel", "register") ) {
                $user->status = 1;
                $user->activated = time();
                $uid = $user->save();

                if($uid) {
                    $hotel->user_id = $uid;
                    $hotel->published = 0;

                    $alias = convert_accented_characters(mb_strtolower($_POST['hotel']['title']));
                    $alias = str_replace(' ', '-', $alias);

                    if($existing = $this->hotel->find(array('%alias' => $alias), array('count' => 1, 'start' => 1), array('alias', 'desc'))) {
                        $parts = explode('-', $existing);
                        if(count($parts) > 1 && is_int($parts[count($parts) - 1])) {
                            $num = intval($parts[count($parts) - 1]) + 1;
                            $alias = $alias . "-" . $num;
                        } else {
                            $alias = $alias . "-1";
                        }
                    }

                    $hotel->alias = $alias;

                    if(isset($_POST['phone']['code']) && isset($_POST['phone']['num'])) {
                        $hotel->phone = $_POST['phone']['code'] . "|" . $_POST['phone']['num'];
                    }

                    if(isset($_POST['addr']['street']) && isset($_POST['addr']['house']) && isset($_POST['addr']['build'])) {
                        $hotel->address = $_POST['addr']['street'] . "|" . $_POST['addr']['house'] . "|" . $_POST['addr']['build'];
                    }

                    $hid = $hotel->save();

                    if(isset($_FILES['certificate']) && $_FILES['certificate']['name'] != "") {
                        $certificate_path = BASEPATH . "../upload/certificates/";

                        move_uploaded_file($_FILES['certificate']["tmp_name"], $certificate_path . $_FILES['certificate']["name"]);

                        $hotel = new Hotel();
                        $hotel->id = $hid;
                        $hotel->certificate = $_FILES['certificate']["name"];
                        $hotel->save(1);

                        $notice = new Notice();
                        $notice->hotel = $hid;
                        $notice->user = $uid;
                        $notice->status = 1;
                        $notice->created = time();
                        $notice->type = 103;

                        $notice->save();
                    }

                    if(isset($_POST['hotel']['type']) && $hid) {
                        $hotel->add_types($_POST['hotel']['type'], $hid);
                    }

                    $notice = new Notice();
                    $notice->hotel = $hid;
                    $notice->user = $uid;
                    $notice->status = 1;
                    $notice->created = time();
                    $notice->type = 201;

                    $notice->save();

                    $this->email->clear();

                    $this->email->to($_POST['user']['email']);
                    $this->email->from('system@rusotels.ru');
                    $this->email->subject('Регистрация на портале Rusotels.ru');
                    $this->email->message("Уважаемый отельер! \n
                                            Благодарим Вас за регистрацию на сайте Rusotels.ru \n
                                            Ваша заявка отправлена на модерацию для проверки и будет рассмотрена в течении 2-5 часов. \n
                                            С уважением, команда Rusotels.ru \n");
                    $this->email->send();

                    redirect('/signup/success');
                }
            }
        }

		$this->render('signup_view', $data);
    }

    public function login() {

        $data = array();

        if(isset($_POST['email']) && isset($_POST['password'])) {
            $user = $this->user->find(array('email' => $_POST['email'], 'password' => md5($_POST['password'])));
            if($user && $user['id']) {
                $_SESSION['user'] = $user;

                $hotel = $this->hotel->find(array('user_id' => $user['id']));

                if($hotel) {
                    $_SESSION['user']['hotel'] = $hotel;
                }

                $user = new User();
                $user->id = $_SESSION['user']['id'];
                $user->last_active = time();
                $user->save();

                redirect('/panel');
            } else {
                $data['error'] = 'Неверный e-mail или пароль';
            }
        }

        $this->render('login_view', $data);
    }

    public function tour() {
        $post=$this->input->post();
		
        $data['validate']='';  
        if($post){
			$data=$post;
			$data['validate']=$this->_valid_tour($post);
			if((string)$data['validate']===''){

			  
                //отсылаем сообщение админу
				
                $msg='tour  '.
                ' '.$data['name'].
                ' '.$data['organization'].
                ' '.$data['email'].
                ' '.$data['code'].
                ' '.$data['num'].
                ' '.$data['content'];   
			  

                $message = new Message();
                $message->user =0;
                $message->message = $msg;
                $message->date = time();
                $message->type = 2;
                $message->save();
                    

                //письмо
                $this->email->clear();
                $this->email->to(array("skorbenko@digitalwill.ru", "support@rusotels.ru", "ornot.work@gmail.com","ribets@digitalwill.ru"));
                //$this->email->to(array("ribets@digitalwill.ru"));//для тестового сервера
                $this->email->from('system@rusotels.ru');
                $this->email->subject('Новая туристическая фирма на Rusotels');
                $this->email->message("Зарегистрирована новая  туристическая фирма: " . $msg) ;
                $this->email->send();


				//
				$data['msg'] = "Спасибо за регистрацию.";
				$data['color'] = "confirm-message";
				$this->render('alert_view', $data);
			}
			else{
				$this->render('tour',$data);
			}
        }   
        else{
            $data['name']=''; 
            $data['organization']='';  
            $data['email']=''; 
            $data['code']=''; 
            $data['num']=''; 
            $data['content']='';
			
            $this->render('tour',$data);			
        }
    }


       

    public function corp() {
        $post=$this->input->post();
		
        $data['validate']='';  
        if($post){
			$data=$post;
			$data['validate']=$this->_valid_corp($post);
			if((string)$data['validate']===''){

             
                //отсылаем сообщение админу
               
                $msg='corp  '.
                ' '.$data['name'].
                ' '.$data['organization'].
                ' '.$data['email'].
                ' '.$data['code'].
                ' '.$data['num'].
                ' '.$data['content'];   

                $message = new Message();
                $message->user =0;
                $message->message = $msg;
                $message->date = time();
                $message->type = 2;
                $message->save();
                    

                //письмо
                $this->email->clear();
                $this->email->to(array("skorbenko@digitalwill.ru", "support@rusotels.ru", "ornot.work@gmail.com","ribets@digitalwill.ru"));
                //$this->email->to(array("ribets@digitalwill.ru"));//для тестового сервера
                $this->email->from('system@rusotels.ru');
                $this->email->subject('Новый клиент на Rusotels');
                $this->email->message("Зарегистрирован новый корпоративный клиент: " . $msg) ;
                $this->email->send();

                //
				$data['msg'] = "Спасибо за регистрацию.";
				$data['color'] = "confirm-message";
				$this->render('alert_view', $data);
			}
			else{
				$this->render('corp',$data);
			}
        }   
        else{
            $data['name']=''; 
            $data['organization']='';  
            $data['email']=''; 
            $data['code']=''; 
            $data['num']=''; 
            $data['content']='';
			
            $this->render('corp',$data);			
        }
    }

    public function logout() {
        if(isset($_SESSION['user'])) {
            unset($_SESSION['user']);
        }

        redirect('/');
    }

    public function recovery() {
        if(isset($_POST['email'])) {
            $u = $this->user->find(array('email' => $_POST['email']));

            if(isset($u['id'])) {
                $user = new User();
                $user->id = $u['id'];

                $token = md5(time());

                $user->token = $token;

                $user->save();

                $config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'communi.digitalwill.private',
                    'smtp_port' => 25,
                    'mailtype'  => 'html',
                    'charset'   => 'utf-8'
                );

                $this->email->initialize($config);

                $this->email->clear();

                $this->email->to($u['email']);
                $this->email->from('system@rusotels.ru');
                $this->email->subject('Востановление пароля Rusotels');
                $this->email->message("для смены пароля перейдите по ссылке - http://rusotels.ru/token/".$token);
                $this->email->send();


                $data['msg'] = "На указанный e-mail была отправленна ссылка для изменения пароля.";
                $data['color'] = "confirm-message";

                return $this->render('alert_view', $data);
            }
        }

        $this->render('recovery');
    }

    public function success() {
        $data['msg'] = "Спасибо за регистрацию, теперь Вы можете войти в свой личный кабинет воспользовавшить <a href='/login'>формой авторизации</a>";
        $data['color'] = "confirm-message";

        $this->render('alert_view', $data);
    }

    public function token($token) {
        $user = $this->user->find(array('token' => $token));
        if($user) {
            if(isset($_POST['password'])) {

                $u = new User();
                $u->id = $user['id'];
                $u->token = "";
                $u->password = md5($_POST['password']);

                if($user['status'] == 0) {
                    $u->status = 1;
                    $u->activated = time();
                }

                $u->save();

                $data['msg'] = "Пароль был успешно изменен, теперь вы можете войти в свой личный кабинет воспользовавшить <a href='/login'>формой авторизации</a>";
                $data['color'] = "confirm-message";

                return $this->render('alert_view', $data);
            }

            return $this->render('change_pass');

        } else {
            $data['msg'] = "вы уже переходили по этой ссылке и создали свой пароль. теперь воспользуйтесь  <a href='/login'>формой авторизации</a> для входа на сайт используя свой логин (e-mail) и пароль";
            $data['color'] = "confirm-message";

            return $this->render('alert_view', $data);
        }

    }

    private function _valid_tour($dat) {
        $str='';      
        if( (string)$dat['name']===''){  
             $str= ' Пожалуйста , введите имя контактного лица. ';
        }
		if( (string)$dat['organization']===''){  
             $str= ' Пожалуйста , введите название организации. ';
        }
		$flag=filter_var($dat['email'], FILTER_VALIDATE_EMAIL);
		if(!$flag){  
             $str=' Пожалуйста ,  введите  корректный E-mail. ';
        }
		if( (string)$dat['code']===''){  
             $str= ' Пожалуйста , введите код телефона  . ';
        }
		if( (string)$dat['num']===''){  
             $str= ' Пожалуйста , введите номер телефона . ';
        }
		
        return $str;
    }
    
    private function _valid_corp($dat){
        $str='';      
        if( (string)$dat['name']===''){  
             $str= ' Пожалуйста , введите имя контактного лица. ';
        }
        $flag=filter_var($dat['email'], FILTER_VALIDATE_EMAIL);
        if(!$flag){  
             $str=' Пожалуйста ,  введите  корректный E-mail. ';
        }
        if( (string)$dat['code']===''){  
             $str= ' Пожалуйста , введите код телефона  . ';
        }
        if( (string)$dat['num']===''){  
             $str= ' Пожалуйста , введите номер телефона . ';
        }
        
        return $str;
    }
    
    private function _hotelname_validate($str){
        //тире
        $str=str_replace('-','x', $str);
        //буквы цыфры пробелы подчёркивания
	if (preg_match("/[^(\w)|(\x7F-\xFF)|(\s)]/",$str))         
	{
            
            return "В названии отеля можно вводить только буквы, цифры, пробелы, тире и символ подчёркивания.";
	}
	else
	{
          return "";  
	}
        
        
    }
    
   
 
    
    
    
}