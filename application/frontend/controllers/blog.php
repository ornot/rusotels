<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends RO_Controller {

	public function index() {

		$data['posts'] = $this->post->findAll();
		
		$this->render('posts_list_view', $data);
	}

    public function view($alias) {

        $data['page'] = $this->post->find(array('alias' => $alias));

        $this->render('page_view', $data);
    }
}