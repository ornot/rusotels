<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends RO_Controller {

	public function show($keyx) {
		$data['page'] = $this->page->find(array('keyx' => $keyx));

        if(!$data['page']) {
            show_404();
        }

		$this->render('page_view', $data);
	}
}