<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends RO_Controller {

	public function index() {
            
        $this->load->model('metateg_model');
        
           
        //$this->title = "Снять номер в гостинице, хостеле. Самостоятельно забронировать отель. Узнать цену, посмотреть фото и отзывы на Rusotels.ru";
        
        //$this->description = "На rusotels Вы найдете список отелей, каталог гостиниц их адреса и телефоны. У нас Вы сможете самостоятельно забронировать номер в мини-отеле, или место в хостеле. Снять номер в гостинице можно на сайте самостоятельно.";
        
        //главная страница (без шаблонов)
        $this->title = $this->metateg_model->title('main');
        $this->description = $this->metateg_model->description('main');
        $this->keywords = $this->metateg_model->keywords('main');
        
        
		$data['offers'] = $this->catalog_model->getRandomOffers();

        $data['viewed_hotels'] = isset($_SESSION['viewed_hotels'])?$_SESSION['viewed_hotels']:array();

        $data['top_cities'] = $this->catalog_model->getTopCitiesWithTypes();

        $data['types'] = array();
        foreach($this->hotel_type->findAll(array('active' => 1)) as $item) {
            $data['types'][$item['id']] = $item;
        }

        $data['count'] = $this->hotel->count(array('published' => 1));

        $data['banners'] = $this->banner->findAll(array('active' => 1));
        foreach($data['banners'] as &$banner) {
            if($banner['hotel'] > 0) {
                $banner['hotel'] = $this->hotel->find(array('id' => $banner['hotel']));
                $banner['hotel']['city'] = $this->city->find(array('id' => $banner['hotel']['city_id']));
            }
        }

		$this->render('start_view', $data);
	}
}