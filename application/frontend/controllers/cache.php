<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Aws\S3\S3Client;

class Cache extends RO_Controller {

    public function thumbs($size, $image) {

        $allowed_sizes = array('54x46', '113x72', '188x130', '205x145', '240x145', '230x170', '650x380');
        $allowed_types = array('image/jpeg', 'image/png', 'binary/octet-stream');

        $file = 'https://s3.eu-central-1.amazonaws.com/rusotels/hotel_photos/' . $image;

        $raw = get_headers($file);

        $headers = array();

        foreach($raw as $field) {
            $parts = explode(':', $field);
            if(count($parts) > 1) {
                $key = array_shift($parts);
                $headers[$key] = implode(':', $parts);
            } else {
                $headers[] = $field;
            }
        }

        if(in_array($size, $allowed_sizes)) {
//            $type = mime_content_type($file);

            $type = 'image/jpeg';

            $src = imagecreatefromstring(file_get_contents($file));

            $size_array = explode('x', $size);

            $dst = imagecreatetruecolor($size_array[0], $size_array[1]);
            $white = imagecolorallocate($dst, 255, 255, 255);
            imagefill($dst, 0, 0, $white);

            $src_size = getimagesize($file);

            $src_w = $src_size[0];
            $src_h = $src_size[1];

            if($src_w > $src_h) {
                $ratio = $size_array[0] / ($src_w / 100) ;
                $new_h = ($src_h / 100) * $ratio;

                $d_y = ($size_array[1] - $new_h) / 2;

                imagecopyresized($dst, $src, 0, $d_y, 0, 0, $size_array[0], $new_h, $src_w, $src_h );

            } else {
                $ratio = $size_array[1] / ($src_h / 100) ;
                $new_w = ($src_w / 100) * $ratio;

                $d_x = ($size_array[0] - $new_w) / 2;

                imagecopyresized($dst, $src, $d_x, 0, 0, 0, $new_w, $size_array[1], $src_w, $src_h);
            }

            $client = S3Client::factory(array(
                'key'    => 'AKIAIKKAQU7NMAUAZ7VA',
                'secret' => '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',
                'region' => 'eu-central-1',
            ));

            ob_start();

            if($type == 'image/jpeg') {
                imagejpeg($dst, null, 100);
            } elseif($type == 'image/png') {
                imagepng($dst);
            }

            $imageData = ob_get_clean();

            $client->putObject(array(
                'Bucket' => 'rusotels',
                'Key' => 'cache/thumbs/hotels/'.$size.'/'.$image,
                'Body' => $imageData,
                'ACL' => 'public-read',
            ));

            redirect('/cache/thumbs/hotels/'.$size.'/'.$image);
        }
    }
}