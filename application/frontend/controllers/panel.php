<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Panel extends RO_Controller {

    public function setRules() {
        return array(
            '*' => array('user', 'admin'),
            '#fallback' => '/login',
        );
    }

    public function index() {
        $data['hotel'] = $this->hotel->latest()->find(array("user_id" => $_SESSION['user']['id']));
        $data['services'] = array();
        foreach ($this->service->findAll(array('hotel' => $data['hotel']['id'], 'status' => 1)) as $service) {
            $data['services'][$service['type']] = $service;
        }

        $this->render('panel/index', $data);
    }

    public function profile() {
        $data['hotel'] = $this->hotel->latest()->find(array("user_id" => $_SESSION['user']['id']));

        if (isset($_POST['user'])) {
            $user = new User();
            $user->id = $_SESSION['user']['id'];

            if ($user->validate("user", "update_user")) {
                $user->save();

                $_SESSION['user']['password'] = md5($_POST['user']['password']);

                $_SESSION['message'] = "Данные успешно сохранены";
            }
        }

        if (isset($_POST['requisites'])) {
            $r = $this->requisite->find(array('user' => $_SESSION['user']['id']));

            $requisites = new Requisite();
            if (isset($r['id'])) {
                $requisites->id = $r['id'];
            } else {
                $requisites->user = $_SESSION['user']['id'];
            }

            if ($requisites->validate("requisites", "default")) {
                $requisites->save();

                $_SESSION['message'] = "Данные успешно сохранены";
            }
        }

        $data['user'] = $this->user->find(array('id' => $_SESSION['user']['id']));
        $data['requisites'] = $this->requisite->find(array('user' => $_SESSION['user']['id']));

        if (isset($_SESSION['message'])) {
            $data['message'] = $_SESSION['message'];

            unset($_SESSION['message']);
        }

        $data['services'] = array();
        foreach ($this->service->findAll(array('hotel' => $data['hotel']['id'], 'status' => 1)) as $service) {
            $data['services'][$service['type']] = $service;
        }
        $this->render('panel/profile', $data);
    }

    public function hotel() {
        //если пользователь неавторизирован
        $m_user_id = $_SESSION['user']['id'];
        if (!(preg_match("/^[\d\+]+$/", $m_user_id))) {
            $data['msg'] = "Вы не авторизированы ";
            $data['color'] = "confirm-message";
            return $this->render('alert_view', $data);
        }

        if (isset($_GET['remove'])) {
            $this->hotel_photo->remove($_GET['remove']);

            redirect('/panel/hotel');
        }

        if (isset($_POST['pos'])) {
            foreach ($_POST['pos'] as $photo => $pos) {
                $p = new Hotel_Photo();
                $p->id = $photo;
                $p->position = $pos;
                $p->save();
            }
        }
        
        $h = $this->hotel->find(array("user_id" => $_SESSION['user']['id']));
        $hotel_id = $h['id'];
        
        if (isset($_POST['hotel'])) {
            $flag = false;
            $_SESSION['message'] = "Данные успешно сохранены";

          
            if (isset($h['city_id'])) {
                $h = $this->hotel->find(array("alias" => $h['alias'], "city_id" => $h['city_id']));
                if (is_int($h['id']) !== "" && is_int($h['city_id']) !== "" && is_int($h['user_id']) !== "" && (string) $h['alias'] !== "" && (string) $h['title'] !== "") {
                    $flag = true; //проверяем чтоб отель был с данными а не пустой
                }
            }

            
            $hotel = new Hotel();
            $hotel->id = $hotel_id;

            if ($hotel->validate("hotel", "register") && $flag) {

               // получаем данные немодерированной ревизии
               $r=$this->hotel_revision->get_passive($hotel_id);
                       
                $data = array();

                foreach ($_POST['hotel'] as $key => $value) {
                    if (isset($r[$key]) && trim($r[$key]) != trim($value)) {
                        $data[$key] = array(
                            'old' => $r[$key],
                            'new' => $value,
                        );
                    }
                }

                if (isset($_POST['phone']['code']) && isset($_POST['phone']['num'])) {
                    $hotel->phone = $_POST['phone']['code'] . "|" . $_POST['phone']['num'];

                    if ($_POST['phone']['code'] . "|" . $_POST['phone']['num'] != $r['phone']) {
                        $data['phone'] = array(
                            'old' => $r['phone'],
                            'new' => $_POST['phone']['code'] . "|" . $_POST['phone']['num'],
                        );
                    }
                }

                if (isset($_POST['addr']['street']) && isset($_POST['addr']['house']) && isset($_POST['addr']['build'])) {
                    $hotel->address = $_POST['addr']['street'] . "|" . $_POST['addr']['house'] . "|" . $_POST['addr']['build'];

                    if ($_POST['addr']['street'] . "|" . $_POST['addr']['house'] . "|" . $_POST['addr']['build'] != $r['address']) {
                        $data['address'] = array(
                            'old' => $r['address'],
                            'new' => $_POST['addr']['street'] . "|" . $_POST['addr']['house'] . "|" . $_POST['addr']['build'],
                        );
                    }
                }

                //echo 'count'.count($data);
                if (count($data) > 0) {
                    //смотрим есть ли уже редакция отеля на модерации
                    $flag_modifet = $this->hotel_revision->is_modifet($hotel_id);
                
                     //удаляем все тестовые ревизии отеля
                    $this->hotel_revision->del_all_passive($hotel_id);
                    
                    $hotel->save_passive(); //сохраняем новую редакцию отеля


                    if (!$flag_modifet) {
                        //отправляем новую редакцию на модерацию  
                        $notice = new Notice();
                        $notice->created = time();

                        $notice->type = 102;
                        $notice->hotel = $h['id'];
                        $notice->user = $_SESSION['user']['id'];

                        $notice->status = 1;

                        $notice->save();
                        
                    }
                }
                if (isset($_POST['hotel']['type']) && $h['id']) {
                    $hotel->add_types($_POST['hotel']['type'], $h['id']);
                }
            }

            if (isset($_FILES['photo']) && $_FILES['photo']["name"] != "" && $this->hotel_photo->count(array('hotel' => $h['id'])) < 25) {
                $photo_path = BASEPATH . "../upload/hotel_photos/";

                $ext = pathinfo($_FILES['photo']["name"], PATHINFO_EXTENSION);

                $new_name = $h['id'] . "_" . time() . '.' . $ext;

                move_uploaded_file($_FILES['photo']["tmp_name"], $photo_path . $new_name);

                $photo = new Hotel_Photo();
                $photo->hotel = $h['id'];
                $photo->photo = $new_name;
                $pid = $photo->save();

                $notice = new Notice();
                $notice->hotel = $h['id'];
                $notice->user = $_SESSION['user']['id'];
                $notice->status = 1;
                $notice->created = time();
                $notice->type = 104;

                $notice->data = json_encode(array('photo' => $pid));

                $notice->save();
            }

            if (isset($_FILES['banner']) && $_FILES['banner']['name'] != "") {
                $banner_path = BASEPATH . "../upload/banners/";

                $ext = pathinfo($_FILES['photo']["name"], PATHINFO_EXTENSION);

                $new_name = $h['id'] . "_" . time() . $ext;

                move_uploaded_file($_FILES['banner']["tmp_name"], $banner_path . $new_name);

                $b = $this->banner->find(array('hotel' => $h['id']));
                $banner = new Banner();
                if ($b) {
                    $banner->id = $b['id'];
                }
                $banner->hotel = $h['id'];
                $banner->banner = $new_name;
                $banner->active = 1;
                $banner->save();
            }

            if (isset($_FILES['certificate']) && $_FILES['certificate']['name'] != "") {
                $certificate_path = BASEPATH . "../upload/certificates/";

                move_uploaded_file($_FILES['certificate']["tmp_name"], $certificate_path . $_FILES['certificate']["name"]);

                $h = $this->hotel->find(array("user_id" => $_SESSION['user']['id']));

                $hotel = new Hotel();
                $hotel->id = $h['id'];
                $hotel->certificate = $_FILES['certificate']["name"];
                $hotel->save(1);

                $notice = new Notice();
                $notice->hotel = $h['id'];
                $notice->user = $_SESSION['user']['id'];
                $notice->status = 1;
                $notice->created = time();
                $notice->type = 103;

                $notice->data = json_encode(array('certificate' => $_FILES['certificate']["name"]));

                $notice->save();
            }



            redirect('/panel/hotel');
            //return;
        }

        
        
      
        
         //возвращает данные немодерированной ревизии
        //если их нет то модерированной
        $data['hotel'] = $this->hotel->find_passive($hotel_id);
       
        
        //print_r($data['hotel']);
        //return;
       

        foreach ($this->hotel->get_type($data['hotel']['id']) as $type) {
            $data['hotel']['type'][$type['id']] = $type;
        }

        $data['hotel']['photos'] = $this->hotel_photo->findAll(
                array("hotel" => $data['hotel']['id']), array(), array('position', 'asc')
        );

        $data['hotel']['city'] = $this->city->find(array("id" => $data['hotel']['city_id']));

        $data['types'] = $this->hotel_type->findAll(array("active" => 1), array(), array('position', 'asc'));
        $data['citys'] = $this->city->findAll(array(), array(), array('name', 'asc'));
        foreach ($data['citys'] as &$city) {
            $city['region'] = $this->region->find(array('id' => $city['region_id']));
        }

        if (isset($_SESSION['message'])) {
            $data['message'] = $_SESSION['message'];

            unset($_SESSION['message']);
        }

        $data['services'] = array();
        foreach ($this->service->findAll(array('hotel' => $data['hotel']['id'], 'status' => 1)) as $service) {
            $data['services'][$service['type']] = $service;
        }

        $data['banner'] = $this->banner->find(array('hotel' => $data['hotel']['id']));
         
        
        //на модерации?
        if($this->hotel_revision->is_modifet($hotel_id)){
            $data['moderation'] = 'Ваши изменения вступят в силу после прохождения модерации.'; 
        }
        
        $this->render('panel/hotel', $data);
    }

    public function booking() {
        $data['hotel'] = $this->hotel->latest()->find(array("user_id" => $_SESSION['user']['id']));
        $data['services'] = array();
        foreach ($this->service->findAll(array('hotel' => $data['hotel']['id'], 'status' => 1)) as $service) {
            $data['services'][$service['type']] = $service;
        }

        if (isset($data['services'][1])) {
            $data['admin_widget'] = file_get_contents('http://api.litebooking.ru/widgets/admin/?hotel=' . $data['hotel']['lb_id']);
        }

        $this->render('panel/booking', $data);
    }

    public function stats() {
        $data['hotel'] = $this->hotel->latest()->find(array("user_id" => $_SESSION['user']['id']));
        $data['services'] = array();
        foreach ($this->service->findAll(array('hotel' => $data['hotel']['id'], 'status' => 1)) as $service) {
            $data['services'][$service['type']] = $service;
        }

        $this->render('panel/stats');
    }

    public function services($sid = 0) {
        $hotel = $this->hotel->find(array("user_id" => $_SESSION['user']['id']));
        $city = $this->city->find(array("id" => $hotel['city_id']));

        if (isset($_REQUEST['enable'])) {

            switch ($_REQUEST['enable']) {
                case 1: {
                        $notice = new Notice();

                        $notice->hotel = $hotel['id'];
                        $notice->user = $_SESSION['user']['id'];
                        $notice->type = 101;
                        $notice->status = 1;

                        $data = array();
                        if (isset($_POST['period'])) {
                            $data['period'] = $_POST['period'];
                        }

                        $notice->data = json_encode($data);
                        $notice->created = time();

                        $notice->save();

                        $this->email->clear();

                        $this->email->to($_SESSION['user']['email']);
                        $this->email->from('system@rusotels.ru');
                        $this->email->subject("Заявка на услугу «Размещение на RUSOTEL.RU с подключением booking  engine на страницу каталога и на сайт отеля»");
                        $this->email->message("Уважаемый отельер! \n
                                            Благодарим Вас за заявку на услугу «Размещение на RUSOTEL.RU с подключением booking  engine на страницу каталога и на сайт отеля»  на портале Rusotels.ru!\n
                                            В ближайшее время с Вами свяжется наш оператор для уточнения параметров заказа!\n
                                            С уважением, команда Rusotels.ru\n");

                        $this->email->send();

                        redirect('/panel/services');
                    };
                    break;

                case 9: {
                        $notice = new Notice();

                        $notice->hotel = $hotel['id'];
                        $notice->user = $_SESSION['user']['id'];
                        $notice->type = 109;
                        $notice->status = 1;

                        $notice->save();
                    };
                    break;

                case 10: {
                        $notice = new Notice();

                        $notice->hotel = $hotel['id'];
                        $notice->user = $_SESSION['user']['id'];
                        $notice->type = 110;
                        $notice->status = 1;

                        $notice->save();
                    };
                    break;
            }
        }

        $data['hotel'] = $hotel;
        $data['services'] = array();
        foreach ($this->service->findAll(array('hotel' => $hotel['id'], 'status' => 1)) as $service) {
            $data['services'][$service['type']] = $service;
        }

        $data['notices'] = array();
        foreach ($this->notice->findAll(array('hotel' => $hotel['id'], 'status' => 1)) as $notice) {
            $data['notices'][$notice['type']] = $notice;
        }

        if ($sid > 0 && file_exists(BASEPATH . "../application/frontend/views/panel/service/" . $sid . ".php")) {
            $this->render('panel/service/' . $sid, $data);
        } else {
            $this->render('panel/services', $data);
        }
    }

    public function support() {
        $data['hotel'] = $this->hotel->latest()->find(array("user_id" => $_SESSION['user']['id']));
        if (isset($_POST['message'])) {
            $message = new Message();
            $message->user = $_SESSION['user']['id'];
            $message->message = $_POST['message'];
            $message->date = time();
            $message->type = 2;

            $message->save();

            $this->email->clear();

            $this->email->to(array("skorbenko@digitalwill.ru", "support@rusotels.ru", "ornot.work@gmail.com", "ribets@digitalwill.ru"));
            //$this->email->to(array("ribets@digitalwill.ru"));//для тестового сервера
            $this->email->from('system@rusotels.ru');
            $this->email->subject("Новое сообщение в службу поддержки");
            $this->email->message("пользователь: " . $_SESSION['user']['email'] . "\n
                                   текст сообщения: \n
                                   \n" . $_POST['message']);

            $this->email->send();

            redirect('panel/support');
        }

        $data['messages'] = $this->message->findAll(array("user" => $_SESSION['user']['id']));
        $data['services'] = array();
        foreach ($this->service->findAll(array('hotel' => $data['hotel']['id'], 'status' => 1)) as $service) {
            $data['services'][$service['type']] = $service;
        }

        $this->render('panel/support', $data);
    }

    public function old_password($value) {
        if (isset($_SESSION['user']) && md5($value) == $_SESSION['user']['password']) {
            return TRUE;
        }

        $this->form_validation->set_message("old_password", "Вы указали неверный пароль");
        return FALSE;
    }

    public function match_password($value) {
        if ($_POST['user']['password'] == $_POST['user']['repeat']) {
            return TRUE;
        }

        $this->form_validation->set_message("match_password", "Пароли не совпадают");
        return FALSE;
    }

}
