<div class="column-left inner">
    <div class="block block-search">
        <div class="title">Поиск</div>
        <div class="content">
            <div class="info">отели, гостиничные комплексы, пансионаты, санатории, хостелы и другие средства размещения по всей России</div>
            <form class="search-form" action="<?php echo base_url('search'); ?>">
                <div class="form-row">
                    <label for="q">Город / Название отеля:</label>
                    <input type="text" name="q" class="search" placeholder="например, Москва" value="<?php if(isset($_GET["q"])) echo $_GET["q"]; ?>" autocomplete="off" required>
                </div>
                <div class="form-row">
                    <div class="f-left date">
                        <label>Дата заезда:</label>
                        <input type="text" placeholder="01.01.2014" name="date_in" id="date-in" value="<?php if(isset($_SESSION["date_in"])) echo $_SESSION["date_in"]; ?>"/>
                    </div>
                    <div class="f-right date">
                        <label>Дата отъезда:</label>
                        <input type="text" placeholder="01.02.2014" name="date_out" id="date-out" value="<?php if(isset($_SESSION["date_out"])) echo $_SESSION["date_out"]; ?>"/>
                    </div>
                </div>
                <div class="form-row">
                    <input type="checkbox"> <span style="font-size: 13px">Точные даты поездки пока неизвестны</span>
                </div>
                <div class="form-row">
                    <input type="submit" class="btn" value="Найти" >
                </div>
            </form>
        </div>
    </div>

    <div class="block block-filter">
        <div class="title">Подобрать средства размещения по критериям:</div>
        <div class="content">
            <form method="get" action="">
                <?php if(isset($_GET["q"])): ?>
                    <input type="hidden" name="q" value="<?php echo $_GET["q"]; ?>" />
                <?php endif; ?>
                <?php if(isset($_GET["sort"])): ?>
                    <input type="hidden" name="sort" value="<?php echo $_GET["sort"]; ?>" />
                <?php endif; ?>

                <div class="section price">
                    <div class="label">Цена</div>
                    <div class="body">
                        <span>от</span><input type="text" name="filter[price_min]" /><span>до</span><input type="text" name="filter[price_max]" />
                    </div>
                </div>
                <div class="section rank">
                    <div class="label">Количество звезд</div>
                    <div class="body">
                        <ul class="hotels-filter-stars">
                            <li>
                                <label>
                                    <input type="radio" name="filter[rank]" value="" <?php echo set_radio("filter[rank]", ""); ?>>
                                    <span class="all">Все</span>
                                </label>
                            </li>
                            <li>
                                <label>
                                    <input type="radio" name="filter[rank]" value="2" <?php echo set_radio("filter[rank]", 2); ?>>
                                    <span class="star stars-2">Две звезды</span>
                                </label>
                            </li>
                            <li>
                                <label>
                                    <input type="radio" name="filter[rank]" value="3" <?php echo set_radio("filter[rank]", 3); ?>>
                                    <span class="star stars-3">Три звезды</span>
                                </label>
                            </li>
                            <li>
                                <label>
                                    <input type="radio" name="filter[rank]" value="4" <?php echo set_radio("filter[rank]", 4); ?>>
                                    <span class="star stars-4">Четыре звезды</span>
                                </label>
                            </li>
                            <li>
                                <label>
                                    <input type="radio" name="filter[rank]" value="5" <?php echo set_radio("filter[rank]", 5); ?>>
                                    <span class="star stars-5">Пять звезд</span>
                                </label>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="section">
                    <div class="label">Тип размещения</div>
                    <div class="body">
                        <ul class="hotels-filter-type">
                            <?php if(isset($links_view) && $links_view == true): ?>
                                <?php foreach($types as $item): ?>
                                    <li>
                                        <a href="/hotels/<?php echo $city['alias'] . '/' . $item['alias']; ?>"><?php echo $item['type']; ?></a>
                                    </li>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <?php foreach($types as $item): ?>
                                    <li>
                                        <label>
                                            <input type="checkbox" name="filter[type][]" value="<?php echo $item['id']; ?>"
                                                   <?php if(isset($_GET['filter']['type']) && in_array($item['id'], $_GET['filter']['type'])): ?>checked="checked"<?php endif; ?>
                                                />
                                            <span><?php echo $item['type']; ?></span>
                                        </label>
                                    </li>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>

                <p class="side-search-submit-wrap">
                    <input type="submit" class="btn" value="Подобрать" >
                </p>
            </form>
        </div>
    </div>

    <?php if(count($viewed_hotels) > 0): ?>
        <div class="block block-viewed-hotels">
            <div class="title">
                Вы смотрели: <a href="#" class="remove-all-viewed">удалить все</a>
            </div>
            <div class="content">
                <ul>
                    <?php foreach($viewed_hotels as $id => $hotel): ?>
                        <li>
                            <a href="<?php echo  base_url('/hotels/'.$hotel['uri']);?>">
                                <img src="<?php base_url(); ?>/upload/hotel_photos/<?php echo $hotel['photo']; ?>" />
                                <span class="name">
                                    <?php echo $hotel['name']; ?>
                                    <span class="stars"><?php echo str_repeat('<i></i>', $hotel['stars']); ?></span>
                                </span>
                                <span class="price">от <?php echo $hotel['price']; ?> руб.</span>
                                <span class="city">г.<?php echo $hotel['city']; ?></span>
                            </a>
                            <a href="#" class="remove-viewed" data-id="<?php echo $id; ?>">&times;</a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    <?php endif; ?>
</div>


<div class="main-col">
    <!-- Хлебные крошки -->
    <div class="breadcrumbs">
        
        <?php if(count($this->breadcrumbs) > 0): ?>
            <?php $end_element = array_pop($this->breadcrumbs); ?>
            <?php foreach($this->breadcrumbs as $link): ?>
                <span><a href="<?php echo $link['link']; ?>"><?php echo $link['title']; ?></a></span>
            <?php endforeach; ?>
                <span><?php echo $end_element['title']; ?></span>
        <?php else: ?>
            <span><a href="<?php echo base_url(); ?>">Каталог отелей</a></span>
            <span>Поиск</span>
        <?php endif; ?>
    </div>

    <div class="block block-random-hotels c-both">
        <div class="content">
            <ul>
                <?php foreach ($offers as $offer): ?>
                    <li>
                        <a href="/hotels/<?php echo $offer['link']; ?>">
                            <span class='img-wrap'>
                                <img src="/cache/thumbs/hotels/188x130/<?php echo $offer['photo']; ?>" alt="" width="188" height="130"/>
                                <span class='price'><?php echo $offer['text']; ?></span>
                                <span class="go">Перейти</span>
                            </span>
                            <span class='name'><?php echo $offer['name']; ?></span>
                            <span class='city'>г. <?php echo $offer['city']; ?></span>
                            <span class='stars'><?php echo str_repeat('<i></i>', $offer['stars']); ?></span>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>

<!--    <div class="block block-banner f-left">-->
<!--        <div class="content">-->
<!--            <a href="#"><img src="/assets/images/banner.png" /></a>-->
<!--        </div>-->
<!--    </div>-->
<!---->
<!--    <div class="block block-subscription f-right">-->
<!--        <div class="content">-->
<!--            <div class="label"><span>Подписаться</span> на рассылку</div>-->
<!--            <form>-->
<!--                <input type="text" name="subscr" class="subscr-input" placeholder="Ваш e-mail" />-->
<!--                <input type="submit" name="submit" class="subscr-submit" value="Oк" />-->
<!--            </form>-->
<!--        </div>-->
<!--    </div>-->

    <div class="block block-results c-both">
        <div class="content">
            <div class="sort">
                <ul class="sorting-types">
                    <li><a href="#">Рекомендуемые</a></li>
                    <li><a href="#">Расположение</a></li>
                    <li><a href="?<?php if(isset($_GET["q"])):?>q=<?php echo $_GET["q"]; ?>&<?php endif; ?>sort=price_min" <?php if(isset($_GET['sort']) &&  $_GET['sort'] == 'price_min'):?>class="active"<?php endif;?>>Цена</a></li>
                    <li><a href="?<?php if(isset($_GET["q"])):?>q=<?php echo $_GET["q"]; ?>&<?php endif; ?>sort=rank" <?php if(isset($_GET['sort']) &&  $_GET['sort'] == 'rank'):?>class="active"<?php endif;?> >Звезды</a></li>
                </ul>
                <a href="" class="map">Показать карту</a>
            </div>




            <?php if(isset($hotels_by_title) && $hotels_by_title): ?>
                <h2> Отели по запросу: <b><?php echo $_GET["q"]; ?></b></h2>
                <ul class="hotels">
                    <?php foreach ($hotels_by_title as $hotel): ?>
                        <?php $fcity = $this->catalog_model->getCityById($hotel['city_id']); ?>
                        <li>
                            <div class='img-wrap'>
                                <a href='<?php echo base_url('/hotels/' . $fcity['alias'] . "/" . $hotel['alias']); ?>'>
                                    <?php if(isset($hotel['photos']) && count($hotel['photos']) > 0): ?>
                                        <img src="/cache/thumbs/hotels/240x145/<?php echo $hotel['photos'][0]['photo']; ?>" alt='' width='240' height='145' />
                                    <?php else: ?>
                                        <img src="http://placehold.it/240x145" />
                                    <?php endif; ?>
                                    <span class='price'>от <strong><?php echo $hotel['price_min']; ?></strong> руб./сут.</span>
                                </a>
                            </div>
                            <div class='info-wrap'>
                                <div class='name'>
                                    <a href="<?php echo base_url('/hotels/' . $fcity['alias'] . "/" . $hotel['alias']); ?>">
                                        <?php echo $hotel['title']; ?>
                                    </a>
                                    <span class='stars'><?php echo str_repeat("<i></i>", $hotel['rank']); ?></span>
                                </div>
                                <div class="address">
                                    <span class='city'><?php echo $fcity['name']; ?></span>, <?php echo f_addr($hotel['address']); ?>
                                </div>

                                <div class='type'><b>Тип:</b>
                                    <?php foreach($hotel['type'] as $t) : ?>
                                        <?php echo $types[$t['id']]['type']; ?>
                                    <?php endforeach; ?>
                                </div>
                                <div class='rooms'><b>Количество номеров:</b> <?php echo $hotel['room_count']; ?></div>
                                <div class="buttons">
                                    <a class='btn' href="<?php echo base_url('/hotels/' . $fcity['alias'] . "/" . $hotel['alias']); ?>">Забронировать</a>
                                    <a class='view-map' href="#">Посмотреть карту</a>
                                </div>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>

            <?php if(isset($hotels_by_city) && $hotels_by_city): ?>
            <h1> Отели в городе <?php echo $city['name']; ?></h1>
            <ul class="hotels">
                <?php foreach ($hotels_by_city as $hotel): ?>
                    <?php $fcity = $this->catalog_model->getCityById($hotel['city_id']); ?>
                    <li>
                        <div class='img-wrap'>
                            <a href='<?php echo base_url('/hotels/' . $fcity['alias'] . "/" . $hotel['alias']); ?>'>
                                <?php if(isset($hotel['photos']) && count($hotel['photos']) > 0): ?>
                                    <img src="/cache/thumbs/hotels/240x145/<?php echo $hotel['photos'][0]['photo']; ?>" alt='' width='240' height='145' />
                                <?php else: ?>
                                    <img src="http://placehold.it/240x145" />
                                <?php endif; ?>
                                <span class='price'>от <strong><?php echo $hotel['price_min']; ?></strong> руб./сут.</span>
                            </a>
                        </div>
                        <div class='info-wrap'>
                            <div class='name'>
                                <a href="<?php echo base_url('/hotels/' . $fcity['alias'] . "/" . $hotel['alias']); ?>">
                                    <?php echo $hotel['title']; ?>
                                </a>
                                <span class='stars'><?php echo str_repeat("<i></i>", $hotel['rank']); ?></span>
                            </div>
                            <div class="address">
                                <span class='city'><?php echo $fcity['name']; ?></span>, <?php echo f_addr($hotel['address']); ?>
                            </div>

                            <div class='type'><b>Тип:</b>
                                <?php foreach($hotel['type'] as $t) : ?>
                                    <?php echo $types[$t['id']]['type']; ?>
                                <?php endforeach; ?>
                            </div>
                            <div class='rooms'><b>Количество номеров:</b> <?php echo $hotel['room_count']; ?></div>
                            <div class="buttons">
                                <a class='btn' href="<?php echo base_url('/hotels/' . $fcity['alias'] . "/" . $hotel['alias']); ?>">Забронировать</a>
                                <a class='view-map' href="#">Посмотреть карту</a>
                            </div>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>
            <ul class="pager">
                <li <?php if(!isset($_GET['page'])): ?>class="active"<?php endif;?>>
                    <?php if(!isset($_GET['page'])): ?>
                        <span>1</span>
                    <?php else: ?>
                        <?php if(isset($city) & !isset($hotels_by_title)): ?>
                            <a href="/hotels/<?php echo $city['alias']; ?>?<?php echo (isset($query_string)) ? $query_string : "" ?>">
                                1
                            </a>
                        <?php else: ?>
                            <a href="/search?<?php echo (isset($query_string)) ? $query_string  : "" ?>">
                                1
                            </a>
                        <?php endif; ?>
                    <?php endif; ?>
                </li>
                <?php for($i = 2; $i <= ceil($total / 20); $i++): ?>
                    <li <?php if(isset($_GET['page']) && $_GET['page'] == $i): ?>class="active"<?php endif;?>>
                        <?php if(isset($_GET['page']) && $_GET['page'] == $i): ?>
                            <span><?php echo $i; ?></span>
                        <?php else: ?>
                            <?php if(isset($city) & !isset($hotels_by_title)): ?>
                                <a href="/hotels/<?php echo $city['alias']; ?>?<?php echo (isset($query_string)) ? $query_string . "&" : "" ?>page=<?php echo $i; ?>">
                                    <?php echo $i; ?>
                                </a>
                            <?php else: ?>
                                <a href="/search?<?php echo (isset($query_string)) ? $query_string . "&" : "" ?>page=<?php echo $i; ?>">
                                    <?php echo $i; ?>
                                </a>
                            <?php endif; ?>
                        <?php endif; ?>
                    </li>
                <?php endfor; ?>
            </ul>
            <?php endif; ?>

            <?php if(isset($hotels) && $hotels): ?>
                <h1> <?php echo $type['type']; ?> в городе <?php echo $city['name']; ?>:</h1>
                <ul class="hotels">
                    <?php foreach ($hotels as $hotel): ?>
                        <?php $fcity = $this->catalog_model->getCityById($hotel['city_id']); ?>
                        <li>
                            <div class='img-wrap'>
                                <a href='<?php echo base_url('/hotels/' . $fcity['alias'] . "/" . $hotel['alias']); ?>'>
                                    <?php if(isset($hotel['photos']) && count($hotel['photos']) > 0): ?>
                                        <img src="/cache/thumbs/hotels/240x145/<?php echo $hotel['photos'][0]['photo']; ?>" alt='' width='240' height='145' />
                                    <?php else: ?>
                                        <img src="http://placehold.it/240x145" />
                                    <?php endif; ?>
                                    <span class='price'>от <strong><?php echo $hotel['price_min']; ?></strong> руб./сут.</span>
                                </a>
                            </div>
                            <div class='info-wrap'>
                                <div class='name'>
                                    <a href="<?php echo base_url('/hotels/' . $fcity['alias'] . "/" . $hotel['alias']); ?>">
                                        <?php echo $hotel['title']; ?>
                                    </a>
                                    <span class='stars'><?php echo str_repeat("<i></i>", $hotel['rank']); ?></span>
                                </div>
                                <div class="address">
                                    <span class='city'><?php echo $fcity['name']; ?></span>, <?php echo f_addr($hotel['address']); ?>
                                </div>

                                <div class='type'><b>Тип:</b>
                                    <?php foreach($hotel['type'] as $t) : ?>
                                        <?php echo $types[$t['id']]['type']; ?>
                                    <?php endforeach; ?>
                                </div>
                                <div class='rooms'><b>Количество номеров:</b> <?php echo $hotel['room_count']; ?></div>
                                <div class="buttons">
                                    <a class='btn' href="<?php echo base_url('/hotels/' . $fcity['alias'] . "/" . $hotel['alias']); ?>">Забронировать</a>
                                    <a class='view-map' href="#">Посмотреть карту</a>
                                </div>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
        </div>
    </div>

</div>