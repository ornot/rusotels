<div class="column-left inner">
    <div class="block block-search">
        <div class="title">Поиск</div>
        <div class="content">
            <div class="info">отели, гостиничные комплексы, пансионаты, санатории, хостелы и другие средства размещения по всей России</div>
            <form class="search-form" action="<?php echo base_url('search'); ?>">
                <div class="form-row">
                    <label for="q">Город / Название отеля:</label>
                    <input type="text" name="q" class="search" placeholder="например, Москва" required>
                </div>
                <div class="form-row">
                    <div class="f-left date">
                        <label>Дата заезда:</label>
                        <input type="text" placeholder="01.01.2014" name="date_in" id="date-in" value="<?php if(isset($_SESSION["date_in"])) echo $_SESSION["date_in"]; ?>"/>
                    </div>
                    <div class="f-right date">
                        <label>Дата отъезда:</label>
                        <input type="text" placeholder="01.02.2014" name ="date_out" id="date-out" value="<?php if(isset($_SESSION["date_in"])) echo $_SESSION["date_out"]; ?>"/>
                    </div>
                </div>
                <div class="form-row">
                    <input type="checkbox"> <span style="font-size: 13px">Точные даты поездки пока неизвестны</span>
                </div>
                <div class="form-row">
                    <input type="submit" class="btn" value="Найти">
                </div>
            </form>
        </div>
    </div>

    <?php if (count($viewed_hotels) > 0): ?>
        <div class="block block-viewed-hotels">
            <div class="title">
                Вы смотрели: <a href="#" class="remove-all-viewed">удалить все</a>
            </div>
            <div class="content">
                <ul>
                    <?php foreach ($viewed_hotels as $id => $viewed_hotel): ?>
                        <li>
                            <a href="<?php echo  base_url('/hotels/' . $viewed_hotel['uri']); ?>">
                                <img src="<?php base_url(); ?>/cache/thumbs/hotels/54x46/<?php echo $viewed_hotel['photo']; ?>"/>
                                <span class="name"><?php echo $viewed_hotel['name']; ?>  <span class="stars"><?php echo str_repeat('<i></i>', $viewed_hotel['stars']); ?></span></span>
                                <span class="price">от <?php echo $viewed_hotel['price']; ?> руб.</span>
                                <span class="city">г.<?php echo $viewed_hotel['city']; ?></span>
                            </a>
                            <a href="#" class="remove-viewed" data-id="<?php echo $id; ?>">&times;</a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    <?php endif; ?>

<!--    <div class="block block-subscription">-->
<!--        <div class="title">-->
<!--            Еженедельная подборка лучших спецпредложений отелей-->
<!--        </div>-->
<!--        <div class="content">-->
<!--            <div class="label"><span>Подписаться</span> на рассылку</div>-->
<!--            <form>-->
<!--                <input type="text" name="subscr" class="subscr-input" placeholder="Ваш e-mail"/>-->
<!--                <input type="submit" name="submit" class="subscr-submit" value="Oк"/>-->
<!--            </form>-->
<!--        </div>-->
<!--    </div>-->

</div>

<div class="main-col">
    <div class="breadcrumbs">
        <span><a href="<?php echo base_url(); ?>">Каталог отелей</a></span>
        <span>
            <a href="<?php echo base_url('/hotels/' . $city['alias']); ?>">
                Отели в городе <?php echo $city["name"]; ?>
            </a>
        </span>
        <span>
            
                <?php echo $hotel['title']; ?>
           
        </span>
    </div>

    <?php if($hotel['published'] == 0): ?>
        <h2 style="margin: 200px 0; text-align: center">“ваша страница еще не одобрена модератором”</h2>
    <?php else: ?>

    <div class="hotel-details">
        <div class="hotel-header">
            <h1><?php echo $hotel['title']; ?><span class="stars"><?php echo repeater("<i></i>", $hotel['rank']); ?></span></h1>

            <span class="address">
                <a href="<?php echo base_url('/hotels/' . $city['alias']); ?>"><?php echo $city['name']; ?></a>,
                <?php echo f_addr($hotel['address']); ?>
                <a class='view-map' href="#">Показать на карте</a>
            </span>
        </div>

        <div class="anchor-menu">
            <ul>
                <?php if($hotel["rooms_widget"]): ?>
                    <li><a href="#a-rooms">Доступные номера</a></li>
                <?php endif; ?>
                <li><a href="#a-contacts">Контакты отеля</a></li>
                <?php if($hotel["terms"]): ?>
                    <li><a href="#a-terms">Условия проживания</a></li>
                <?php endif; ?>
<!--                <li><a href="#">Отзывы</a></li>-->
            </ul>
        </div>

        <div class="photos-wrap c-both">
            <div class="big">
                <?php if (isset($photos[0])): ?>
                    <img src="/cache/thumbs/hotels/650x380/<?php echo $photos[0]['photo']; ?>" width='650' height='380'/>
                <?php else: ?>
                    <img src="http://placehold.it/650x380" alt='' width='650' height='380'/>
                <?php endif; ?>
            </div>
            <div class="carousel">
                <div class="ctrl left"></div>
                <ul class="thumbs">
                    <?php foreach($photos as $photo): ?>
                        <li>
                            <img src="/cache/thumbs/hotels/113x72/<?php echo $photo['photo']; ?>" data-large="/cache/thumbs/hotels/650x380/<?php echo $photo['photo']; ?>"/>
                        </li>
                    <?php endforeach; ?>
                </ul>
                <div class="ctrl right"></div>
            </div>
        </div>

        <div class="rooms-count">
            Всего номеров в отеле: <?php echo $hotel['room_count']; ?>
        </div>

        <div class="description article">
            <p><?php echo $hotel['content']; ?></p>
        </div>

        <?php if(isset($services[1])): ?>
            <script type="text/javascript">
                var dateIn = "<?php echo isset($_SESSION['date_in'])?$_SESSION['date_in']:'';?>";
                var dateOut = "<?php echo isset($_SESSION['date_out'])?$_SESSION['date_out']:'';?>";
                var hotelID = "<?php echo $hotel['lb_id']; ?>";
            </script>

            <div class="section" id="a-rooms">
                <div class="label">Доступные номера</div>

                <?php echo $rooms_widget; ?>

            </div>
        <?php endif; ?>

        <div class="section contacts" id="a-contacts">
            <div class="label">Контакты</div>
            <div class="body">
                <ul class="hotel-contacts">
                    <li class='phone'>
                        <b>Телефон:</b>
<!--                        --><?php //if(isset($services[1])): ?>
                            <a href='skype:<?php echo f_phone($hotel['phone']); ?>?call'>
                                <?php echo f_phone($hotel['phone']); ?>
                            </a>
<!--                        --><?php //else: ?>
<!--                            <span class="label">информация недоступна</span>-->
<!--                        --><?php //endif; ?>
                    </li>
                    <li class='email'>
                        <b>E-mail:</b>
<!--                        --><?php //if(isset($services[1])): ?>
                            <a href='mailto:<?php echo $hotel['email']; ?>?subject=Сообщение с сайта rusotels.ru'>
                                <?php echo $hotel['email']; ?>
                            </a>
<!--                        --><?php //else: ?>
<!--                            <span class="label">информация недоступна</span>-->
<!--                        --><?php //endif; ?>
                    </li>
                    <li class='skype'>
                        <b>Skype:</b>
<!--                        --><?php //if(isset($services[1])): ?>
                            <a href='skype:<?php echo $hotel['skype']; ?>?call'>
                                <?php echo $hotel['skype']; ?>
                            </a>
<!--                        --><?php //else: ?>
<!--                            <span class="label">информация недоступна</span>-->
<!--                        --><?php //endif; ?>
                    </li>
                    <li class='address'>
                        <b>Адрес:</b> &nbsp;<?php echo $city['name'] . ", " . f_addr($hotel['address']); ?>
                    <li>
                    <?php if($hotel['site'] !== ""): ?>
                        <li class='site'>
                            <b>Сайт:</b>
    <!--                        -->
                                <a href='/go/<?php echo $hotel['id']; ?>' rel="nofollow" target="_blank">
                                    <?php echo $hotel['site']; ?>
                                </a>
    <!--                        --><?php //else: ?>
    <!--                            <span class="label">информация недоступна</span>-->
                        </li>
                    <?php endif; ?>

                </ul>

                <div class="map">
                    <!-- НАЧАЛО КАРТЫ -->

                    <?php

                    $country = "Россия";
                    $city = $city['name'];
                    $address = f_addr($hotel['address']);

                    $loc = $city . " " . $address;

                    echo "<!-- $loc -->";

                    $params = array(
                        'geocode' => $loc, // адрес
                        'format' => 'json', //формат Json
                        'results' => 1, //количество выводимых результатов
                        'key' => '', //api ключ
                    );

                    $response = json_decode(file_get_contents('http://geocode-maps.yandex.ru/1.x/?' . http_build_query($params, '', '&')));

                    if ($response->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found > 0) {
                        $a1 = $response->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos;

                        $pieces = explode(" ", $a1);
                        $x = $pieces[1];
                        $y = $pieces[0];
                        $coord = $pieces[1] . ", " . $pieces[0];
                    } else {
                        $coord = '55.753559, 37.609218'; //МСК
                        $x = '55.753559';
                        $y = '37.609218';
                    }

                    ?>

                    <script src="http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU"
                            type="text/javascript"></script>
                    <script type="text/javascript">
                        window.onload = function () {
                            ymaps.ready(function () {
                                // Создание экземпляра карты
                                var myMap = new ymaps.Map('map', {
                                    center: [<?php echo"$coord"; ?>],
                                    zoom: <?php echo"14"; ?>
                                });

                                //ЭЛЕМЕНТЫ УПРАВЛЕНИЯ
                                myMap.controls
                                    .add('zoomControl') // Кнопка зума расширен
                                    //.add('typeSelector')  // Список типов карты
                                    //.add('smallZoomControl', { right: 5, top: 75 }) //компакт зуум
                                    .add('mapTools'); // Стандартный набор кнопок


                                // СОЗДАЕМ МЕТКУ И ЗАДАЕМ ИКОНКУ ДЛЯ ЕЕ ОТОБРАЖЕНИЯ
                                myPlacemark = new ymaps.Placemark([<?php echo"$x"; ?>, <?php echo"$y"; ?>], {
                                    balloonContent: '<?php echo $hotel['title']; ?>'
                                }, {
                                    iconImageHref: '<?php echo base_url() . "assets/images/marker.png" ?>', // картинка иконки
                                    iconImageSize: [37, 42], // размеры картинки
                                    iconImageOffset: [-12, -42] // смещение картинки
                                });
                                // Добавление метки на карту
                                myMap.geoObjects.add(myPlacemark);


                            });
                        }
                    </script>


                    <div id="map" style="width:635px; height:340px"></div>
            </div>



                <!-- КОНЕЦ КАРТЫ -->
            </div>
            <!--            <div class="buttons">-->
            <!---->
            <!--                --><?php //if ($hotel['email'] != ''): ?>
            <!--                    <a href='#' class='grey-btn' onclick="$('#exampleModal2').arcticmodal()">-->
            <!--                        <img src="-->
            <?php //echo base_url(); ?><!--assets/images/grey-btn-icon2.png" alt=''/>-->
            <!--                        <span class='ts-wh1'>Отправить сообщение</span>-->
            <!--                        <i></i><b></b>-->
            <!--                    </a>-->
            <!--                --><?php //endif; ?>
            <!---->
            <!--                --><?php //if (($hotel['email'] != '') and ($hotel['phone'] != '')): ?>
            <!--                    <a href='#' class='grey-btn' onclick="$('#exampleModal1').arcticmodal()">-->
            <!--                        <img src="-->
            <?php //echo base_url(); ?><!--assets/images/grey-btn-icon1.png" alt=''/>-->
            <!--                        <span class='ts-wh1'>Заказать звонок</span>-->
            <!--                        <i></i><b></b>-->
            <!--                    </a>-->
            <!--                --><?php //endif; ?>
            <!---->
            <!--            </div>-->
        </div>

        <?php if($hotel["terms"]): ?>
        <div class="section terms" id="a-terms">
            <div class="label">Условия проживания</div>
            <div class="body">
                <?php echo $hotel["terms"]; ?>
            </div>
        </div>
        <?php endif; ?>

    </div>
    <?php endif; ?>

    <div class="block block-random-hotels c-both">
        <div class="content">
            <ul>
                <?php foreach ($offers as $offer): ?>
                    <li>
                        <a href="/hotels/<?php echo $offer['link']; ?>">
                            <span class='img-wrap'>
                                <img src="/cache/thumbs/hotels/188x130/<?php echo $offer['photo']; ?>" alt="" width="188" height="130"/>
                                <span class='price'><?php echo $offer['text']; ?></span>
                                <span class="go">Перейти</span>
                            </span>
                            <span class='name'><?php echo $offer['name']; ?></span>
                            <span class='city'>г. <?php echo $offer['city']; ?></span>
                            <span class='stars'><?php echo str_repeat('<i></i>', $offer['stars']); ?></span>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>

</div>
