
<div id="ctbg">

    <div id="main">

        <div class="page-two-cols clearfix">

            <div class="main-col">

                <div class="blog">

                    <?php foreach ($posts as $post): ?>
                        <?php $publish_date = date('d.m.Y', strtotime($post['publish_date'])); ?>
                            <div class='article-wrap'>
                                <h2><?php echo $post['title']; ?></h2>
                                <div class='article clearfix'>
                                    <?php echo mb_substr(strip_tags($post['content']), 0, 600); ?>
                                </div>
                                <a href="/blog/<?php echo $post['alias']; ?>">подробнее</a>
                            </div>
                    <?php endforeach; ?>
                </div>

            </div>
        </div>
    </div>
</div>


