<div class="main-col wide">
    <div class="cabinet">
        <h1>Вход в личный кабинет</h1>
        <br/>
        <br/>
        <?php if(isset($error)): ?>
            <div class="form-errors">
                <p>
                    <?php echo $error; ?>
                </p>
            </div>
        <?php endif; ?>
        <form action="" class="form" method="post">
            <ul class="form-elements">
                <li>
                    <span class="title">E-mail:</span>
                    <input type="text" name="email" value="" placeholder=""/>
                </li>
                <li>
                    <span class="title">Пароль:</span>
                    <input type="password" name="password" value="" placeholder=""/>
                </li>
            </ul>

            <div class="ta-r">
                <input type="submit" class="btn" value="Авторизация">
                <br/>
                <br/>
                <a class="f-right c-both" href="/recovery">восстановить пароль</a>
            </div>
        </form>
    </div>
</div>