<ul class="tabs">
    <li><a href="/panel">Общие сведения</a></li>
    <li><a href="/panel/profile">Аккаунт</a></li>
    <li class="active"><a href="/panel/hotel">Данные отеля</a></li>
    <?php if(isset($services[1])): ?>
        <li><a href="/panel/booking">Управление бронированием</a></li>
    <?php endif; ?>
    <li><a href="/panel/services">Услуги портала</a></li>
    <li><a href="/panel/support">Поддержка</a></li>
</ul>
<?php if(isset($moderation)): ?>
<div class="moderation_info info">
    <p><?php echo $moderation; ?></p>
</div>
<?php endif; ?>
<div class="main-col wide">
    <?php if(isset($message)): ?>
        <div class="message green">
            <p><?php echo $message; ?></p>
        </div>
    <?php endif; ?>

    <form action="" class="form" method="post" enctype="multipart/form-data">
        <div class="f-left" style="width: 680px">
            <h2>
                Общие данные
                <?php if($hotel['published'] == 1): ?>
                    <a class="sys-link" href="/hotels/<?php echo $hotel['city']['alias'];?>/<?php echo $hotel['alias'];?>" target="_blank">
                        просмотреть на сайте
                    </a>
                <?php else: ?>
                    <span style="text-transform: none; font-size: 14px; font-style: italic; font-weight: 100; color: #0066cc;">Ваш отель все еще не одобрен модератором и не опубликован на rusotels.ru</span>
                <?php endif; ?>
            </h2>
            <ul class="form-elements">
                <li>
                    <span class="title">Наименование:</span>
                    <input type="text" name="hotel[title]" value="<?php echo $hotel['title']; ?>" placeholder=""/>
                </li>
                <li>
                    <span class="title">Тип:</span>
                    <div class="input-one-col">
                        <select class="multiple" name="hotel[type][]"  class="types">
                            <?php foreach($types as $type): ?>
                                <option value="<?php echo $type['id']; ?>" <?php if(isset($hotel['type'][$type['id']])): ?>selected<?php endif; ?>>
                                    <?php echo $type['type']; ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </li>
                <li>
                    <span class="title">Кол-во звезд:</span>
                    <div class="input-one-col">
                        <?php if($hotel['rank']): ?>
                            <span class="big-stars edit"><?php echo str_repeat("<i></i>", $hotel['rank']); ?></span>
                        <?php else: ?>
                            <span>Нет звезд</span>
                        <?php endif; ?>
                    </div>
                    <div style="padding: 5px 0 0 0; font-style: italic; font-size: 12px; font-weight: 100">
                        Количество звезд появится в сведениях о вашем отеле после модерации и проверки загруженного сертификата.
                    </div>
                </li>
                <li>
                    <span class="title">Копия сертификата на получение звезд:</span>
                    <?php if($hotel['certificate_status'] == 1): ?>
                        <span style="display: inline-block; padding: 14px 0 0 0; font-style: italic; font-weight: 100; color: #009900;">Подтвержден</span>
                    <?php elseif($hotel['certificate_status'] == 0 && $hotel['certificate'] != ""): ?>
                        <span style="display: inline-block; padding: 14px 0 0 0; font-style: italic; font-weight: 100; color: #0066cc;">Ожидается подтверждение</span>
                    <?php else: ?>
                    <span style="display: inline-block; padding: 14px 0 0 0; font-style: italic; font-weight: 100">изображение в формате JPG, PNG</span>
                    <div style="border: 1px solid #eeeeee; line-height: 30px; display: inline-block; margin-left: 303px; padding: 0 5px">
                        <input type="file" name="certificate" />
                    </div>
                    <?php endif; ?>
                </li>
                <li>
                    <span class="title">Кол-во номеров:</span>
                    <div class="input-one-col">
                        <input id="number-of-rooms" class="spinner input-text" name="hotel[room_count]" value="<?php echo $hotel["room_count"]; ?>"/>
                    </div>
                </li>
                <li>
                    <span class="title">Минимальная цена (руб/сутки):</span>
                    <input type="text" name="hotel[price_min]" value="<?php echo $hotel["price_min"]; ?>"/>
                </li>
                <li>
                    <span class="title">Максимальная цена (руб/сутки):</span>
                    <input type="text" name="hotel[price_max]" value="<?php echo $hotel["price_max"]; ?>"/>
                </li>
                <li>
                    <span class="title">Город:</span>
                    <select name="hotel[city_id]">
                        <?php foreach($citys as $city): ?>
                            <option value="<?php echo $city["id"]; ?>" <?php if($hotel["city_id"] == $city["id"]): ?>selected<?php endif; ?>>
                                <?php echo $city["name"]; ?>
                                (<?php echo $city['region']['name']; ?>)
                            </option>
                        <?php endforeach; ?>
                    </select>
                </li>

                <li>
                    <span class="title" style="width: 100px">Адрес:</span>
                    <?php $parts = explode('|', $hotel["address"]); ?>
                    <span>ул. </span><input type="text" name="addr[street]" value="<?php echo $parts[0]; ?>" />
                    <span>&nbsp;&nbsp;д. </span>
                    <input style="width: 40px" type="text" name="addr[house]" value="<?php echo (isset($parts[1])) ? $parts[1] : "" ; ?>" />
                    <span> стр. </span>
                    <input style="width: 40px" type="text" name="addr[build]" value="<?php echo (isset($parts[2])) ? $parts[2] : "" ; ?>" />
                </li>
            </ul>
        </div>

        <div class="f-right" style="width: 280px">
            <h2>Контактная информация отдела бронирования</h2>
            <ul class="form-elements vertical">
                <li>
                    <span class="title phone-icon">Телефон отдела бронирования:</span>
                    <?php $parts = explode('|', $hotel["phone"]); ?>
                    <span>+7</span>
                    ( <input class="validate[required,custom[p_code]]" style="width: 35px;" type="text" name="phone[code]" value="<?php echo (isset($parts[1])) ? $parts[0] : ""; ?>" /> )
                    <input class="validate[required,custom[p_num]]" style="width: 150px;" type="text" name="phone[num]" placeholder="" value="<?php echo (isset($parts[1])) ? $parts[1] : $parts[0]; ?>"/>
                </li>
                <li>
                    <span class="title email-icon">E-mail отдела бронирования:</span>
                    <input type="text" name="hotel[email]" value="<?php echo $hotel["email"]; ?>"/>
                </li>
                <li>
                    <span class="title skype-icon">Skype:</span>
                    <input type="text" name="hotel[skype]" value="<?php echo $hotel["skype"]; ?>"/>
                </li>
                <li>
                    <span class="title site-icon">Сайт (без http://):</span>
                    <input type="text" name="hotel[site]" value="<?php echo $hotel["site"]; ?>"/>
                </li>
            </ul>
            <input type="submit" class="btn f-right c-both" value="Сохранить">

        </div>

        <hr class="c-both f-left" />

        <h2 class="c-both">
            Подробное описание гостиницы
        </h2>

        <textarea class="description" style="margin-bottom: 50px" rows="20" cols="118" name="hotel[content]" placeholder="Расскажите о своей гостинице. Добавьте подробное описание сервисов и услуг. Туристам будет интересно узнать о вас больше информации."><?php echo $hotel["content"]; ?></textarea>

        <hr class="c-both f-left" />

        <h2 class="c-both">Фотографии</h2>
        <p style="font-size: 12px; color: darkgray; font-style: italic;">Вы можете поменять порядок фото просто захватив указателем мыши необходифую фото и переставив ее на нужное место.</p>
        <ul class="photos">
            <?php foreach($hotel['photos'] as $photo): ?>
                <li>
                    <img width="230" height="170" src="/cache/thumbs/hotels/230x170/<?php echo $photo['photo']; ?>" />
                    <a href="/panel/hotel?remove=<?php echo $photo['id']; ?>" class="remove-photo">удалить</a>
                    <input type="hidden" name="pos[<?php echo $photo['id']; ?>]" value="<?php echo $photo['position']; ?>" class="pos"/>
                </li>
            <?php endforeach; ?>
        </ul>

        <div style="display: inline-block; line-height: 45px; padding: 0 0 0 10px; border: 1px solid #eeeeee;">
            <label style="font-size: 16px; ">Добавить фото: </label> <input type="file" name="photo" accept="image/jpeg,image/png"/>
            <input type="submit" class="btn" value="Загрузить">
        </div>

        <input type="submit" class="btn f-right c-both" value="Сохранить">

        <?php if(isset($services[10])): ?>
            <br><br><br><br><br>

            <hr class="c-both f-left" />

            <h2 class="c-both">Баннер для главной странницы</h2>

            <?php if($banner): ?>
                <img src="<?php echo $banner['banner']; ?>" width="660" height="436"/>
            <?php endif; ?>

            <div style="display: inline-block; line-height: 45px; padding: 0 0 0 10px; border: 1px solid #eeeeee;">
                <label style="font-size: 16px; ">Новый баннер: </label> <input type="file" name="banner" accept="image/jpeg,image/png"/>
                <input type="submit" class="btn" value="Загрузить">
            </div>
        <?php endif; ?>

    </form>
</div>

<script>
    $(document).ready(function(){
        $(".remove-photo").click(function(){
            if(confirm("Удалить выбранное фото?")) {
                $.get($(this).attr('href'));
                $(this).parent().remove();
            }

            return false;
        });

        $(".photos").sortable({
            cursor: "move",
            update: function( event, ui ) {
                $(".photos li").each(function(i, e){
                    inp = $(e).find(".pos")[0];
                    $(inp).val($(e).index());
                });
            }
        });

        $('.description').ckeditor({
            width: 1010,
            height: 200,
            toolbar: [
                { name: 'basicstyles', items: [ 'Bold', 'Italic' ] }
            ]
        });

        setTimeout(function(){
            $('.message').remove();
        }, 3000);
    });
</script>