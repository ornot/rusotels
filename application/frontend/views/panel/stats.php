<ul class="tabs">
    <li><a href="/panel">Общие сведения</a></li>
    <li><a href="/panel/profile">Аккаунт</a></li>
    <li><a href="/panel/hotel">Данные отеля</a></li>
<!--    <li><a href="/panel/booking">Управление бронированием</a></li>-->
    <li class="active"><a href="/panel/stats">Платные услуги</a></li>
    <li><a href="/panel/services">Услуги портала</a></li>
    <li><a href="/panel/support">Поддержка</a></li>
</ul>

<div class="main-col wide">
    <p>После прохождения модерации и размещения в каталоге вы сможете следить за количеством просмотров и запросов на бронирование</p>
</div>