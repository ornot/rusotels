<ul class="tabs">
    <li><a href="/panel">Общие сведения</a></li>
    <li><a href="/panel/profile">Аккаунт</a></li>
    <li><a href="/panel/hotel">Данные отеля</a></li>
    <?php if(isset($services[1])): ?>
        <li><a href="/panel/booking">Управление бронированием</a></li>
    <?php endif; ?>
    <li><a href="/panel/services">Услуги портала</a></li>
    <li class="active"><a href="/panel/support">Поддержка</a></li>
</ul>

<div class="main-col wide">
    <form class="form" method="post" style="overflow: hidden">

        <textarea name="message" style="margin-bottom: 10px" rows="20" cols="118" placeholder="Кратко опишите вашу проблему. Оставьте телефон по которому с вами удобнее связаться или е-mail. Мы обязательно Вам поможем"></textarea>
        <input type="submit" class="btn f-right c-both" value="Отправить" />
    </form>
    <hr class="c-both" style="margin-top: 20px"/>
    <h3>История сообщений:</h3>
    <ul class="messages-history">
        <?php foreach($messages as $message): ?>
            <li>
                <div class="date"><?php echo date("d/m/Y - H:i:s", $message['date']); ?></div>
                <?php echo $message['message']; ?>
            </li>
        <?php endforeach; ?>
    </ul>
</div>