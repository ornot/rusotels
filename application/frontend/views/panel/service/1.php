<ul class="tabs">
    <li><a href="/panel">Общие сведения</a></li>
    <li><a href="/panel/profile">Аккаунт</a></li>
    <li><a href="/panel/hotel">Данные отеля</a></li>
    <!--    <li><a href="/panel/booking">Управление бронированием</a></li>-->
    <li class="active"><a href="/panel/services">Платные услуги</a></li>
    <li><a href="/panel/support">Поддержка</a></li>
</ul>

<div class="main-col wide">
    <p>Размещение на Rusorels.ru с подключением booking engine на страницу каталога и на сайт отеля</p>

    <form class="form" method="post">
        <input type="hidden" value="1" name="enable"/>
        <table class="services-table">
            <thead>
                <tr>
                    <th width="300px"></th>
                    <th></th>
                    <th></th>
                    <th width="300px"></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="name">
                        1 месяц &mdash;
                        <?php if($hotel['room_count'] < 51): ?>
                            65 руб./день
                        <?php elseif($hotel['room_count'] > 50 && $hotel['room_count'] < 101): ?>
                            80 руб./ день
                        <?php elseif($hotel['room_count'] > 100 && $hotel['room_count'] < 301): ?>
                            95 руб./день
                        <?php elseif($hotel['room_count'] > 300): ?>
                            115 руб./день
                        <?php endif; ?>

                    </td>
                    <td class="price">
                        <b>
                            <?php if($hotel['room_count'] < 51): ?>
                                1 950 руб.
                            <?php elseif($hotel['room_count'] > 50 && $hotel['room_count'] < 101): ?>
                                2 400 руб.
                            <?php elseif($hotel['room_count'] > 100 && $hotel['room_count'] < 301): ?>
                                2 850 руб.
                            <?php elseif($hotel['room_count'] > 300): ?>
                                3 450 руб.
                            <?php endif; ?>
                        </b>
                    </td>
                    <td>
                        <input type="radio" value="1" name="period"/>
                    </td>
                    <td rowspan="7" class="actions">
                        <input type="submit" value="заказать" class="btn">
                    </td>
                </tr>
                <tr class="spacer"><td colspan="4"></td></tr>
                <tr>
                    <td class="name">
                        3 месяца &mdash;
                        <?php if($hotel['room_count'] < 51): ?>
                            62 руб./день
                        <?php elseif($hotel['room_count'] > 50 && $hotel['room_count'] < 101): ?>
                            76 руб./ день
                        <?php elseif($hotel['room_count'] > 100 && $hotel['room_count'] < 301): ?>
                            90 руб./день
                        <?php elseif($hotel['room_count'] > 300): ?>
                            109 руб./день
                        <?php endif; ?>
                        <br/>
                        <b>cкидка 5%</b>
                    </td>
                    <td class="price">
                        <b>
                            <?php if($hotel['room_count'] < 51): ?>
                                5 558 руб.
                            <?php elseif($hotel['room_count'] > 50 && $hotel['room_count'] < 101): ?>
                                6 840 руб.
                            <?php elseif($hotel['room_count'] > 100 && $hotel['room_count'] < 301): ?>
                                8 123 руб.
                            <?php elseif($hotel['room_count'] > 300): ?>
                                9 833 руб.
                            <?php endif; ?>
                        </b>
                    </td>
                    <td>
                        <input type="radio" value="3" name="period"/>
                    </td>
                </tr>
                <tr class="spacer"><td colspan="4"></td></tr>
                <tr>
                    <td class="name">
                        6 месяц &mdash;
                        <?php if($hotel['room_count'] < 51): ?>
                            59 руб./день
                        <?php elseif($hotel['room_count'] > 50 && $hotel['room_count'] < 101): ?>
                            72 руб./ день
                        <?php elseif($hotel['room_count'] > 100 && $hotel['room_count'] < 301): ?>
                            86 руб./день
                        <?php elseif($hotel['room_count'] > 300): ?>
                            104 руб./день
                        <?php endif; ?>
                        <br/>
                        <b>cкидка 10%</b>
                    </td>
                    <td class="price">
                        <b>
                            <?php if($hotel['room_count'] < 51): ?>
                                10 530 руб.
                            <?php elseif($hotel['room_count'] > 50 && $hotel['room_count'] < 101): ?>
                                12 960 руб.
                            <?php elseif($hotel['room_count'] > 100 && $hotel['room_count'] < 301): ?>
                                15 390 руб.
                            <?php elseif($hotel['room_count'] > 300): ?>
                                18 630 руб.
                            <?php endif; ?>
                        </b>
                    </td>
                    <td>
                        <input type="radio" value="6" name="period"/>
                    </td>
                </tr>
                <tr class="spacer"><td colspan="4"></td></tr>
                <tr>
                    <td class="name">
                        12 месяц &mdash;
                        <?php if($hotel['room_count'] < 51): ?>
                            55 руб./день
                        <?php elseif($hotel['room_count'] > 50 && $hotel['room_count'] < 101): ?>
                            68 руб./ день
                        <?php elseif($hotel['room_count'] > 100 && $hotel['room_count'] < 301): ?>
                            81 руб./день
                        <?php elseif($hotel['room_count'] > 300): ?>
                            98 руб./день
                        <?php endif; ?>
                        <br/>
                        <b>cкидка 15%</b>
                    </td>
                    <td class="price">
                        <b>
                            <?php if($hotel['room_count'] < 51): ?>
                                19 890 руб.
                            <?php elseif($hotel['room_count'] > 50 && $hotel['room_count'] < 101): ?>
                                24 480 руб.
                            <?php elseif($hotel['room_count'] > 100 && $hotel['room_count'] < 301): ?>
                                29 070 руб.
                            <?php elseif($hotel['room_count'] > 300): ?>
                                35 190 руб.
                            <?php endif; ?>
                        </b>
                    </td>
                    <td>
                        <input type="radio" value="12" name="period"/>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>