<ul class="tabs">
    <li><a href="/panel">Общие сведения</a></li>
    <li><a href="/panel/profile">Аккаунт</a></li>
    <li><a href="/panel/hotel">Данные отеля</a></li>
    <?php if(isset($services[1])): ?>
        <li class="active"><a href="/panel/booking">Управление бронированием</a></li>
    <?php endif; ?>
    <li><a href="/panel/services">Услуги портала</a></li>
    <li><a href="/panel/support">Поддержка</a></li>
</ul>

<div class="main-col wide">
            <?php if(isset($admin_widget) && $admin_widget != ""):?>
                <?php echo $admin_widget; ?>
            <?php else: ?>
                <h4>Услуга будет подключена в течении суток</h4>
            <?php endif; ?>
</div>