<ul class="tabs">
    <li><a href="/panel">Общие сведения</a></li>
    <li class="active"><a href="/panel/profile">Аккаунт</a></li>
    <li><a href="/panel/hotel">Данные отеля</a></li>
    <?php if(isset($services[1])): ?>
        <li><a href="/panel/booking">Управление бронированием</a></li>
    <?php endif; ?>
    <li><a href="/panel/services">Услуги портала</a></li>
    <li><a href="/panel/support">Поддержка</a></li>
</ul>

<div class="main-col wide">
    <?php if(isset($message)): ?>
        <div class="message green">
            <p><?php echo $message; ?></p>
        </div>
    <?php endif; ?>
    <form action="" class="form" method="post">
        <h2 style="padding: 0px 0px 20px 0px;">Информация о пользователе</h2>
        <?php if(validation_errors()): ?>
            <div class="form-errors">
                <?php echo validation_errors(); ?>
            </div>
        <?php endif; ?>
        <ul class="form-elements">
            <li>
                <span class="title">Текущий пароль:</span>
                <input type="password" name="user[old]" value="" />
            </li>
            <li>
                <span class="title">Новый пароль:</span>
                <input type="password" name="user[password]" value="" />
            </li>
            <li>
                <span class="title">Повторите новый пароль:</span>
                <input type="password" name="user[repeat]" value="" />
            </li>
        </ul>

        <div class="ta-r">
            <input type="submit" class="btn" value="Сохранить">
        </div>
    </form>

    <form action="" class="form" method="post">
        <h2 style="padding: 0px 0px 20px 0px;">Реквизиты юр. лица</h2>

        <ul class="form-elements requisites">
            <li>
                <span class="title">Название организации:</span>
                <input type="text" name="requisites[name]" value="<?php echo or_empty($requisites['name']); ?>" placeholder=""/>
            </li>
            <li>
                <span class="title">Юридический адрес:</span>
                <input type="text" name="requisites[addr]" value="<?php echo or_empty($requisites['addr']); ?>" placeholder=""/>
            </li>
            <li>
                <span class="title">Почтовый адрес:</span>
                <input type="text" name="requisites[post_addr]" value="<?php echo or_empty($requisites['post_addr']); ?>" placeholder=""/>
            </li>
            <li>
                <span class="title">Адрес электронной почты:</span>
                <input type="text" name="requisites[email]" value="<?php echo or_empty($requisites['email']); ?>" placeholder=""/>
            </li>
            <li>
                <span class="title">Телефон:</span>
                <input type="text" name="requisites[phone]" value="<?php echo or_empty($requisites['phone']); ?>" placeholder=""/>
            </li>
            <li>
                <span class="title">ОГРН:</span>
                <input type="text" name="requisites[ogrn]" value="<?php echo or_empty($requisites['ogrn']); ?>" placeholder=""/>
            </li>
            <li>
                <span class="title">КПП:</span>
                <input type="text" name="requisites[kpp]" value="<?php echo or_empty($requisites['kpp']); ?>" placeholder=""/>
            </li>
            <li>
                <span class="title">ИНН:</span>
                <input type="text" name="requisites[inn]" value="<?php echo or_empty($requisites['inn']); ?>" placeholder=""/>
            </li>
            <li>
                <span class="title">Корреспондентский счет:</span>
                <input type="text" name="requisites[cor_account]" value="<?php echo or_empty($requisites['cor_account']); ?>" placeholder=""/>
            </li>
            <li>
                <span class="title">Р/С:</span>
                <input type="text" name="requisites[bank_account]" value="<?php echo or_empty($requisites['bank_account']); ?>" placeholder=""/>
            </li>
            <li>
                <span class="title">БИК:</span>
                <input type="text" name="requisites[bik]" value="<?php echo or_empty($requisites['bik']); ?>" placeholder=""/>
            </li>
            <li>
                <span class="title">Наименование банка:</span>
                <input type="text" name="requisites[bank_name]" value="<?php echo or_empty($requisites['bank_name']); ?>" placeholder=""/>
            </li>
            <li>
                <span class="title">Должность лица, подписывающего договор от имени Организатора:</span>
                <input type="text" name="requisites[position]" value="<?php echo or_empty($requisites['position']); ?>" placeholder=""/>
            </li>
            <li>
                <span class="title">Основания полномочий лица, подписывающего договор:</span>
                <input type="text" name="requisites[position_basement]" value="<?php echo or_empty($requisites['position_basement']); ?>" placeholder=""/>
            </li>
        </ul>

        <div class="ta-r" style="width: 780px;">
            <input type="submit" class="btn" value="Сохранить">
        </div>

    </form>
</div>