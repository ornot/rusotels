<ul class="tabs">
    <li><a href="/panel">Общие сведения</a></li>
    <li><a href="/panel/profile">Аккаунт</a></li>
    <li><a href="/panel/hotel">Данные отеля</a></li>
    <?php if(isset($services[1])): ?>
        <li><a href="/panel/booking">Управление бронированием</a></li>
    <?php endif; ?>
    <li class="active"><a href="/panel/services">Услуги портала</a></li>
    <li><a href="/panel/support">Поддержка</a></li>
</ul>

<div class="main-col wide">
    <table class="services-table">
        <thead>
            <tr>
                <th class="name">Описание услуг</th>
                <th class="price">Тариф</th>
                <th class="actions">статус</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="name">
                    Размещение в каталоге RUSOTELS.RU контактных данных отеля и фотографий
                    <i class="icon-question-sign"
                       data-content="Подробное описание услуги можно посмотреть по <a href='/help-catalog' target='_blank'>ссылке</a>"></i>
                </td>
                <td class="price">Бесплатно!</td>
                <td class="actions">
                    <?php if($hotel['published']): ?>
                        <span class="green">Активна</span>
                    <?php else: ?>
                        <span class="blue">На модерации</span>
                    <?php endif; ?>
                </td>
            </tr>
            <tr class="spacer"><td colspan="6"></td></tr>
            <tr>
                <td class="name">
                    Подключение booking engine (on-line booking) на страницу отеля в каталоге и на личный сайт отеля (при его наличии)
                    <i class="icon-question-sign"
                       data-content="Подробное описание услуги можно посмотреть по <a href='/help-booking-engine' target='_blank'>ссылке</a>"></i>
                </td>
                <td class="price">
                    <?php if($hotel['room_count'] < 51): ?>
                        65 руб./день
                    <?php elseif($hotel['room_count'] > 50 && $hotel['room_count'] < 101): ?>
                        80 руб./ день
                    <?php elseif($hotel['room_count'] > 100 && $hotel['room_count'] < 301): ?>
                        95 руб./день
                    <?php elseif($hotel['room_count'] > 300): ?>
                        115 руб./день
                    <?php endif; ?>
                </td>
                <td class="actions">
<!--                    <span class="gray">Скоро</span>-->
                    <?php if(isset($services[1])) :?>
                        <span class="green">Активна</span>
                    <?php elseif(isset($notices[101])): ?>
                        <span class="blue">В обработке</span>
                    <?php else: ?>
                        <a href="/panel/services/1" class="orange-link">
                            Заказать
                        </a>
                    <?php endif; ?>
                </td>
            </tr>
            <tr class="spacer"><td colspan="6"></td></tr>
            <tr>
                <td class="name">
                    Кнопка <b>"Заказать звонок"</b> на странице отеля
                    <i class="icon-question-sign"
                       data-content="Подробное описание услуги можно посмотреть по <a href='/help-hotel-callback' target='_blank'>ссылке</a>"></i>
                </td>
                <td class="price">15 руб/день</td>
                <td class="actions"><span class="gray">Скоро</span></td>
            </tr>
            <tr class="spacer"><td colspan="6"></td></tr>
            <tr>
                <td class="name">
                    Кнопка <b>"Отправить сообщение в отель"</b> на странице отеля
                    <i class="icon-question-sign"
                       data-content="Подробное описание услуги можно посмотреть по <a href='/help-hotel-messages' target='_blank'>ссылке</a>"></i>
                </td>
                <td class="price">15 руб/день</td>
                <td class="actions"><span class="gray">Скоро</span></td>
            </tr>
            <tr class="spacer"><td colspan="6"></td></tr>
            <tr>
                <td class="name">
                    Список спецпредложений (разовые акции: акция месяца/недели)
                    <i class="icon-question-sign"
                       data-content="Подробное описание услуги можно посмотреть по <a href='/help-special-offers' target='_blank'>ссылке</a>"></i>
                </td>
                <td class="price">45  руб/день за 1 акцию</td>
                <td class="actions"><span class="gray">Скоро</span></td>
            </tr>
            <tr class="spacer"><td colspan="6"></td></tr>
            <tr>
                <td class="name">
                    Список пакетных предложений отеля (постоянных: свадебный, бизнес и т.п.)
                    <i class="icon-question-sign"
                       data-content="Подробное описание услуги можно посмотреть по <a href='/help-bulk-services' target='_blank'>ссылке</a>"></i>
                </td>
                <td class="price">20 руб/день за 1 пакет</td>
                <td class="actions"><span class="gray">Скоро</span></td>
            </tr>
            <tr class="spacer"><td colspan="6"></td></tr>
            <tr>
                <td class="name">
                    Поднятие наверх в каталоге в выдаче по своему городу
                    <i class="icon-question-sign"
                       data-content="Подробное описание услуги можно посмотреть по <a href='/help-push-up' target='_blank'>ссылке</a>"></i>
                </td>
                <td class="price">2000 руб/поднятие</td>
                <td class="actions">
                    <?php if(isset($notices[109])): ?>
                        <span class="blue">В обработке</span>
                    <?php else: ?>
                        <a href="/panel/services/?enable=9" class="orange-link">
                            Заказать
                        </a>
                    <?php endif; ?>
                </td>
            </tr>
            <tr class="spacer"><td colspan="6"></td></tr>
            <tr>
                <td class="name">
                    Размещение баннера отеля на главной странице портала
                    <i class="icon-question-sign"
                       data-content="Подробное описание услуги можно посмотреть по <a href='/help-adv-banners' target='_blank'>ссылке</a>"></i>
                </td>
                <td class="price">12 000 руб.в месяц</td>
                <td class="actions">
                    <?php if(isset($services[10])) :?>
                        <span class="green">Активна</span>
                    <?php elseif(isset($notices[110])): ?>
                        <span class="blue">В обработке</span>
                    <?php else: ?>
                        <a href="/panel/services/?enable=10" class="orange-link">
                            Заказать
                        </a>
                    <?php endif; ?>
                </td>
            </tr>
        </tbody>
    </table>
    <br/>
    <br/>
    <br/>
    <br/>
    <hr>
    <div style="line-height: 24px">
        <a href="http://www.edurusotels.com/#!untitled/c1nls"><img src="/upload/other/rusotels-edu.png" style="float:left; margin: 0 50px 0 0"></a>
        <p><b>20% постоянная скидка</b> на все обучающие программы для участников каталога.</p>
        <p><a href="http://www.edurusotels.com/#!untitled/c1nls" class="orange-link">Узнать подробнее</a></p>
        <br/>
        <hr>
        <a href="http://www.digitalwill.ru/"><img src="/upload/other/digital-will.png" style="float:left; margin: 0 130px 0 0"></a>
        <p>Скидки от Digital Wil для участников каталога</p>
        <p><a href="http://www.digitalwill.ru/" class="orange-link">Узнать подробнее</a></p>
        <br/>
        <br/>
    </div>
</div>