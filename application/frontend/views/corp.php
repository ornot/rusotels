<div class="main-col wide">
    <div class="cabinet">
        <h1>Зарегистрироваться как корпоративный клиент</h1>

        <div class="info" style="padding: 0 10px; line-height: 20px; background: #fafff4; border: 2px solid #a3d49d; border-radius: 5px">
            <p>
                Если вы являетесь индивидуальным туристом или корпоративным клиентом и хотите бесплатно 
				зарегистрироваться в нашем каталоге, чтобы получать информацию о предложениях гостиниц России,
				оставьте в форме обратной связи контакты о себе.   
            </p>
            <p>
                Мы уведомим Вас по e-mail и телефону, когда будет
				завершена разработка личного кабинета туриста.
            </p>
           
        </div>

        <?php if ( $validate!=''): ?>
		    <br />
            <div class="form-errors">
               <?php echo $validate;?> 
            </div>
        <?php endif; ?>
      

        <form action="" class="form" method="post" enctype="multipart/form-data">

            <ul class="form-elements">
               
                <li>
                    <span class="title"><span class="red">*</span> Имя контактного лица:</span>
                    <input required type="text" name="name" value="<?php echo $name;?>" />
                </li>
                <li>
                    <span class="title"> Название организации:</span>
                    <input type="text" name="organization" value="<?php echo $organization;?>" />
                </li>
                <li>
                    <span class="title"><span class="red">*</span> E-mail:</span>
                    <input required type="text" name="email" value="<?php echo $email;?>" />

                </li>

                 <li>
                    <span class="title phone-icon"><span class="red">*</span> Телефон:</span>
                    <span>+7</span>
                    ( <input class="validate[required,custom[p_code]]" type="text" style="width: 45px" name="code" value="<?php echo $code;?>"/> )
                    <input class="validate[required,custom[p_num]]" required type="text" style="width: 150px" name="num" value="<?php echo $num;?>"/>
                </li>
                 
                <li>
                <h2>Комментарий</h2>
                	<textarea rows="5" cols="68" name="content" placeholder=""><?php echo $content;?></textarea>
                </li>

               
            </ul>

            <div class="ta-r">
                <input type="submit" class="btn" value="Зарегистрировать">
            </div>

        </form>
    </div>
</div>