<div class="main-col wide">
    <div class="cabinet">
        <h1>БЕСПЛАТНАЯ РЕГИСТРАЦИЯ СРЕДСТВА РАЗМЕЩЕНИЯ</h1>

        <div class="info" style="padding: 0 10px; line-height: 20px; background: #fafff4; border: 2px solid #a3d49d; border-radius: 5px">
            <p>
                Если вы хотите зарегистрировать новый отель, пожалуйста проверьте его по поиску.
            </p>
            <p>
                Возможно мы уже внесли сведения о Вас в наш каталог. Если это так, напишите на info@rusotels.ru письмо, где укажите ссылку на страницу вашей гостиницы в каталоге и ваш действующий e-mail, на который вам будет выслана ссылка для восстановления пароля.
            </p>
            <p>
                После этого вы получите доступ в личный кабинет и сможете отредактировать сведения. Для входа используйте указанный вами e-mail и придуманный пароль.
            </p>
        </div>

        <?php if(validation_errors()): ?>
            <br/>
            <br/>
            <div class="form-errors">
                <?php echo validation_errors(); ?>
            </div>
        <?php endif; ?>
        <?php if ( $validate!=''): ?>
		    <br />
            <div class="form-errors">
               <?php echo $validate;?> 
            </div>
        <?php endif; ?>
        <form action="" class="form" method="post" enctype="multipart/form-data">
            <h2 style="padding: 0px 0px 20px 0px;">Информация о пользователе</h2>
            <ul class="form-elements">
                <li>
                    <span class="title"><span class="red">*</span> E-mail:</span>
                    <input required type="text" name="user[email]" value="<?php echo (isset($_POST['user']['email'])) ? $_POST['user']['email'] : ""; ?>" placeholder=""/>

                </li>
                <li>
                    <span class="title"><span class="red">*</span> Пароль:</span>
                    <input required type="password" name="user[password]" value="" />
                </li>
                <li>
                    <span class="title"><span class="red">*</span> Повторите пароль:</span>
                    <input required type="password" name="user[repeat]" value="" />
                </li>
                <li>
                    <p>
                        <br/>
                        <span class="red">*</span>
                        <i>Я согласен с условиями <a href="/agreement" target="_blank">пользовательского соглашения</a></i> &nbsp; <input required type="checkbox" name="offer" />
                        <br/>
                        <br/>
                        <span class="red">*</span>
                        <i>Я согласен получать на указанный e-mail уведомления и информационные сообщения</i> &nbsp; <input required type="checkbox" name="subscribe" />
                    </p>
                </li>
            </ul>

            <div class="info" style="padding: 0 10px; line-height: 20px; background: #fafff4; border: 2px solid #a3d49d; border-radius: 5px">
                <p>
                    Вы можете ознакомиться с краткой <a href="/faq" target="_blank">инструкцией</a> по заполнению полей. Правильно заполненные поля ускоряют процесс модерации
                </p>
            </div>

            <h2 style="padding: 0px 0px 20px 0px;">Информация о гостинице</h2>
            <ul class="form-elements">
                <li>
                    <span class="title"><span class="red">*</span> Наименование:</span>
                    <input id="signup_hotel_title" required type="text" name="hotel[title]" value="<?php echo (isset($_POST['hotel']['title'])) ? $_POST['hotel']['title'] : ""; ?>" placeholder="Название отеля"/>
                </li>
                <li>
                    <span class="title">Тип:</span>
                    <div class="input-one-col">
                        <select  name="hotel[type][]">
                            <?php foreach($types as $type): ?>
                                <option value="<?php echo $type['id']; ?>">
                                    <?php echo $type['type']; ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </li>
                <li>
                    <span class="title">Копия сертификата на получение звезд:</span>
                    <span style="display: inline-block; padding: 14px 0 0 0; font-style: italic; font-weight: 100">изображение в формате JPG, PNG</span>
                    <div style="border: 1px solid #eeeeee; line-height: 30px; display: inline-block; margin-left: 303px; padding: 0 5px">
                        <input type="file" name="certificate" />
                    </div>
                </li>
                <li>
                    <span class="title">Кол-во номеров:</span>
                    <div class="input-one-col">
                        <input id="number-of-rooms" class="spinner input-text" name="hotel[room_count]" value="<?php echo (isset($_POST['hotel']['room_count'])) ? $_POST['hotel']['room_count'] : 50; ?>"/>
                    </div>
                </li>
                <li>
                    <span class="title">Минимальная цена номера (руб/сутки):</span>
                    <input type="text" name="hotel[price_min]" value="<?php echo (isset($_POST['hotel']['price_min'])) ? $_POST['hotel']['price_min'] : ""; ?>"/>
                </li>
                <li>
                    <span class="title">Максимальная цена номера (руб/сутки):</span>
                    <input type="text" name="hotel[price_max]" value="<?php echo (isset($_POST['hotel']['price_max'])) ? $_POST['hotel']['price_max'] : ""; ?>"/>
                </li>
                <li>
                    <span class="title">Город:</span>
                    <select name="hotel[city_id]">
                        <?php foreach($citys as $city): ?>
                            <option value="<?php echo $city["id"]; ?>" <?php if(isset($_POST['hotel']['city_id']) &&  $_POST['hotel']['city_id'] == $city["id"]): ?>selected="selected"<?php endif; ?>>
                                <?php echo $city["name"]; ?>
                                (<?php echo $city['region']['name']; ?>)
                            </option>
                        <?php endforeach; ?>
                    </select>
                </li>

                <li>
                    <span class="title" style="width: 100px">Адрес:</span>
                    <span>ул. </span><input type="text" name="addr[street]" value="" />
                    <span>&nbsp;&nbsp;д. </span>
                    <input style="width: 40px" type="text" name="addr[house]" value="" />
                    <span> стр. </span>
                    <input style="width: 40px" type="text" name="addr[build]" value="" />
                </li>

            </ul>

            <h2>Подробное описание гостиницы</h2>
            <textarea rows="5" cols="68" name="hotel[content]" placeholder="Расскажите о своей гостинице. Добавьте подробное описание сервисов и услуг. Туристам будет интересно узнать о вас больше информации."><?php echo (isset($_POST['hotel']['content'])) ? $_POST['hotel']['content'] : ""; ?></textarea>

            <h2>Контактная информация отдела бронирования</h2>
            <ul class="form-elements ">
                <li>
                    <span class="title phone-icon"><span class="red">*</span> Телефон отдела бронирования:</span>
                    <span>+7</span>
                    ( <input class="validate[required,custom[p_code]]" type="text" style="width: 45px" name="phone[code]" value="<?php echo (isset($_POST['phone']['code'])) ? $_POST['phone']['code'] :  ""; ?>"/> )
                    <input class="validate[required,custom[p_num]]" required type="text" style="width: 150px" name="phone[num]" value="<?php echo (isset($_POST['phone']['num'])) ? $_POST['phone']['num'] :  ""; ?>"/>
                </li>
                <li>
                    <span class="title email-icon"><span class="red">*</span> E-mail отдела бронирования:</span>
                    <input required type="text" name="hotel[email]" value="<?php echo (isset($_POST['hotel']['email'])) ? $_POST['hotel']['email'] : ""; ?>"/>
                </li>
                <li>
                    <span class="title skype-icon">Skype:</span>
                    <input type="text" name="hotel[skype]" value="<?php echo (isset($_POST['hotel']['skype'])) ? $_POST['hotel']['skype'] : ""; ?>"/>
                </li>
                <li>
                    <span class="title site-icon">Сайт (без http://):</span>
                    <input type="text" name="hotel[site]" value="<?php echo (isset($_POST['hotel']['site'])) ? $_POST['hotel']['site'] : "" ; ?>"/>
                </li>
            </ul>


            <div class="ta-r">
                <input type="submit" class="btn" value="Зарегистрировать">
            </div>

        </form>
    </div>
</div>