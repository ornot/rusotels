<div class="block block-search f-left">
    <div class="title">Поиск</div>
    <div class="content">
        <div class="info">отели, гостиничные комплексы, пансионаты, санатории, хостелы и другие средства размещения по всей России</div>
        <form class="search-form" action="<?php echo base_url('search'); ?>">
            <div class="form-row">
                <label for="q">Город / Название отеля:</label>
                <input type="text" name="q" class="search" placeholder="например, Москва" autocomplete="off" required>
            </div>
            <div class="form-row">
                <div class="f-left date">
                    <label>Дата заезда:</label>
                    <input type="text" placeholder="01.01.2014" name="date_in" id="date-in" value="<?php if(isset($_SESSION["date_in"])) echo $_SESSION["date_in"]; ?>"/>
                </div>
                <div class="f-right date">
                    <label>Дата отъезда:</label>
                    <input type="text" placeholder="01.02.2014" name="date_out" id="date-out" value="<?php if(isset($_SESSION["date_in"])) echo $_SESSION["date_out"]; ?>"/>
                </div>
            </div>
            <div class="form-row">
                <input type="checkbox"> <span style="font-size: 13px">Точные даты поездки пока неизвестны</span>
            </div>
            <div class="form-row">
                <input type="submit" class="btn" value="Найти" >
            </div>
        </form>
    </div>
</div>

<div class="block block-slider f-right">
    <div class="content">
        <div id="slider-container">
            <?php foreach($banners as $banner): ?>
                <div class="slide">
                    <?php if($banner['hotel']): ?>
                        <a href="/hotels/<?php echo $banner['hotel']['city']['alias']; ?>/<?php echo $banner['hotel']['alias'];?>">
                            <img src="/upload/banners/<?php echo $banner['banner']; ?>" alt="" width="660" height="436" />
                            <span class="info">
                                <span class="name"><?php echo $banner['hotel']['title']; ?>, <?php echo $banner['hotel']['city']['name']; ?></span>
                                <span class="price">от <?php echo $banner['hotel']['price_min']; ?> руб. / сутки</span>
                            </span>
                        </a>
                    <?php else: ?>
<!--                        <a href="http://www.edurusotels.com/#!anti-crisis-manager-seminar/cxde" target="_blank">-->
                            <img src="/upload/banners/<?php echo $banner['banner']; ?>" alt="" width="660" height="436" />
<!--                        </a>-->
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>

            <div class="slide">
                <a href="/hotels/perm/prikamie">
                    <img src="/cache/thumbs/hotels/650x380/3263_07.jpg" alt="" width="660" height="436" />
                    <span class="info">
                        <span class="name">ПРИКАМЬЕ, Пермь</span>
                        <span class="price">от 3 800 руб. / сутки</span>
                    </span>
                </a>
            </div>

            <div class="slide">
                <a href="/hotels/irkutsk/evropa">
                    <img src="/cache/thumbs/hotels/650x380/evropa_10_15_50_2.jpg" alt="" width="660" height="436" />
                    <span class="info">
                        <span class="name">ЕВРОПА, Иркутск</span>
                        <span class="price">от 2 900 руб. / сутки</span>
                    </span>
                </a>
            </div>

            <div class="slide">
                <a href="/hotels/habarovsk/royal-laim">
                    <img src="/cache/thumbs/hotels/650x380/5843_28.jpg" alt="" width="660" height="436" />
                    <span class="info">
                        <span class="name">РОЯЛ-ЛАЙМ, Хабаровск</span>
                        <span class="price">от 3 500 руб. / сутки</span>
                    </span>
                </a>
            </div>

            <div class="slide">
                <a href="/hotels/chelyabinsk/malahit">
                    <img src="/cache/thumbs/hotels/650x380/2146_01.jpg" alt="" width="660" height="436" />
                    <span class="info">
                        <span class="name">МАЛАХИТ, Челябинск</span>
                        <span class="price">от 2 200 руб. / сутки</span>
                    </span>
                </a>
            </div>

            <div class="slide">
                <a href="http://www.projectrusotels.com" target="_blank">
                    <img src="<?php echo base_url() . "upload/banners/management.png" ?>" alt="" width="660" height="436" />
                </a>
            </div>

            <div class="slide">
                <a href="/hotels/vladivostok/akvilon">
                    <img src="/cache/thumbs/hotels/650x380/2358_01.jpg" alt="" width="660" height="436" />
                    <span class="info">
                        <span class="name">АКВИЛОН, Владивосток</span>
                        <span class="price">от 4 000 руб. / сутки</span>
                    </span>
                </a>
            </div>

        </div>
    </div>
</div>

<div class="block block-random-hotels c-both">
    <div class="content">
        <ul>
            <?php foreach ($offers as $offer): ?>
                <li>
                    <a href="/hotels/<?php echo $offer['link']; ?>">
                        <span class='img-wrap'>
                            <img src="<?php base_url(); ?>/cache/thumbs/hotels/205x145/<?php echo $offer['photo']; ?>" alt="" width="205" height="145"/>
                            <?php if($offer['text']): ?>
                                <span class='price'>от <b><?php echo $offer['text']; ?></b> руб.</span>
                            <?php endif; ?>
                            <span class="go">Перейти</span>
                        </span>
                        <span class='name'><?php echo $offer['name']; ?></span>
                        <span class='city'>г. <?php echo $offer['city']; ?></span>
                        <span class='stars'><?php echo str_repeat('<i></i>', $offer['stars']); ?></span>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>

<div class="block block-info c-both">
    <div class="content">
        <a href="http://vertolexpo.ru/Exhibition.aspx?eid=457&id=132&num=131">
            <img src="/upload/other/partners_banner.gif" style="float: left; margin-right: 40px">
        </a>

        <p style="font-size: 18px">
            УНИКАЛЬНАЯ ONLINE-ПЛОЩАДКА<br/>
        </p>
        <p style="font-size: 14px">
            Каталог RUSOTELS - это место, где без посредников встречаются отели, гостиничные комплексы, мини-отели и иные средства размещения, и их клиенты – путешественники и бизнес-туристы.
        </p>
        <div class="c-both advantages" style="padding-top: 20px">
            <div class="coll">
                <h4>Для туриста Rusotels.ru это:</h4>
                <ol>
                    <li><span>Бронь без комиссии.</span></li>
                    <li><span>Цены без накрутки.</span></li>
                    <li><span>Прямая связь с отелем.</span></li>
                    <li><span>Специальные предложения и скидки для корпоративных клиентов.</span></li>
                    <li><span>Более 3000 средств размещения на любой вкус и каприз.</span></li>
                </ol>
            </div>
            <div class="coll">
                <h4>Для отеля Rusotels.ru это:</h4>
                <ol>
                    <li><span>Бесплатное размещение контактов отеля на портале.</span></li>
                    <li><span>Прямой контакт с туристом.</span></li>
                    <li><span>Корпоративные клиенты и турфирмы.</span></li>
                    <li><span>Управление продажами на бескомиссионной основе.</span></li>
                    <li><span>Контроль видимости своего объекта на сайте и в поиске.</span></li>
                    <li><span>Специальные цены от поставщиков, новинки, необходимые для работы курсы и тренинги, тенденции отрасли.</span></li>
                </ol>
                <a href="http://www.edurusotels.com/">
                    <img src="/upload/banners/20-percents.png"/>
                </a>
            </div>
        </div>
    </div>
</div>

<div class="column-middle">
    <div class="block block-cities">
        <div class="title">
            Бронируйте сейчас!
        </div>
        <div class="content">
            <div class="description">более <?php echo $count; ?> предложений по всей России!</div>
            <ul>
                <?php foreach($top_cities as $i => $city): ?>
                <li class="<?php echo ($i % 2) ? "f-left" : "f-right";?>">
                    <div class="name">
                        <a href="<?php echo base_url('/hotels/'.$city['alias']); ?>"><?php echo $city['city_name']; ?> <span>перейти </span></a>
                    </div>
                    <div class="types">
                        <?php foreach($city['types'] as $type): ?>
                            <a href="<?php echo base_url('/hotels/'.$city['alias']); ?>/<?php echo $types[$type->name]['alias']; ?>"><?php echo $type->num;?> <?php echo $types[$type->name]['type']; ?></a>
                        <?php endforeach; ?>
                    </div>
                </li>
                <?php endforeach; ?>
            </ul>
            <a href="/cities" class="view-all">Перейти к списку всех городов</a>
        </div>
    </div>
</div>
<div class="column-right">
    <?php if(count($viewed_hotels) > 0): ?>
        <div class="block block-viewed-hotels">
            <div class="title">
                Вы смотрели: <a href="#" class="remove-all-viewed">удалить все</a>
            </div>
            <div class="content">
                <ul>
                    <?php foreach($viewed_hotels as $id => $hotel): ?>
                    <li>
                        <a href="<?php echo  base_url('/hotels/'.$hotel['uri']);?>">
                            <img style="margin-bottom:10px" src="<?php base_url(); ?>/cache/thumbs/hotels/54x46/<?php echo $hotel['photo']; ?>" />
                            <span class="name">
                                <?php echo $hotel['name']; ?>
                            </span>
                            <?php if($hotel['stars']): ?>
                                <span class="stars"  style="position: relative;"><?php echo str_repeat('<i></i>', $hotel['stars']); ?></span>
                            <?php endif; ?>
                            <span class="price">от <?php echo $hotel['price']; ?> руб.</span>
                            <span class="city">г.<?php echo $hotel['city']; ?></span>
                        </a>
                        <a href="#" class="remove-viewed" data-id="<?php echo $id; ?>">&times;</a>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    <?php endif; ?>

<!--    <div class="block block-subscription">-->
<!--        <div class="title">-->
<!--            Еженедельная подборка лучших спецпредложений отелей-->
<!--        </div>-->
<!--        <div class="content">-->
<!--            <div class="label"><span>Подписаться</span> на рассылку</div>-->
<!--            <form>-->
<!--                <input type="text" name="subscr" class="subscr-input" placeholder="Ваш e-mail" />-->
<!--                <input type="submit" name="submit" class="subscr-submit" value="Oк" />-->
<!--            </form>-->
<!--        </div>-->
<!--    </div>-->
</div>