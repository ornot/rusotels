<p style="color: #038bb2; font-size: 16px; font-weight: 300;">
    <b style="font-size: 18px; text-transform: uppercase; font-weight: 500;">Наша цель</b> - предоставить путешественнику информацию о всех средствах размещения (отели, мотели, пансионаты, хостелы, санатории, общежития, базы отдыха, охотничьи домики и т.п.) во всех городах на территории России
</p>

<div class="cities links">
    <a href="/cities" <?php if($type == 'alphabetic'): ?>class="active"<?php endif; ?>>По алфавиту</a>
    <a href="/cities/regions" <?php if($type == 'regions'): ?>class="active"<?php endif; ?>>По регионам</a>
</div>

<table class="cities <?php if($type == 'regions'): ?>regions<?php endif; ?>">
    <tr>
        <?php $i = 0; ?>
        <?php foreach($cities as $group => $list) : ?>
            <?php if($i == 4 ): ?>
                </tr>
                <tr>
                <?php $i = 0; ?>
            <?php endif; ?>
            <td>
                <h2><?php echo $group; ?></h2>
                <ul>
                    <?php foreach($list as $city): ?>
                        <li><a href="/hotels/<?php echo $city['alias'];?>">
                                <?php if($city['major'] == 1): ?>
                                    <b><?php echo $city['name']; ?></b>
                                <?php else: ?>
                                    <?php echo $city['name']; ?>
                                <?php endif; ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </td>
            <?php $i++; ?>
        <?php endforeach; ?>
    </tr>
</table>
