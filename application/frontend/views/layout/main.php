<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?php echo $this->title; ?></title>
    <meta name="keywords" content='<?php echo $this->keywords; ?>'>
    <meta name="description" content='<?php echo $this->description; ?>'>
    <link rel="stylesheet" type="text/css" href="/assets/js/chosen/chosen.min.css" media="screen">
    <link href="/assets/css/validationEngine.jquery.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="/assets/css/ui-lightness/jquery-ui-1.10.4.custom.min.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/assets/_css/style.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/assets/css/style.css" media="screen">

    <script type="text/javascript" src="/assets/js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="/assets/js/jquery-ui-1.10.4.custom.min.js"></script>
    <script type="text/javascript" src="/assets/js/jquery.ui.datepicker-ru.js"></script>
    <script type="text/javascript" src="/assets/js/jquery.placeholder.min.js"></script>
    <script type="text/javascript" src="/assets/js/jquery.carouFredSel-6.0.4.js"></script>

    <script type="text/javascript" src="/assets/js/text-shadow.min.js"></script>

    <script type="text/javascript" src="/assets/js/jquery.formstyler.min.js"></script>
    <script type="text/javascript" src="/assets/js/jquery.accordion.js"></script>
    <script type="text/javascript" src="/assets/js/ajax.js"></script>
    <script type="text/javascript" src="/assets/js/chosen/chosen.jquery.min.js"></script>

    <script type="text/javascript" src="/assets/js/bootstrap.min.js"></script>

    <script src="/assets/js/jquery.validationEngine-ru.js"></script>
    <script src="/assets/js/jquery.validationEngine.js"></script>

    <script src="/assets/js/ckeditor/ckeditor.js"></script>
    <script src="/assets/js/ckeditor/adapters/jquery.js"></script>

    <script type="text/javascript" src="/assets/js/scripts.js"></script>
</head>
<body>

    <div id="header" class="wrap">
        <a class="logo f-left" href="<?php echo base_url(); ?>"><img src="<?php echo base_url();?>/assets/images/logo.png" /></a>
        <div class="login f-right">
            <?php if($this->user->hasRole('guest')): ?>
                <div class="register-menu-trigger">
                    <a class="active" href="<?php echo base_url('signup'); ?>">Бесплатная регистрация</a>
                    <ul class="register-menu">
                        <li><a href="/signup">Зарегистрироваться как отель</a></li>
                        <li><a href="/signup/tour">Зарегистрироваться как туристская фирма</a></li>
                        <li><a href="/signup/corp">Зарегистрироваться как корпоративный клиент</a></li>
                    </ul>
                </div>
                <a href="<?php echo base_url('login'); ?>">Вход в личный кабинет отеля</a>
            <?php else: ?>
                <a href="<?php echo base_url('panel'); ?>">
                    Личный кабинет
                    <?php if(isset($_SESSION['user']['hotel'])): ?>
                        отеля "<?php echo $_SESSION['user']['hotel']['title']; ?>"
                    <?php endif; ?>
                </a>
                <a href="<?php echo base_url('logout'); ?>">Выход</a>
            <?php endif; ?>
        </div>
    </div>

    <div id="content" class="wrap">
        <?php echo $content; ?>
    </div>

    <div id="footer">
        <div class="wrap">
           <div class="block block-menu menu-company">
               <div class="title">Компания Rusotels</div>
               <div class="content">
                   <ul class="menu">
                       <li><a href="<?php echo base_url('about'); ?>">О компании</a></li>
                       <li><a href="<?php echo base_url('projects'); ?>">Проекты</a></li>
                       <li><a href="<?php echo base_url('blog'); ?>">Блог</a></li>
                       <li><a href="<?php echo base_url('contacts'); ?>">Контакты</a></li>
                   </ul>
               </div>
           </div>
           <div class="block block-menu menu-hotels">
               <div class="title">Для гостиниц, отелей, мини-отелей и других средств размещения</div>
               <div class="content">
                   <ul class="menu">
                       <li><a href="http://edu.rusotels.ru">Обучение персонала</a></li>
                       <li><a href="<?php echo base_url('signup'); ?>">Размещение в каталоге</a></li>
                   </ul>
               </div>
           </div>
           <div class="block block-menu menu-additions">
               <div class="title">Дополнительная информация</div>
               <div class="content">
                   <ul class="menu">
                       <li><a href="<?php echo base_url('sitemap'); ?>">Карта сайта</a></li>
                       <li><a href="<?php echo base_url('agreement'); ?>">Соглашение</a></li>
                       <!-- <li><a href="<?php echo base_url('partners'); ?>">Партнеры Rusotels</a></li> -->
                   </ul>
               </div>
           </div>
            <div class="block block-contacts">
                <div class="content">
                    <!-- Соц сети -->
                    <div class="phone">+7 812 404 6524</div>
                    <ul class="social">
                        <li><a class="fb" href="https://www.facebook.com/rusotels"></a></li>
                        <li><a class="tw" href="https://twitter.com/rusotels"></a></li>
                        <li><a class="vk" href="http://vk.com/rusotels"></a></li>
                    </ul>
                    <div class="copyright">© 2012-2014. Все права защищены.</div>
                    <div style="font-size: 10px">Сайт имеет возрастное ограничение 12+</div>
                </div>
            </div>
        </div>
    </div>

</body>
</html>