
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>rusotels: manager</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Le styles -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/bootstrap-responsive.css" rel="stylesheet">
    <style>
      body { padding-top: 60px; }
    </style>

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->

  </head>

  <body>


<?php
  echo managerNav(); //верхняя нав панель
?>

<div class="container"> <!-- container -->
<div class="row"> <!-- row -->




<div class="span3">
<!-- НАЧАЛО ПЕРВОЙ КОЛОНКИ -->
  <?php
    echo managerMenu(); //боковое левое меню
  ?>
<!-- КОНЕЦ ПЕРВОЙ КОЛОНКИ -->
</div>


<div class="span9">
<!-- НАЧАЛО ВТОРОЙ КОЛОНКИ -->


  <h3><?php echo $teacher['lastname'] . " " . $teacher['firstname'] . " " . $teacher['middlename']; ?></h3>

  <!-- <div class="btn-toolbar">
    <a href="#" class="btn btn-primary pull-right">Добавить лектора</a>
  </div> -->


  <?php

if (!empty($teacher['photo_small'])) { echo "<img src='" . "http://edu.rusotels.ru/upload/teachers/" . $teacher['photo_small'] . "'><br>"; }
echo "Фамилия: " . $teacher['lastname'] . "<br>";
echo "Имя: " . $teacher['firstname'] . "<br>";
echo "Отчество: " . $teacher['middlename'] . "<br><br>";

echo "url: " . $teacher['alias'] . "<br>";
//echo "" . $teacher['position'] . "<br>";
echo "email: " . $teacher['email'] . "<br><br>";
//echo "" . $teacher['photo_small'] . "<br>";
//echo "" . $teacher['photo_big'] . "<br>";

echo "<strong>Краткий текст:</strong><br>" . $teacher['content_short'] . "<br><br>";
echo "<strong>Подробный текст:</strong><br>" . $teacher['content'] . "<br>";

  ?>

<br><br>
<!-- КОНЕЦ ВТОРОЙ КОЛОНКИ -->
</div>




</div> <!-- /row -->
</div> <!-- /container -->



<?php
  echo menagerJs(); //подключение js
?>

  </body>
</html>