
<h3>Запросы</h3>

<ul class="nav nav-tabs">
    <li class="active">
        <a href="<?php echo base_url() . "notices"; ?>">
            Активные <span class='label label-success'><?php # echo $total['active']; ?></span>
        </a>
    </li>
    <li>
        <a href="<?php echo base_url() . "notices/closed"; ?>">
            Архив <span class='label label-info'><?php # echo ($total['other']-1); ?></span>
        </a>
    </li>
</ul>

<form class="form">

</form>
<form action="" method="post">
<table class="table table-hover">
    <thead>
    <tr>
        <th width="10px"></th>
        <th>#</th>
        <th width="50px">Дата</th>
        <th>Тип запроса</th>
        <th>Отель</th>
        <th>Пользователь</th>
        <th width="100px"></th>
    </tr>
    </thead>
    <div class="form-inline">
    <select class="input-large" name="action">
        <option value="none">-- Укажите действие --</option>
        <option value="unpublish">Отклонить</option>
        <option value="publish">Подтвердить</option>
    </select>
    <input class="btn" type="submit" value="Выполнить" />
</div>
    <br/>
<input class="m_check_all" type="checkbox"  value=""> Выбрать всё<br/>

    <tbody>
    <?php foreach ($notices as $notice): ?>
    <?php if(isset($notice['hotel']['id'])&&isset($notice['user']['email'])): ?>
        <tr class="notice" data-id="<?php echo $notice['id']; ?>">
            <td><input class="m_checkbox"  type="checkbox" name="notice[<?php echo $notice['id']; ?>]"/></td>
            <td><?php echo $notice['id']; ?></td>
            <td class="date"><?php echo date('<\i>H:i:s</\i><b\r><b>d.m.Y</b>', $notice['created']); ?></td>
            <td>
                <?php echo l_notice($notice['type']); ?>
            </td>
            <td><a href="hotels/edit/<?php echo $notice['hotel']['id'];?>"><?php echo $notice['hotel']['title']; ?></a></td>
            <td><?php echo $notice['user']['email']; ?></td>
            <td>
                 
                <div class="btn-group">
                    <a href="/notices/confirm/<?php echo $notice['id']; ?>" class="btn btn-mini btn-success" data-toggle="tooltip" title="подтвердить"><i class="icon-white icon-ok"></i></a>
                    <a href="/notices/reject/<?php echo $notice['id']; ?>" class="btn btn-mini btn-warning" data-toggle="tooltip" title="отклонить"><i class="icon-white icon-ban-circle"></i></a>
                </div>
                &nbsp;&nbsp;
                <?php if ($notice['type'] != 102): ?>
                    <a href="/notices/remove/<?php echo $notice['id']; ?>" class="btn btn-mini btn-inverse" data-toggle="tooltip" title="удалить"><i class="icon-white icon-trash"></i></a>
                <?php endif; ?>
            </td>
        </tr>
        <?php if($notice['data']): ?>
            <tr class="notice-more-<?php echo $notice['id']; ?>">
                <td colspan="7">
                    <?php if($notice['type'] == 101): ?>
                        <b>Подключение на:</b> <?php echo $notice['data']->period; ?> мес.
                    <?php endif; ?>

                    <?php if($notice['type'] == 102): ?>
                        <b>Изменившиеся поля:</b>
                        <br/>
                        <br/>
                        <table class="table table-bordered">
                            <tr>
                                <th>поле</th>
                                <th>старое значение</th>
                                <th>новое значение</th>
                            </tr>
                            <?php foreach($notice['data'] as $field => $values): ?>
                                <?php if($field == "phone"): ?>
                                    <tr>
                                        <td><?php echo $field; ?></td>
                                        <td><?php echo f_phone($values['old']); ?></td>
                                        <td><?php echo f_phone($values['new']); ?></td>
                                    </tr>
                                <?php elseif($field == "address"): ?>
                                    <tr>
                                        <td><?php echo $field; ?></td>
                                        <td   width="43%" ><?php echo f_addr($values['old']); ?></td>
                                        <td  width="43%" ><?php echo f_addr($values['new']); ?></td>
                                    </tr>
                                <?php else: ?>
                                    <tr>
                                        <td><?php echo $field; ?></td>
                                        <td  width="43%" ><?php echo $values['old']; ?></td>
                                        <td  width="43%" ><?php echo $values['new']; ?></td>
                                    </tr>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </table>
                    <?php endif; ?>

                    <?php if($notice['type'] == 103): ?>
                        <b>Сертификат:</b> http://rusotels.ru/upload/certificates/<?php echo $notice['data']->certificate; ?>
                    <?php endif; ?>
                </td>
            </tr>
        <?php elseif($notice['type'] === 102): ?>
           <tr class="notice-more-<?php echo $notice['id']; ?>">
                 <div class="message green">Без изменений</div>
           </tr>
        <?php endif; ?>
    <?php endif; ?>
    <?php endforeach; ?>
    </tbody>
</table>
</form>
<br/>
<br/>
<br/>

<script type="text/javascript">
    $(document).ready(function(){
        $(".notice").click(function(){
            $('.notice-more-' + $(this).data('id')).toggle();
        });
    });
</script>