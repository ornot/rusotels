
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>rusotels: manager</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Le styles -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/bootstrap-responsive.css" rel="stylesheet">
    <style>
      body { padding-top: 60px; }
    </style>

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->

  </head>

  <body>


<?php
  echo managerNav(); //верхняя нав панель
?>

<div class="container"> <!-- container -->
<div class="row"> <!-- row -->




<div class="span3">
<!-- НАЧАЛО ПЕРВОЙ КОЛОНКИ -->
  <?php
    echo managerMenu(); //боковое левое меню
  ?>
<!-- КОНЕЦ ПЕРВОЙ КОЛОНКИ -->
</div>


<div class="span9">
<!-- НАЧАЛО ВТОРОЙ КОЛОНКИ -->



<div class="alert alert-block alert-error fade in">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <h4 class="alert-heading">Данная функция отключена</h4>
            <p>В финальной сборке всё будет подключено, а пока воспользуйтесь доступными функциями из меню слева.</p>
</div>




<!-- КОНЕЦ ВТОРОЙ КОЛОНКИ -->
</div>




</div> <!-- /row -->
</div> <!-- /container -->

<?php
  echo menagerJs(); //подключение js
?>

  </body>
</html>