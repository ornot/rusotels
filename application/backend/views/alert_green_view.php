<div class="alert alert-block alert-success fade in">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <h4 class="alert-heading"><?php echo $alert_title; ?></h4>

    <p><?php echo $alert_text; ?></p>

    <p>
        <!-- <a class="btn" href="<?php echo base_url() . "posts/destroy/" . $id . "/1";?>">Да, удалить</a> --> <a
            class="btn" href="<?php echo $this->agent->referrer(); ?>">Назад</a>
    </p>
</div>
