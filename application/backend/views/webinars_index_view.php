
  <h3>Вебинары</h3>

  <div class="btn-toolbar">
    <a href="<?php echo base_url() . "webinars/build"; ?>" class="btn btn-primary pull-right">Добавить вебинар</a>
  </div>


  <table class="table table-condensed">
    <thead>
      <tr>
        <th>Название:</th>
        <th width="80px"><div style='text-align: right'>Изобр-ие:</div></th>
        <th width="20px"><div style='text-align: right'> _ </div></th>
        <!-- <th width="75px">Цена:</th> -->
        <th width="170px"> </th>
        <th width="70px"> </th>
        <!-- <th> </th> -->
        <!-- <th> </th> -->
      </tr>
    </thead>

    <tbody>
  <?php
    foreach ($webinars as $webinar) {
      echo "<tr>";
      echo "<td><a href='" . base_url() . "webinars/edit/" . $webinar['id'] . "' class=''>" . $webinar['title'] . "</a></td>";
      //if ($webinar['price'] != "") { echo "<td>" . $webinar['price'] . " руб.</td>"; } else {echo "<td></td>"; }
      
      echo "<td>" . visual_photo($webinar['image']) . "</td>";
      echo "<td><a href='" . edu_url() . "webinars/" . $webinar['alias'] . "'>" . "<i class='icon-eye-open'></i>" . "</a>" . "</td>";

      $date_start = tot($webinar['date_start']);
      $date_end = tot($webinar['date_end']);


    echo "<td>";
      if ($webinar['date_start'] != "0000-00-00") { echo $date_start; }
      if ($webinar['date_end'] != "0000-00-00") { echo " - " . $date_end . ""; }
    echo "</td>";

      echo "<td><a href='" . base_url() . "webinars/destroy/" . $webinar['id'] . "' class=''>Удалить</a></td>";
      echo "</tr>";
    }
  ?>
    </tbody>
  </table>