<h3>Отели на модерацию</h3>

<table class="table table-condensed">
    <thead>
        <tr>
            <th>Название отеля:</th>
            <th width='75px'>Статус:</th>
            <th width='115px'></th>
            <th width='115px'></th>
            <th width='70px'></th>
        </tr>
    </thead>

    <tbody>
        <?php foreach ($hotels as $hotel): ?>
            <tr>
                <td><?php echo $hotel['title']; ?></td>
                <td>
                    <?php if($hotel['published'] == 1): ?>
                        <span class='label label-success'>Вкл.</span>
                    <?php elseif($hotel['published'] == 2): ?>
                        <span class='label label-info'>Парсер.</span>
                    <?php else: ?>
                        <span class='label label-important'>Выкл.</span>
                    <?php endif; ?>
                </td>
                <td><a href='<?php echo base_url(); ?>hotels/show/<?php echo $hotel['id']; ?>' class=''>Просмотреть</a></td>
                <td><a href='<?php echo base_url(); ?>hotels/edit/<?php echo $hotel['id']; ?>' class=''>Редактировать</a>
                </td>
                <td><a href='<?php echo base_url(); ?>hotels/destroy/<?php echo $hotel['id']; ?>' class=''>Удалить</a></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $pagination; ?>

<br/>
<br/>
<br/>