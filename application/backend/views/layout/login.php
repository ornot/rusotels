<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>rusotels: manager</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/bootstrap-responsive.css" rel="stylesheet">

    <link href="/assets/css/validationEngine.jquery.css" rel="stylesheet">

    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

    <script src="/assets/js/jquery.validationEngine-ru.js"></script>
    <script src="/assets/js/jquery.validationEngine.js"></script>

    <script src="/assets/js/nicEdit.js"></script>

    <style>
        body {
            padding-top: 60px;
        }
    </style>

    <script>
        $(document).ready(function(){
            $("form").validationEngine('attach');
            new nicEditor().panelInstance('terms');
        });
    </script>

</head>

<body>

<div class='navbar navbar-inverse navbar-fixed-top'>
    <div class='navbar-inner'>
        <div class='container'>
            <button type='button' class='btn btn-navbar' data-toggle='collapse' data-target='.nav-collapse'>
                <span class='icon-bar'></span>
                <span class='icon-bar'></span>
                <span class='icon-bar'></span>
            </button>
            <a class='brand' href='<?php echo base_url(); ?>'>
                <font color='#33CCFF'>rus</font><font color='#CCCCCC'>otels: manager</font>
            </a>
        </div>
    </div>
</div>

<div class="container"> <!-- container -->
    <div class="row"> <!-- row -->
        <div class="span12">
            <?php echo $content; ?>
        </div>
    </div>
</div>

</body>
</html>