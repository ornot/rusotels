<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>rusotels: manager</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/bootstrap-responsive.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/js/chosen/chosen.min.css" rel="stylesheet">

        <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">

        <link href="/assets/css/validationEngine.jquery.css" rel="stylesheet">

        <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

        <script src="/assets/js/jquery.validationEngine-ru.js"></script>
        <script src="/assets/js/jquery.validationEngine.js"></script>

        <script src="/assets/js/bootstrap.js"></script>
        <script src="/assets/js/nicEdit.js"></script>
        <script src="/assets/js/ckeditor/ckeditor.js"></script>
        <script src="/assets/js/ckeditor/adapters/jquery.js"></script>
        <script src="/assets/js/chosen/chosen.jquery.min.js"></script>

        <script src="/assets/js/admin.js"></script>

        <style>
            body {
                padding-top: 60px;
            }
        </style>

        <script>
            $(document).ready(function(){
                $("form").validationEngine('attach');
                if($('terms').length) {
                    new nicEditor().panelInstance('terms');
                }

                CKEDITOR.disableAutoInline = true;
                CKEDITOR.config.allowedContent = true;
                $('.ck_editor').ckeditor();

                $('.btn').tooltip({container: "body"});
            });
        </script>

    </head>

    <body>

    <div class='navbar navbar-inverse navbar-fixed-top'>
        <div class='navbar-inner'>
            <div class='container'>
                <button type='button' class='btn btn-navbar' data-toggle='collapse' data-target='.nav-collapse'>
                    <span class='icon-bar'></span>
                    <span class='icon-bar'></span>
                    <span class='icon-bar'></span>
                </button>
                <a class='brand' href='<?php echo base_url(); ?>'>
                    <font color='#33CCFF'>rus</font><font color='#CCCCCC'>otels: manager</font>
                </a>
                <div class='nav-collapse collapse'>
                    <ul class='nav'>
<!--                        <li><a href='--><?php //echo base_url(); ?><!--statistics'>Статистика</a></li>-->
<!--                        <li><a href='--><?php //echo base_url(); ?><!--myerror/e301'>Бейджи</a></li>-->
<!--                        <li><a href='--><?php //echo base_url(); ?><!--myerror/e301'>Оповещения</a></li>-->
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="container"> <!-- container -->
        <div class="row"> <!-- row -->

            <div class="span2" style="margin: 0">
                <div class='well' style='width: 160px; padding: 8px 0;' data-spy="affix" data-offset-top="50">
                    <ul class='nav nav-list'>
                        <li class='nav-header'>Каталог</li>

                        <li><a href='<?php echo base_url(); ?>citys'><i class='icon-th-large'></i> Города</a></li>
                        <li><a href='<?php echo base_url(); ?>hotels'><i class='icon-th-list'></i> Отели</a></li>
                        <li><a href='<?php echo base_url(); ?>notices'><i class='icon-magnet'></i> Запросы</a></li>
                        <li><a href='/banners'><i class='icon-picture'></i> Баннеры</a></li>

                        <!-- Removed temporally
                        <li class='nav-header'>Обучение</li>
                        <li><a href='<?php echo base_url(); ?>teachers'><i class='icon-user'></i> Лекторы</a></li>
                        <li><a href='<?php echo base_url(); ?>webinars'><i class='icon-facetime-video'></i> Вебинары</a></li> -->

                        <li class='nav-header'>Общее</li>
                        <li><a href='<?php echo base_url(); ?>posts'><i class='icon-book'></i> Блог</a></li>
                        <li><a href='<?php echo base_url(); ?>pages'><i class='icon-book'></i> Страницы</a></li>
                        <li><a href='<?php echo base_url(); ?>users'><i class='icon-user'></i> Пользователи</a></li>

                        <li><a href='/messages'><i class='icon-comment'></i> Сообщения</a></li>

                        <li class='nav-header'>Настройки</li>
                        <li><a href='<?php echo base_url(); ?>social'><i class='icon-thumbs-up'></i> Соц профили</a></li>
                        <li><a href='<?php echo base_url(); ?>metateg'><i class='icon-th-list'></i> Метатеги</a></li>
                    </ul>
                </div>
            </div>

            <div class="span10">
                <?php echo $content; ?>
            </div>
        </div>
    </div>

</body>
</html>