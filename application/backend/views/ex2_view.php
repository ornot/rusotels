
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>rusotels: manager</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Le styles -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/bootstrap-responsive.css" rel="stylesheet">
    <style>
      body { padding-top: 60px; }
    </style>

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->

  </head>

  <body>


<?php
  echo managerNav(); //верхняя нав панель
?>

<div class="container"> <!-- container -->
<div class="row"> <!-- row -->




<div class="span3">
<!-- НАЧАЛО ПЕРВОЙ КОЛОНКИ -->
  <?php
    echo managerMenu(); //боковое левое меню
  ?>
<!-- КОНЕЦ ПЕРВОЙ КОЛОНКИ -->
</div>



<div class="span9">
<!-- НАЧАЛО ВТОРОЙ КОЛОНКИ -->

  <h3>Заголовок</h3>


  <ul class="nav nav-tabs">
    <li class="active">
      <a href="#">Основные</a>
    </li>
    <li><a href="#">По регионам</a></li>
    <li><a href="#">По алфавиту</a></li>
  </ul>




  <div class="btn-toolbar">
    <div class="btn-group pull-right">
      <a class="btn" href="#"><i class="icon-align-left"></i></a>
      <a class="btn" href="#"><i class="icon-align-center"></i></a>
      <a class="btn" href="#"><i class="icon-align-right"></i></a>
      <a class="btn" href="#"><i class="icon-align-justify"></i></a>
    </div>

    <div class="btn-group">
      <a href="#" class="btn"><i class="icon-download-alt"></i> Primary link</a>
      <a href="#" class="btn">link</a>
      <a href="#" class="btn">link</a>
    </div>

    <a href="#" class="btn btn-primary">Primary link</a>
  </div>





<table class="table table-condensed">
  <caption>Заголовок</caption>
  <thead>
    <tr>
      <th>Шапка раз</th>
      <th>Шапка два</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>тбл1</td>
      <td>тбл2</td>
    </tr>
  </tbody>
</table>




<!-- КОНЕЦ ВТОРОЙ КОЛОНКИ -->
</div>




</div> <!-- /row -->
</div> <!-- /container -->

<?php
  echo menagerJs(); //подключение js
?>

  </body>
</html>
