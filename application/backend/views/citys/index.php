
<h3>Города</h3>

<ul class="nav nav-tabs">
    <li class="active"><a href="<?php echo base_url() . "citys"; ?>">Основные</a></li>
    <li><a href="<?php echo base_url() . "citys/index_regions"; ?>">По регионам</a></li>

    <div class="btn-group pull-right">
        <a href="/citys/add" class="btn btn-primary">добавить город</a>
    </div>
</ul>

<table class="table table-hover">
    <thead>
        <tr>
            <th>#</th>
            <th>Город:</th>
            <th>Регион:</th>
            <th width='200px'>Отелей:</th>
            <th width='70px'> </th>
        </tr>
    </thead>

    <tbody>
    <?php foreach ($citys as $city): ?>
        <tr>
            <td><?php echo $city['id']; ?></td>
            <td><?php echo $city['name']; ?></td>
            <td><?php echo $city['region']['name']; ?></td>
            <td>
                <span class='label label-info'>
                    <?php echo $this->cp_model->getCountHotelsWithCityId($city['id']); ?>
                </span>
            </td>
            <td>
                <a href='/citys/edit/<?php echo $city['id']; ?>' class='btn btn-info btn-mini' data-toggle="tooltip" title="редактировать"><i class="icon-white  icon-pencil"></i></a>
                &nbsp;
                <a href='/citys/remove/<?php echo $city['id']; ?>' class='btn btn-inverse btn-mini remove' data-text="<?php echo $city['name']; ?>" data-toggle="tooltip" title="удалить"><i class="icon-white icon-trash"></i></a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<?php echo $pagination; ?>

<br/>
<br/>
<br/>

<script>
    $(document).ready(function(){
        $('a.remove').click(function(){
            return confirm('Удалить город - "' + $(this).data('text') + '" ?');
        });
    });
</script>