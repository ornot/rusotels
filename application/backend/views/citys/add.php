<h3>Новый город</h3>


<form class="form-horizontal" method="POST" action="">

    <div class="control-group">
        <label class="control-label" for="name">Название:</label>

        <div class="controls">
            <input type="text" class="input-xxlarge" name="city[name]" id="name" value="">
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="name">Регион:</label>

        <div class="controls">
            <select name="city[region_id]">
                <?php foreach($regions as $region): ?>
                    <option value="<?php echo $region['id']; ?>">
                        <?php echo $region['name']; ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="alias">Alias (url):</label>

        <div class="controls">
            <input type="text" name="city[alias]" id="alias" value=""> (на англ. без пробелов)
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="content">Текст про город:</label>

        <div class="controls">
            <textarea class="input-xxlarge" name="content" id="content" rows="14"></textarea>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="alias">Столица:</label>

        <div class="controls">
            <input type="checkbox" name="city[major]" id="alias" value="1">
        </div>
    </div>

    <div class="control-group">
        <div class="controls">
            <button type="submit" class="btn">Сохранить</button>
        </div>
    </div>

</form>