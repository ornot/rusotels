<h3>Города</h3>


<ul class="nav nav-tabs">
    <li><a href="<?php echo base_url() . "citys"; ?>">Основные</a></li>
    <li class="active"><a href="<?php echo base_url() . "citys/index_regions"; ?>">По регионам</a></li>
    <!-- <li><a href="#">Поиск</a></li> -->
</ul>

<table class="table table-hover">
    <thead>
    <tr>
        <th></th>
    </tr>
    </thead>

    <tbody>
    <?php foreach ($regions as $region): ?>
        <tr>
            <td>
                <a href='<?php echo base_url(); ?>citys/index_regions/<?php echo $region['id']; ?>'><?php echo $region['name']; ?></a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>