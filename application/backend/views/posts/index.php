<h3>Сообщения блога</h3>

<div class="btn-toolbar">
    <a href="<?php echo base_url() . "posts/build"; ?>" class="btn btn-primary pull-right">Новая запись в блог</a>
</div>


<table class="table table">
    <thead>
    <tr>
        <th width="100px">Дата:</th>
        <th>Заголовок:</th>
        <th width='70px'></th>
    </tr>
    </thead>

    <tbody>
    <?php foreach ($posts as $post): ?>
        <tr>
            <td><span class='date'><?php echo $post['publish_date']; ?></span></td>
            <td><?php echo $post['title']; ?></td>
            <td>
                <a href="/posts/edit/<?php echo $post['id']; ?>" class="btn btn-mini btn-info" data-toggle="tooltip" title="редактировать">
                    <i class="icon-white icon-pencil"></i>
                </a>
                &nbsp;
                <a href="/posts/destroy/<?php echo $post['id']; ?>"
                   data-text="<?php echo $post['title']; ?>"
                   class="remove btn btn-mini btn-inverse" data-toggle="tooltip" title="удалить">
                    <i class="icon-trash icon-white"></i>
                </a>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<script>
    $(document).ready(function(){
        $('a.remove').click(function(){
            return confirm('Удалить запись - "' + $(this).data('text') + '" ?');
        });
    });
</script>