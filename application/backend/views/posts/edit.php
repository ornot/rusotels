<h3>Редактирование записи в блоге</h3>


<form class="form" method="POST">

    <div class="control-group">
        <label class="control-label" for="title">Заголовок:</label>

        <div class="controls">
            <input type="text" class="input-xxlarge" name="post[title]" id="title"
                   value="<?php echo $post['title']; ?>">
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="alias">Alias (url):</label>

        <div class="controls">
            <input type="text" name="post[alias]" id="alias" value="<?php echo $post['alias']; ?>"> (на англ. без
            пробелов)
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="publish_date">Дата публикации:</label>

        <div class="controls">
            <input type="text" name="post[publish_date]" id="publish_date"
                   value="<?php echo $post['publish_date']; ?>"> (в формате гггг-мм-дд)
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="content">Текст записи:</label>

        <div class="controls">
            <textarea class="ck_editor" name="post[content]" id="content" rows="14" style="width: 990px;"><?php echo $post['content']; ?></textarea>
        </div>
    </div>

    <div class="control-group">
        <div class="controls">
            <button type="submit" class="btn">Сохранить</button>
        </div>
    </div>

</form>