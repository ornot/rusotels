<h3>Добавление новой записи в блог</h3>

<form class="form" method="POST">

    <div class="control-group">
        <label class="control-label" for="title">Заголовок:</label>

        <div class="controls">
            <input type="text" class="input-xxlarge" name="post[title]" id="title" value="">
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="alias">Alias (url):</label>

        <div class="controls">
            <input type="text" name="post[alias]" id="alias" value=""> (на англ. без пробелов)
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="publish_date">Дата публикации:</label>

        <div class="controls">
            <input type="text" name="post[publish_date]" id="publish_date" value="<?php echo date('Y-m-d'); ?>"> (в формате
            гггг-мм-дд)
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="content">Текст записи:</label>

        <div class="controls">
            <textarea class="ck_editor" name="post[content]" id="content" rows="14" placeholder=''></textarea>
        </div>
    </div>

    <div class="control-group">
        <div class="controls">
            <button type="submit" class="btn">Сохранить</button>
        </div>
    </div>

</form>