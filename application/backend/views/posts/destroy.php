<h3>Удаление записи из блога</h3>


<div class="alert alert-block alert-error fade in">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <h4 class="alert-heading">Вы действительно хотите удалить запись?</h4>

    <p>Удаление записи из базы данных. Подумайте 200 раз, прежде чем удалить информацию. Это действие необратимо.</p>

    <p>
        <a class="btn btn-danger" href="<?php echo base_url() . "posts/destroy/" . $id . "/1"; ?>">Да, удалить</a>
        <a class="btn" href="<?php echo $this->agent->referrer(); ?>">Отмена</a>
    </p>
</div>
