<h3><?php echo $hotel['title']; ?> (<?php echo $hotel['alias']; ?>)</h3>

<ul class="nav nav-tabs">
    <li><a href="<?php echo base_url() . "hotels/edit/" . $hotel['id']; ?>">Информация</a></li>
    <li><a href="<?php echo base_url() . "hotels/edit_img/" . $hotel['id']; ?>">Фотографии</a></li>
    <li><a href="<?php echo base_url() . "hotels/edit_req/" . $hotel['id']; ?>">Реквизиты</a></li>
    <li class="active"><a href="<?php echo base_url() . "hotels/services/" . $hotel['id']; ?>">Услуги</a></li>
</ul>

<?php if(!$services): ?>
    Платные услуги не подключены
    <br/>
    <br/>
    <br/>
    <br/>
    <hr>
<?php else: ?>
    <table class="table table-hover">
        <thead>
        <tr>
            <th width="80px">Активировано</th>
            <th width="80px">Срок</th>
            <th>Услуга
            <th>Статус</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
            <?php foreach($services as $service): ?>
                <tr>
                    <td class="date">
                        <?php if($service['start'] > 0): ?>
                            <?php echo date('<\i>H:i:s</\i><b\r><b>d.m.Y</b>',$service['start']); ?>
                        <?php endif; ?>
                    </td>
                    <td><b><?php echo $service['end']/(60*60*24);?></b> д</td>
                    <td><?php echo l_service($service['type']); ?></td>
                    <td><?php echo l_service_status($service['status']); ?></td>
                    <td>
                        <a href='<?php echo "/hotels/services/" . $hotel['id'] . "?remove=" . $service['id']; ?>' class='btn btn-mini btn-inverse' data-toggle="tooltip" title="удалить">
                            <i class="icon-white icon-trash"></i>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>
<form class="form form-inline" action="" method="POST">
    <select name="service">
        <option value="1">Букинг</option>
        <option value="10">Баннер на главной</option>
    </select>
    <input class="btn" type="submit" value="Подключить">
</form>