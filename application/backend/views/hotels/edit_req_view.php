<h3><?php echo db2form($hotel['title']); ?> (<?php echo db2form($hotel['alias']); ?>)</h3>

<ul class="nav nav-tabs">
    <li><a href="<?php echo base_url() . "hotels/edit/" . $hotel['id']; ?>">Информация</a></li>
    <li><a href="<?php echo base_url() . "hotels/edit_img/" . $hotel['id']; ?>">Фотографии</a></li>
    <li class="active"><a href="<?php echo base_url() . "hotels/edit_req/" . $hotel['id']; ?>">Реквизиты</a></li>
    <li><a href="<?php echo base_url() . "hotels/services/" . $hotel['id']; ?>">Услуги</a></li>
</ul>

<?php if($requisites): ?>
    <form action="" class="form-horizontal" method="post">
        <div class="control-group">
            <label class="control-label">Название организации:</label>
            <div class="controls">
                <input type="text" name="requisites[name]" value="<?php echo or_empty($requisites['name']); ?>" placeholder=""/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Юридический адрес:</label>
            <div class="controls">
                <input type="text" name="requisites[addr]" value="<?php echo or_empty($requisites['addr']); ?>" placeholder=""/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Почтовый адрес:</label>
            <div class="controls">
                <input type="text" name="requisites[post_addr]" value="<?php echo or_empty($requisites['post_addr']); ?>" placeholder=""/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">E-mail:</label>
            <div class="controls">
                <input type="text" name="requisites[email]" value="<?php echo or_empty($requisites['email']); ?>" placeholder=""/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Телефон:</label>
            <div class="controls">
                <input type="text" name="requisites[phone]" value="<?php echo or_empty($requisites['phone']); ?>" placeholder=""/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">ОГРН:</label>
            <div class="controls">
                <input type="text" name="requisites[ogrn]" value="<?php echo or_empty($requisites['ogrn']); ?>" placeholder=""/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">КПП:</label>
            <div class="controls">
                <input type="text" name="requisites[kpp]" value="<?php echo or_empty($requisites['kpp']); ?>" placeholder=""/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">ИНН:</label>
            <div class="controls">
                <input type="text" name="requisites[inn]" value="<?php echo or_empty($requisites['inn']); ?>" placeholder=""/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Корр. счет:</label>
            <div class="controls">
                <input type="text" name="requisites[cor_account]" value="<?php echo or_empty($requisites['cor_account']); ?>" placeholder=""/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Р/С:</label>
            <div class="controls">
                <input type="text" name="requisites[bank_account]" value="<?php echo or_empty($requisites['bank_account']); ?>" placeholder=""/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">БИК:</label>
            <div class="controls">
                <input type="text" name="requisites[bik]" value="<?php echo or_empty($requisites['bik']); ?>" placeholder=""/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Наименование банка:</label>
            <div class="controls">
                <input type="text" name="requisites[bank_name]" value="<?php echo or_empty($requisites['bank_name']); ?>" placeholder=""/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Должность лица:</label>
            <div class="controls">
                <input type="text" name="requisites[position]" value="<?php echo or_empty($requisites['position']); ?>" placeholder=""/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Основания:</label>
            <div class="controls">
                <input type="text" name="requisites[position_basement]" value="<?php echo or_empty($requisites['position_basement']); ?>" placeholder=""/>
            </div>
        </div>

        <div class="form-actions">
            <input type="submit" class="btn" value="Сохранить">
        </div>
    </form>
<?php else: ?>
   Данный отель не связан с контрагентами
<?php endif; ?>