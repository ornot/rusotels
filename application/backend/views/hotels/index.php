
<h3>Отели</h3>

<div class="btn-toolbar">
    <a href="<?php echo base_url() . "hotels/build"; ?>" class="btn btn-primary pull-right">Добавить новый
        отель</a>
</div>


<ul class="nav nav-tabs">
    <li>
        <a href="<?php echo base_url() . "hotels/clients"; ?>">
            Клиенты <span class='label label-success'><?php echo $total['clients']; ?></span>
        </a>
    </li>
    <li class="active">
        <a href="<?php echo base_url() . "hotels"; ?>">
            Активные <span class='label label-success'><?php echo $total['active']; ?></span>
        </a>
    </li>
    <li>
        <a href="<?php echo base_url() . "hotels/others"; ?>">
            На модерацию <span class='label label-info'><?php echo ($total['other']-1); ?></span>
        </a>
    </li>
</ul>

<form class="form">
    <input type="text" name="search" class="input-xxlarge" value="<?php echo or_empty($_GET['search']); ?>"/>
    <span style="width: 44px; display: inline-block"></span>
    <select name="region" class="input-medium region-filter">
        <option value="0">Любая область</option>
        <?php foreach($regions as $region): ?>
            <option value="<?php echo $region['id']; ?>" <?php if(or_empty($_GET['region']) == $region['id']): ?>selected=<?php endif; ?>>
                <?php echo $region['name']; ?>
            </option>
        <?php endforeach; ?>
    </select>
    <select name="city" class="city-filter">
        <option value="0">Любой город</option>
    </select>
    <div class="row">
        <input type="submit" value="найти" class="btn btn-info pull-right"/>
    </div>
</form>

<form action="" method="post">
<table class="table table-hover">
    <thead>
        <tr>
            <th></th>
            <th width="80px">Последняя активность</th>
            <th>Название отеля:</th>
            <th width='200px'>Контакты:</th>
            <th width="40px">Н-ра</th>
            <th>Ф.</th>
            <th width='100px'>Услуги</th>
            <th width='30px'>Ст.</th>
            <th width='70px'></th>
        </tr>
    </thead>

    <tbody>
        <?php foreach ($hotels as $hotel): ?>
            <tr>
                <td><input type="checkbox" name="hotels[<?php echo $hotel['id']; ?>]"/></td>
                <td class="date">
                    <?php if($hotel['user']): ?>
                        <?php echo ($hotel['user']['last_active'] > 0) ? date('<\i>H:i:s</\i><b\r><b>d.m.Y</b>', $hotel['user']['last_active']): ""; ?>
                    <?php endif; ?>
                </td>
                <td>
                    <a href="<?php echo  str_replace("admin.","", base_url()).'hotels/'.$hotel['city']['alias']."/".$hotel['alias']; ?>" target="_blank"><?php echo $hotel['title']; ?></a>
                    <div class="muted">
                        <?php echo f_addr($hotel['address']); ?>
                    </div>
                </td>
                <td>
                    <div><b>тл:</b> <?php echo f_phone($hotel['phone']); ?></div>
                    <?php if($hotel['email']): ?>
                        <div><b>em:</b> <a href="mailto:<?php echo $hotel['email']; ?>"><?php echo $hotel['email']; ?></a></div>
                    <?php endif; ?>
                    <?php if($hotel['skype']): ?>
                        <div><b>sk:</b> <?php echo $hotel['skype']; ?></div>
                    <?php endif; ?>
                </td>
                <td><?php echo $hotel['room_count']; ?></td>
                <td><?php echo count($hotel['photos']); ?></td>
                <td></td>
                <td>
                    <?php if($hotel['published'] == 1): ?>
                        <span class='label label-success'>Вкл.</span>
                    <?php elseif($hotel['published'] == 2): ?>
                        <span class='label label-info'>Парсер.</span>
                    <?php else: ?>
                        <span class='label label-important'>Выкл.</span>
                    <?php endif; ?>
                </td>
                <td>
                    <a href='<?php echo base_url() . "hotels/edit/" . $hotel['id']; ?>' class='btn btn-mini btn-info' data-toggle="tooltip" title="редактировать"><i class="icon-white icon-pencil"></i></a>
                    &nbsp;
                    <a href='<?php echo base_url() . "hotels/destroy/" . $hotel['id']; ?>' class='btn btn-mini btn-inverse' data-toggle="tooltip" title="удалить"><i class="icon-white icon-trash"></i></a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div class="form-inline">
    <select class="input-large" name="action">
        <option value="none">-- Укажите действие --</option>
        <option value="remove">Удалить</option>
        <option value="unpublish">Снять с публикации</option>
        <option value="make-client">Таки клиент</option>
    </select>
    <input class="btn" type="submit" value="Выполнить" />
</div>
</form>

<div class="pagination">
    <?php for($i = 1; $i <= ceil($pages_total / 50); $i++): ?>
        <?php if(isset($page) && $page == $i): ?>
            <span class="btn btn-primary"><?php echo $i; ?></span>
        <?php else: ?>
            <a class="btn" href="/hotels/index/<?php echo $i; ?>?<?php echo (isset($_GET['region'])) ? "region=" . $_GET['region'] . "&" : "" ?><?php echo (isset($_GET['city'])) ? "city=" . $_GET['city'] . "&" : "" ?>">
                <?php echo $i; ?>
            </a>
        <?php endif; ?>
    <?php endfor; ?>
</div>

<br/>
<br/>
<br/>

<script type="text/javascript">
    $(document).ready(function() {
        $('.city-filter').val()
    });
</script>
