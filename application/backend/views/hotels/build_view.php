
<h3>Новый отель</h3>

<p>
    <?php echo validation_errors(); ?>
</p>

<form class="form-horizontal" method="POST" action="">
<div class="control-group">
        <label class="control-label" for="title">Наименование:</label>

        <div class="controls">
            <input type="text" class="input-xxlarge" name="hotel[title]" id="title" value="">
        </div>
</div>

<div class="control-group">
    <label class="control-label" for="alias">Alias (url):</label>

    <div class="controls">
        <input type="text" name="hotel[alias]" id="alias">
        (на англ. без пробелов)
    </div>
</div>

<div class="control-group">
        <label class="control-label" for="alias">Litebooking id:</label>

        <div class="controls">
            <input type="text" name="hotel[lb_id]" id="alias" value="<?php echo or_empty($hotel['lb_id']); ?>">
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="meta_title">Title:</label>

        <div class="controls">
            <textarea class="input-xxlarge" name="hotel[meta_title]" id="meta_title" placeholder="Заголовок страницы"><?php echo or_empty($hotel['meta_title']); ?></textarea>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="keywords">Keywords:</label>

        <div class="controls">
            <textarea class="input-xxlarge" name="hotel[keywords]" id="keywords" placeholder="9-10 ключевиков через запятую"><?php echo or_empty($hotel['keywords']); ?></textarea>
        </div>
    </div>
   

    <div class="control-group">
        <label class="control-label" for="keywords">Description:</label>

        <div class="controls">
            <textarea class="input-xxlarge" name="hotel[description]" id="description" placeholder="Описание для поисковиков"><?php echo or_empty($hotel['description']); ?></textarea>
        </div>
    </div>

<hr>

<div class="control-group">
    <label class="control-label" for="content"></label>

    <div class="controls">
        <input name="hotel[is_client]" value="1" type="checkbox" style="margin-top: -2px;" checked >
        <span style="line-height: 32px">Таки клиент</span>
    </div>
</div>



<hr>

<div class="control-group">
    <label class="control-label" for="type">Тип:</label>

    <div class="controls">
        <select  name="type[]" >
            <?php foreach($types as $type): ?>
                <option value="<?php echo $type['id']; ?>">
                    <?php echo $type['type']; ?>
                </option>
            <?php endforeach; ?>
        </select>
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="rank">Кол-во звезд:</label>

    <div class="controls">

        <select class="input-small" name="hotel[rank]" id="rank">
            <option value='0'>0 (нет звезд)</option>
            <?php for ($i = 2; $i < 6; $i++): ?>
                <option value='<?php echo $i;?>'>
                    <?php echo $i;?>
                </option>
            <?php endfor; ?>
        </select>
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="room_count">Кол-во номеров:</label>

    <div class="controls">
        <input type="text" class="input-mini" name="hotel[room_count]" id="room_count">
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="price_min">Минимальная цена:</label>

    <div class="controls">
        <input type="text" class="input-mini" name="hotel[price_min]" id="price_min"> руб.
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="price_max">Максимальная цена:</label>

    <div class="controls">
        <input type="text" class="input-mini" name="hotel[price_max]" id="price_max"> руб.
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="city_id">Город:</label>

    <div class="controls">
        <select name="hotel[city_id]" id="city_id" style="width: 400px;">
            <option value='1' selected>Выберите город</option>
            <?php foreach ($cities as $city): ?>
                <option data-region="<?php echo $city['region']['name']; ?>" value="<?php echo $city['id']; ?>">
                    <?php echo $city['name']; ?>
                    (<?php echo $city['region']['name']; ?>)
                </option>
            <?php endforeach; ?>
        </select>

    </div>
</div>


<div class="control-group">
    <label class="control-label" for="address">Адрес:</label>

    <div class="controls">
        <span>ул.</span><input type="text" class="input-large" name="addr[street]" id="address" />
        <span>дом</span><input type="text" class="input-small validate[custom[addr_n]]" name="addr[build]" />
        <span>стр.</span><input type="text" class="input-small validate[custom[addr_n]]" name="addr[section]" />
    </div>
</div>


<hr>


<div class="control-group">
    <label class="control-label" for="phone">Телефон:</label>

    <div class="controls">
        <span>+7 ( </span><input type="text" class="input-small validate[required,custom[p_code]]" size="4"
                                 maxlength="4" name="phone[code]" /><span> )</span>
        <input type="text" class="input-medium validate[required,custom[p_num]]" name="phone[num]" id="phone" />
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="email">Email:</label>

    <div class="controls">
        <input type="text" class="input-xxlarge validate[custom[email]]" name="hotel[email]" id="email" />
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="skype">Skype:</label>

    <div class="controls">
        <input type="text" class="input-xxlarge" name="hotel[skype]" id="skype" />
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="site">Сайт:</label>

    <div class="controls">
        <input type="text" class="input-xxlarge" name="hotel[site]" id="site" />
    </div>
</div>


<hr>


<div class="control-group">
    <label for="content" style="display: block; margin-bottom: 10px">Описание:</label>
    <div>
        <textarea class="ck_editor" name="hotel[content]" id="content" rows="14"></textarea>
    </div>
</div>

<div class="control-group">
    <label for="terms" style="display: block; margin-bottom: 10px">Условия проживания:</label>

    <div>
        <textarea class="ck_editor" name="hotel[terms]" id="terms" rows="14"></textarea>
    </div>
</div>

<hr>

<div class="widgets-section">
    <h4>Настройки виджетов <a href="#" class="toggle" data-label="свернуть">развернуть</a></h4>
    <br/>
    <div class="control-group">
            <label class="control-label" for="content">Виджет админки:</label>

            <div class="controls">
                <textarea class="input-xxlarge" name="hotel[admin_widget]" id="admin_widget" rows="14"><?php echo $hotel['admin_widget']; ?></textarea>
            </div>
    </div>
    
    <div class="control-group">
        <label class="control-label" for="content">Виджет номеров:</label>

        <div class="controls">
            <textarea class="input-xxlarge" name="hotel[rooms_widget]" id="rooms_widget" rows="14"></textarea>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="content">Виджет бронирования:</label>

        <div class="controls">
            <textarea class="input-xxlarge" name="hotel[book_widget]" id="book_widget" rows="14"></textarea>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="content">Виджет отмены:</label>

        <div class="controls">
            <textarea class="input-xxlarge" name="hotel[revoke_widget]" id="revoke_widget" rows="14"></textarea>
        </div>
    </div>
</div>

<hr>

<div class="control-group" style="border: 1px solid red; padding: 10px 0">
    <label class="control-label" for="published">Опубликовать:</label>

    <div class="controls">

        <select class="input-small" name="hotel[published]" id="published">
            <option value='1'>Да</option>
            <option value='0'>Нет</option>
        </select>

    </div>
</div>

<div class="control-group">
    <div class="controls">
        <button type="submit" class="btn">Сохранить</button>
    </div>
</div>

</form>

<script>
    $(document).ready(function(){
        $('#city_id').chosen();
        $('select.multiple').chosen({ max_selected_options: 3 });

        $('.widgets-section .toggle').click(function() {
            text = $(this).text();

            $(this).text($(this).data('label'));
            $(this).data('label', text);

            $('.widgets-section').toggleClass('active');
            return false;
        });
    });
</script>