


  <h3>Удаление отеля</h3>


<div class="alert alert-block alert-error fade in">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <h4 class="alert-heading">Вы действительно хотите удалить отель?</h4>
            <p>Удаление отеля из базы данных. Подумайте 200 раз, прежде чем удалить информацию. Это действие необратимо.</p>
            <p>
              <a class="btn btn-danger" href="<?php echo base_url() . "hotels/destroy/" . $id . "/1";?>">Да, удалить</a> <a class="btn" href="<?php echo $this->agent->referrer(); ?>">Отмена</a>
            </p>
</div>
