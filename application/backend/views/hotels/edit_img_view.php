
<h3><?php echo db2form($hotel['title']); ?> (<?php echo db2form($hotel['alias']); ?>)</h3>

<ul class="nav nav-tabs">
    <li><a href="<?php echo base_url() . "hotels/edit/" . $hotel['id']; ?>">Информация</a></li>
    <li class="active"><a href="<?php echo base_url() . "hotels/edit_img/" . $hotel['id']; ?>">Фотографии</a></li>
    <li><a href="<?php echo base_url() . "hotels/edit_req/" . $hotel['id']; ?>">Реквизиты</a></li>
    <li><a href="<?php echo base_url() . "hotels/services/" . $hotel['id']; ?>">Услуги</a></li>
</ul>

<div class='well well-small'>
    Зарузить новое фото:<br>
    <form action='' method='post' accept-charset='utf-8' enctype='multipart/form-data'>
        <input type='file' name='userfile' size='20' accept="image/jpeg,image/png"/>
        <input type='submit' class='btn' value='Загрузить' />
    </form>
</div>

<?php if(isset($hotel['photos'])): ?>
<table class="table table-condensed">
    <thead>
    <tr>
        <th width='160'>Фото:</th>
        <th>...</th>
    </tr>
    </thead>
    <tbody>
        <?php foreach($hotel['photos'] as $photo) :?>
            <tr>
                <td><img src="<?php echo catalog_url(); ?>upload/hotel_photos/<?php echo $photo['photo']; ?>" width='160' height='120'></td>
                <td><a href="/hotels/destroy_img/<?php echo $photo['id']; ?>">Удалить</a></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php endif; ?>