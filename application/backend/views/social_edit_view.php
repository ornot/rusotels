<h3>Социальные профили</h3>

<form class="form-horizontal" method="POST" action="<?php echo base_url() . "social/update"; ?>">

    <div class="control-group">
        <label class="control-label" for="twitter">Twitter:</label>

        <div class="controls">
            <input type="text" class="input-xlarge" name="twitter" id="twitter"
                   value="https://twitter.com/rusotels<?php //echo db2form($teacher['twitter']); ?>"> <a
                href='https://twitter.com/rusotels' target="_blank" class='btn'>Перейти</a>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="vk">Вконтакте:</label>

        <div class="controls">
            <input type="text" class="input-xlarge" name="vk" id="vk"
                   value="http://vk.com/rusotels<?php //echo db2form($teacher['vk']); ?>"> <a
                href='http://vk.com/rusotels' target="_blank" class='btn'>Перейти</a>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="fb">Facebook:</label>

        <div class="controls">
            <input type="text" class="input-xlarge" name="fb" id="middlename"
                   value="http://www.facebook.com/pages/Rusotels/337217726398970<?php //echo db2form($teacher['fb']); ?>">
            <a href='http://www.facebook.com/pages/Rusotels/337217726398970' target="_blank" class='btn'>Перейти</a>
        </div>
    </div>

    <input name="id" type="hidden" value="<?php //echo $teacher['id']; ?>">

</form>
