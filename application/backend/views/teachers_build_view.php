
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>rusotels: manager</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Le styles -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/bootstrap-responsive.css" rel="stylesheet">
    <style>
      body { padding-top: 60px; }
    </style>

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->

  </head>

  <body>


<?php
  echo managerNav(); //верхняя нав панель
?>

<div class="container"> <!-- container -->
<div class="row"> <!-- row -->




<div class="span3">
<!-- НАЧАЛО ПЕРВОЙ КОЛОНКИ -->
  <?php
    echo managerMenu(); //боковое левое меню
  ?>
<!-- КОНЕЦ ПЕРВОЙ КОЛОНКИ -->
</div>


<div class="span9">
<!-- НАЧАЛО ВТОРОЙ КОЛОНКИ -->


  <h3>Добавление лектора</h3>

  <!-- <div class="btn-toolbar">
    <a href="#" class="btn btn-primary pull-right">Добавить лектора</a>
  </div> -->

<form class="form-horizontal" method="POST" action="<?php echo base_url() . "teachers/create";?>">

  <div class="control-group">
    <label class="control-label" for="lastname">Фамилия:</label>
    <div class="controls">
      <input type="text" name="lastname" id="lastname" value="">
    </div>
  </div>

  <div class="control-group">
    <label class="control-label" for="firstname">Имя:</label>
    <div class="controls">
      <input type="text" name="firstname" id="firstname" value="">
    </div>
  </div>

  <div class="control-group">
    <label class="control-label" for="middlename">Отчество:</label>
    <div class="controls">
      <input type="text" name="middlename" id="middlename" value="">
    </div>
  </div>

  <div class="control-group">
    <label class="control-label" for="alias">Alias (url):</label>
    <div class="controls">
      <input type="text" name="alias" id="alias" value=""> (на англ. без пробелов)
    </div>
  </div>

  <div class="control-group">
    <label class="control-label" for="email">Email:</label>
    <div class="controls">
      <input type="text" name="email" id="email" value=""> (не показывается на сайте)
    </div>
  </div>

  <div class="control-group">
    <label class="control-label" for="position">Номер в списке:</label>
    <div class="controls">
      <input type="text" name="position" id="position" value="">
    </div>
  </div>



  <div class="control-group">
    <label class="control-label" for="content_short">Краткий текст:</label>
    <div class="controls">
      <textarea class="input-xxlarge" name="content_short" id="content_short" rows="5" placeholder='Краткий текст о лекторе на 1 абзац для вывода на главной странице'></textarea>
    </div>
  </div>


  <div class="control-group">
    <label class="control-label" for="content">Расширенный текст:</label>
    <div class="controls">
      <textarea class="input-xxlarge" name="content" id="content" rows="10" placeholder='Полный текст о лекторе'></textarea>
    </div>
  </div>


  <div class="control-group">
    <div class="controls">
      <button type="submit" class="btn">Сохранить</button>
    </div>
  </div>

</form>






<!-- КОНЕЦ ВТОРОЙ КОЛОНКИ -->
</div>




</div> <!-- /row -->
</div> <!-- /container -->



<?php
  echo menagerJs(); //подключение js
?>

  </body>
</html>