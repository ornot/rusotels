<h3>Редактирование страницы - <?php echo $page['title']; ?></h3>

<form class="form" method="POST" action="">

    <div class="control-group">
        <label class="control-label" for="name">Заголовок:</label>

        <div class="controls">
            <input type="text" class="input-xxlarge" name="page[title]" id="title" value="<?php echo $page['title']; ?>">
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="alias">Alias (url):</label>

        <div class="controls">
            <input type="text" name="page[keyx]" id="keyx" value="<?php echo $page['keyx']; ?>"> (на англ. без пробелов)
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="content">Тело страницы:</label>

        <div class="controls">
            <textarea class="ck_editor" name="page[content]" rows="40" style="width: 990px;"><?php echo $page['content']; ?></textarea>
        </div>
    </div>

    <div class="control-group">
        <div class="controls">
            <button type="submit" class="btn">Сохранить</button>
        </div>
    </div>

</form>