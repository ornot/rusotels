<h3>Статические страницы</h3>

<div class="btn-toolbar">
    <a href="/pages/add" class="btn btn-primary pull-right">Добавить страницу</a>
</div>

<table class="table table-hover">
    <thead>
    <tr>
        <th>Заголовок</th>
        <th>URL</th>
        <th width='95px'></th>
    </tr>
    </thead>

    <tbody>
    <?php foreach ($pages as $page): ?>
        <tr>
            <td><?php echo $page['title']; ?></td>
            <td>
                <a href="http://rusotels.ru/<?php echo $page['keyx']; ?>" target="_blank">
                    http://rusotels.ru/<b><?php echo $page['keyx']; ?></b>
                </a>
            </td>
            <td>
                <a href='<?php echo base_url() . "pages/edit/" . $page['id']; ?>' class='btn btn-info btn-mini'
                   data-toggle="tooltip" title="редактировать">
                    <i class="icon-pencil icon-white"></i>
                </a>
                &nbsp;&nbsp;&nbsp;
                <a href='<?php echo base_url() . "pages/remove/" . $page['id']; ?>'
                   data-text="<?php echo $page['title']; ?>"
                   class='remove btn btn-inverse btn-mini' data-toggle="tooltip" title="удалить">
                    <i class="icon-trash icon-white"></i>
                </a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<?php //echo $pagination; ?>

<br/>
<br/>
<br/>

<script>
    $(document).ready(function(){
        $('a.remove').click(function(){
            return confirm('Удалить страницу - "' + $(this).data('text') + '" ?');
        });
    });
</script>