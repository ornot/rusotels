
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>rusotels: manager</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Le styles -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/bootstrap-responsive.css" rel="stylesheet">
    <style>
      body { padding-top: 60px; }
    </style>

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->

  </head>

  <body>


<?php
  echo managerNav(); //верхняя нав панель
?>

<div class="container"> <!-- container -->
<div class="row"> <!-- row -->




<div class="span3">
<!-- НАЧАЛО ПЕРВОЙ КОЛОНКИ -->
  <?php
    echo managerMenu(); //боковое левое меню
  ?>
<!-- КОНЕЦ ПЕРВОЙ КОЛОНКИ -->
</div>


<div class="span9">
<!-- НАЧАЛО ВТОРОЙ КОЛОНКИ -->


  <h3>Лекторы</h3>

  <div class="btn-toolbar">
    <a href="<?php echo base_url() . "teachers/build"; ?>" class="btn btn-primary pull-right">Добавить лектора</a>
  </div>


  <table class="table table-condensed">
    <thead>
      <tr>
        <th>ФИО лектора:</th>
        <th width='115px'> </th>
        <th width='115px'> </th>
        <th width='80px'> </th>
      </tr>
    </thead>

    <tbody>
  <?php
    foreach ($teachers as $teacher) {
      echo "<tr>";
      echo "<td><a href='" . base_url() . "teachers/edit/" . $teacher['id'] . "'>" . $teacher['lastname'] . " " . $teacher['firstname'] . " " . $teacher['middlename'] . "</a></td>";
      echo "<td><a href='" . base_url() . "teachers/show/" . $teacher['id'] . "' class=''>Просмотреть</a></td>";
      echo "<td><a href='" . base_url() . "teachers/edit/" . $teacher['id'] . "' class=''>Редактировать</a></td>";
      echo "<td><a href='" . base_url() . "teachers/destroy/" . $teacher['id'] . "' class=''>Удалить</a></td>";
      echo "</tr>";
    }
  ?>
    </tbody>
  </table>






<!-- КОНЕЦ ВТОРОЙ КОЛОНКИ -->
</div>




</div> <!-- /row -->
</div> <!-- /container -->

<?php
  echo menagerJs(); //подключение js
?>

  </body>
</html>