
<h3>Баннеры</h3>

<!--<div class="btn-toolbar">-->
<!--    <a href="--><?php //echo base_url() . "hotels/build"; ?><!--" class="btn btn-primary pull-right">Добавить страницу</a>-->
<!--</div>-->

<table class="table table">
    <thead>
    <tr>
        <th width="80px">#ID</th>
        <th>Банер</th>
        <th>Статус</th>
        <th width='85px'></th>
    </tr>
    </thead>

    <tbody>
    <?php foreach ($banners as $banner): ?>
        <tr>
            <td><?php echo $banner['id']; ?></td>
            <td>
                <img src="http://rusotels/upload/banners/<?php echo $banner['banner']; ?>" alt="" width="320" height="240"/>
            </td>
            <td>
            <?php if($banner['active'] == 1): ?>
                <span class="label label-success">Вкл.</span>
            <?php else: ?>
                <span class="label label-important">Выкл.</span>
            <?php endif; ?>
            </td>
            <td>
                <a href='<?php echo base_url() . "banners/edit/" . $banner['id']; ?>'
                   class='btn btn-mini btn-info'
                   data-toggle="tooltip"
                   title="редактировать">
                    <i class="icon-pencil icon-white"></i>
                </a>
                &nbsp;
                <a href='<?php echo base_url() . "banners/remove/" . $banner['id']; ?>'
                   data-text="<?php echo $banner['id']; ?>"
                   class='remove btn btn-inverse btn-mini'
                   data-toggle="tooltip"
                   title="удалить">
                    <i class="icon-trash icon-white"></i>
                </a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<?php //echo $pagination; ?>

<br/>
<br/>
<br/>

<script>
    $(document).ready(function(){
        $('a.remove').click(function(){
            return confirm('Удалить баннер - "' + $(this).data('text') + '" ?');
        });
    });
</script>