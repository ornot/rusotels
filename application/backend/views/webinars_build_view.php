
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>rusotels: manager</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Le styles -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/bootstrap-responsive.css" rel="stylesheet">
    <style>
      body { padding-top: 60px; }
    </style>

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->

  </head>

  <body>


<?php
  echo managerNav(); //верхняя нав панель
?>

<div class="container"> <!-- container -->
<div class="row"> <!-- row -->




<div class="span3">
<!-- НАЧАЛО ПЕРВОЙ КОЛОНКИ -->
  <?php
    echo managerMenu(); //боковое левое меню
  ?>
<!-- КОНЕЦ ПЕРВОЙ КОЛОНКИ -->
</div>


<div class="span9">
<!-- НАЧАЛО ВТОРОЙ КОЛОНКИ -->


  <h3>Добавление нового вебинара</h3>

  <!-- <div class="btn-toolbar">
    <a href="#" class="btn btn-primary pull-right">Добавить лектора</a>
  </div> -->


<form class="form-horizontal" method="POST" action="<?php echo base_url() . "webinars/create"; ?>">

  <div class="control-group">
    <label class="control-label" for="title">Название:</label>
    <div class="controls">
      <input class="input-xxlarge" type="text" name="title" id="title" value="">
    </div>
  </div>

  <div class="control-group">
    <label class="control-label" for="alias">Alias (url):</label>
    <div class="controls">
      <input type="text" name="alias" id="alias" value=""> (на англ без пробелов)
    </div>
  </div>

  <div class="control-group">
    <label class="control-label" for="keywords">Keywords:</label>
    <div class="controls">
      <input type="text" class="input-xxlarge" name="keywords" id="keywords" value="" placeholder="4-5 ключевиков через запятую">
    </div>
  </div>

  <hr>

  <div class="control-group">
    <label class="control-label" for="date_start">Дата начала:</label>
    <div class="controls">
      <input type="text" name="date_start" id="date_start" value="">
    </div>
  </div>

  <div class="control-group">
    <label class="control-label" for="date_end">Дата окончания:</label>
    <div class="controls">
      <input type="text" name="date_end" id="date_end" value=""> (в формате гггг-мм-дд)
    </div>
  </div>


  <div class="control-group">
    <label class="control-label" for="time_start">Время начала:</label>
    <div class="controls">
      <input type="text" name="time_start" id="time_start" value=""> (например, 10:00 МСК)
    </div>
  </div>


  <div class="control-group">
    <label class="control-label" for="time_length">Продолжительность:</label>
    <div class="controls">
      <input type="text" name="time_length" id="time_length" value="">
    </div>
  </div>

  <div class="control-group">
    <label class="control-label" for="price">Цена:</label>
    <div class="controls">
      <input type="text" class="input-mini" name="price" id="price" value=""> руб.
    </div>
  </div>


  <div class="control-group">
    <label class="control-label" for="document">Документ:</label>
    <div class="controls">
      <input type="text" name="document" id="document" value=""> (например, Сертификат или диплом)
    </div>
  </div>


  <div class="control-group">
    <label class="control-label" for="teacher_1">Лектор:</label>
    <div class="controls">

      <select name="teacher_1" id="teacher_1">
        <option value='' selected>Выберите лектора</option>
        <?php
        $teachers = $this->cp_model->getAllTeachers();
        foreach ($teachers as $teacher) {

          echo "<option value='" . $teacher['id'] . "'>" . $teacher['firstname'] . " " . $teacher['lastname'] . "</option>";
        }
        ?>
      </select>

    </div>
  </div>








  <div class="control-group">
    <label class="control-label" for="content">Программа семинара:</label>
    <div class="controls">
      <textarea class="input-xxlarge" name="content" id="content" rows="10"></textarea>
    </div>
  </div>


  <div class="control-group">
    <label class="control-label" for="content_b1">Кому полезен:</label>
    <div class="controls">
      <textarea class="input-xxlarge" name="content_b1" id="content_b1" rows="5"></textarea>
    </div>
  </div>

  <div class="control-group">
    <label class="control-label" for="content_b2">Навыки:</label>
    <div class="controls">
      <textarea class="input-xxlarge" name="content_b2" id="content_b2" rows="5"></textarea>
    </div>
  </div>

  


  <div class="control-group">
    <div class="controls">
      <!-- <label class="checkbox">
        <input type="checkbox"> Remember me
      </label> -->
      <button type="submit" class="btn">Сохранить</button>
    </div>
  </div>

</form>






<!-- КОНЕЦ ВТОРОЙ КОЛОНКИ -->
</div>




</div> <!-- /row -->
</div> <!-- /container -->



<?php
  echo menagerJs(); //подключение js
?>

  </body>
</html>