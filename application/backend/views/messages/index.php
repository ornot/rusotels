<h3>Сообщения пользователей</h3>

<div class="btn-toolbar">
<!--    <a href="/pages/add" class="btn btn-primary pull-right">Добавить страницу</a>-->
</div>

<table class="table table-hover">
    <thead>
    <tr>
        <th>#</th>
        <th>Кто/Когда</th>
        <th>Сообщение</th>
        <th width='95px'></th>
    </tr>
    </thead>

    <tbody>
    <?php foreach ($messages as $message): ?>
        <tr>
            <td><?php echo $message['id']; ?></td>
            <td><?php echo $message['user']['email']; ?></td>
            <td><?php echo $message['message']; ?></td>
            <td>
                <a href='<?php echo base_url() . "messages/hide/" . $message['id']; ?>' class='btn btn-info btn-small'>
                    <i class="icon- icon-white"></i>
                </a>
                &nbsp;&nbsp;&nbsp;
                <a href='<?php echo base_url() . "messages/remove/" . $message['id']; ?>'
                   data-text="#<?php echo $message['id']; ?>"
                   class='remove btn btn-inverse btn-small'>
                    <i class="icon-trash icon-white"></i>
                </a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<?php //echo $pagination; ?>

<br/>
<br/>
<br/>

<script>
    $(document).ready(function(){
        $('a.remove').click(function(){
            return confirm('Удалить сообщение - "' + $(this).data('text') + '" ?');
        });
    });
</script>