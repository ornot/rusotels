<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>rusotels: manager</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Le styles -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/bootstrap-responsive.css" rel="stylesheet">
    <style>
      body { padding-top: 60px; }
    </style>

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->

  </head>

  <body>


<?php
  echo managerNav(); //верхняя нав панель
?>

<div class="container"> <!-- container -->
<div class="row"> <!-- row -->




<div class="span3">
<!-- НАЧАЛО ПЕРВОЙ КОЛОНКИ -->
  <?php
    echo managerMenu(); //боковое левое меню
  ?>
<!-- КОНЕЦ ПЕРВОЙ КОЛОНКИ -->
</div>


<div class="span9">
<!-- НАЧАЛО ВТОРОЙ КОЛОНКИ -->


  <h3>Редактирование вебинара</h3>

  <!-- <div class="btn-toolbar">
    <a href="#" class="btn btn-primary pull-right">Добавить лектора</a>
  </div> -->


  <ul class="nav nav-tabs">
    <li><a href="<?php echo base_url() . "webinars/edit/" . $webinar['id']; ?>">Информация</a></li>
    <li class="active"><a href="<?php echo base_url() . "webinars/edit_img/" . $webinar['id']; ?>">Промо картинка</a></li>
    <!-- <li><a href="#">Поиск</a></li> -->
  </ul>


<?php
	//echo "filezzzz: " . $webinar['image'] . "<br>";
?>




<?php
	$edu_domain = edu_url();




	if ( !empty ($webinar['image']))
	{
		echo "<img src='" . $edu_domain . "upload/webinars/" . $webinar['image'] . "'>" . "<br><br>";
		// echo "
		// 	<form action='" . base_url() . "webinars/do_upload' method='post' accept-charset='utf-8' enctype='multipart/form-data'>
		// 		<input type='file' name='userfile' size='20' />
		// 		<input name='id' type='hidden' value='" . $webinar['id'] . "'>
		// 		<input type='submit' class='btn' value='Загрузить' />
		// 	</form>";
		echo "<a href='" . base_url() . "webinars/destroy_img/" . $webinar['id'] . "'>Удалить изображение</a>";

	}
	else
	{
		echo "Загрузите изображение:<br>";
		echo "
			<form action='" . base_url() . "webinars/do_upload' method='post' accept-charset='utf-8' enctype='multipart/form-data'>
				<input type='file' name='userfile' size='20' />
				<input name='id' type='hidden' value='" . $webinar['id'] . "'>
				<input type='submit' class='btn' value='Загрузить' />
			</form>";
	}

?>



<!-- КОНЕЦ ВТОРОЙ КОЛОНКИ -->
</div>




</div> <!-- /row -->
</div> <!-- /container -->



<?php
  echo menagerJs(); //подключение js
?>

  </body>
</html>