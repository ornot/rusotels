
<h3>Пользователи</h3>

<!--<div class="btn-toolbar">-->
<!--    <a href="--><?php //echo base_url() . "hotels/build"; ?><!--" class="btn btn-primary pull-right">Добавить страницу</a>-->
<!--</div>-->

<table class="table table">
    <thead>
    <tr>
        <th width="80px">#ID</th>
        <th width="80px">Дата активации</th>
        <th>Отель</th>
        <th>E-mail</th>
        <th>Роль</th>
        <th width="80px">Последняя активность</th>
        <th>Статус</th>
        <th width='85px'></th>
    </tr>
    </thead>

    <tbody>
    <?php foreach ($users as $user): ?>
        <tr>
            <td><?php echo $user['id']; ?></td>
            <td class="date"><?php echo ($user['activated'] > 0) ? date('<\i>H:i:s</\i><b\r><b>d.m.Y</b>', $user['activated']) : ""; ?></td>
            <td>
                <?php if($user['hotel']): ?>
                    <a href="/hotels/edit/<?php echo $user['hotel']['id']; ?>"><?php echo $user['hotel']['title']; ?></a>
                <?php else: ?>
                    <i>no hotel</i>
                <?php endif; ?>
            </td>
            <td><?php echo $user['email']; ?></td>
            <td><?php echo $user['role']; ?></td>
            <td class="date"><?php echo ($user['last_active'] > 0) ? date('<\i>H:i:s</\i><b\r><b>d.m.Y</b>', $user['last_active']): ""; ?></td>
            <td>
            <?php if($user['status'] == 1): ?>
            <span class="label label-success">Вкл.</span>
            <?php else: ?>
            <span class="label label-important">Выкл.</span>
            <?php endif; ?>
            </td>
            <td>
                <a href='<?php echo base_url() . "users/edit/" . $user['id']; ?>'
                   class='btn btn-mini btn-info'
                   data-toggle="tooltip"
                   title="редактировать">
                    <i class="icon-pencil icon-white"></i>
                </a>
                &nbsp;
                <?php if ($user['lock'] == 0): ?>
                    <a href='<?php echo base_url() . "users/remove/" . $user['id']; ?>'
                       data-text="<?php echo $user['email'];?>"
                       class='remove btn btn-inverse btn-mini'
                       data-toggle="tooltip"
                       title="удалить">
                        <i class="icon-trash icon-white"></i>
                    </a>
                <?php endif; ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<?php //echo $pagination; ?>

<br/>
<br/>
<br/>

<script>
    $(document).ready(function(){
        $('a.remove').click(function(){
            return confirm('Удалить пользователя - "' + $(this).data('text') + '" ?');
        });
    });
</script>