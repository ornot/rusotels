<h3>Редактирование пользоателя - <?php echo $user['email']; ?></h3>

<form class="form-horizontal" method="POST" action="">

    <div class="control-group">
        <label class="control-label" for="alias">Статус:</label>

        <div class="controls">
            <input type="checkbox" name="user[status]" value="1" style="margin-top: -2px;"
                   <?php if($user['status'] == 1):?>checked<?php endif;?>>
            <span style="line-height: 32px">Активен</span>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="content">Роль:</label>

        <div class="controls">
            <select name="user[role]">
                <option value="user" <?php if($user['role'] == 'user'):?>selected<?php endif;?>>
                    Отельер
                </option>
                <option value="admin" <?php if($user['role'] == 'admin'):?>selected<?php endif;?>>
                    Администратор
                </option>
            </select>
        </div>
    </div>

    <div class="control-group">
        <div class="controls">
            <button type="submit" class="btn">Сохранить</button>
        </div>
    </div>

</form>