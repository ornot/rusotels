<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/general/hooks.html
|
*/

$hook['pre_system'] = array(
    'class'    => 'RO_Autoloader',
    'function' => 'register',
    'filename' => 'RO_Autoloader.php',
    'filepath' => '../third_party/hooks',
    'params'   => array(APPPATH.'../third_party/core/')
);

$hook['post_controller_constructor'][] = array(
    'class'    => 'RO_ACL',
    'function' => 'check',
    'filename' => 'RO_ACL.php',
    'filepath' => '../third_party/hooks',
    'params'   => array()
);

/* End of file hooks.php */
/* Location: ./application/config/hooks.php */