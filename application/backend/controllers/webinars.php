<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Webinars extends RO_Controller {


	public function index()
	{	
		$data['webinars'] = $this->cp_model->getAllWebinars();
		$this->render('webinars_index_view', $data);
	}


	public function show($id=0)
	{	
		//$data['webinar'] = $this->cp_model->getTeacherById($id);
		//$this->load->view('webinars_show_view', $data);
	}


	public function edit($id=0)
	{	
		$data['webinar'] = $this->cp_model->getWebinarById($id);
		$this->load->view('webinars_edit_view', $data);
	}


	public function edit_img($id=0)
	{	
		$data['webinar'] = $this->cp_model->getWebinarById($id);
		$this->load->view('webinars_edit_img_view', $data);
	}

	public function do_upload() //update_img
	{	
		$root_url = root_url();
		$cite_path = edu_path();

		$webinar_tmp_path = $root_url . "/" . $cite_path . "/upload/webinars/tmp/";
		$webinar_tmp2_path = $root_url . "/" . $cite_path . "/upload/webinars/tmp2/";
		$webinar_path = $root_url . "/" . $cite_path . "/upload/webinars/";

		$now = date("YmdHis");
        $config['upload_path'] = $webinar_tmp_path;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size']    = '2048000';
        $config['max_width']  = '3200';
        $config['max_height']  = '2400';
        $config['file_name'] = $now;
        
        $this->load->library('upload', $config);
        $this->load->library('image_lib');


 

	
		if ( ! $this->upload->do_upload())
		{
			$error['error'] = $this->upload->display_errors();
			
			echo "Error: not selected file or error format<br>";
		}	
		else
		{
			// $data['upload_data'] = $this->upload->data();
			
			// echo "goooddd<br>";






			$data = array('upload_data' => $this->upload->data());
            $image = $this->upload->data();

            if ($image['image_width'] > $image['image_height']) {
                $c_photo['new_image'] = $webinar_tmp_path;
                $c_photo['width']     = $image['image_height'];
                $c_photo['height']    = $image['image_height'];
                $c_photo['maintain_ratio'] = false;
                            
                $axis = (int)(($image['image_width'] - $image['image_height']) / 2);
                $c_photo['x_axis']    = $axis;
                $c_photo['y_axis']    = 0;
                            
                $this->image_lib->initialize($c_photo);
                $this->image_lib->crop(); 
                        
            } elseif ($image['image_width'] < $image['image_height']) {
                $c_photo['new_image'] = $webinar_tmp_path;
                $c_photo['width']     = $image['image_width'];
                $c_photo['height']    = $image['image_width'];
                $c_photo['maintain_ratio'] = false;
                        
                $axis = (int)(($image['image_height'] - $image['image_width']) / 2);
                $c_photo['x_axis']    = 0;
                $c_photo['y_axis']    = $axis;;
                            
                $this->image_lib->initialize($c_photo);
                $this->image_lib->crop(); 
            } else {
                $c_photo['new_image'] = $webinar_tmp_path;
                $c_photo['width']     = $image['image_width'];
                $c_photo['height']    = $image['image_width'];
                            
                $this->image_lib->initialize($c_photo);
                $this->image_lib->resize(); 
            }
            $this->image_lib->clear();
                        
            //////////// thumb_100
            // $c_photo['source_image']    = './uploads/webinars/tmp/'.$image['file_name'];
            // $c_photo['new_image'] = './uploads/webinars/thumb_100/';
            // $c_photo['width']     = 100;
            // $c_photo['height']    = 100;
            // $c_photo['maintain_ratio'] = TRUE;

            // $this->image_lib->initialize($c_photo);
            // $this->image_lib->resize(); 
            // $this->image_lib->clear();
                        
            /////////// thumb_50
            // $c_photo['source_image']    = './uploads/webinars/tmp/'.$image['file_name'];
            // $c_photo['new_image'] = './uploads/webinars/thumb_50/';
            // $c_photo['width']     = 50;
            // $c_photo['height']    = 50;

            // $this->image_lib->initialize($c_photo);
            // $this->image_lib->resize(); 
            // $this->image_lib->clear();




            // thumb_395х235
            $c_photo['source_image']    = $webinar_tmp_path .$image['file_name'];
            $c_photo['new_image'] = $webinar_tmp2_path;
            $c_photo['width']     = 395;
            $c_photo['height']    = 1500;
            $c_photo['maintain_ratio'] = TRUE;

            $this->image_lib->initialize($c_photo);
            $this->image_lib->resize(); 
            $this->image_lib->clear();


            //обрежу ка я
            $d_photo['image_library'] = 'gd2'; /*какой библиотекой пользуемся*/
			$d_photo['source_image']	= $webinar_tmp2_path .$image['file_name'];
			//$d_photo['x_axis'] = $image_x_axis;/*смещение слева*/
			//$d_photo['y_axis'] = $image_y_axis;/*смещение сверху*/
			$d_photo['new_image'] = $webinar_path;

			$d_photo['maintain_ratio'] = FALSE;

			$d_photo['width']	 = 395;/*ширина*/
			$d_photo['height']	= 235;/*высота*/
			/*загружаем библиотеку работы с изображениями применяем конфиг*/
			//$this->CI->load->library('image_lib', $config);
			$this->image_lib->initialize($d_photo);
			$this->image_lib->crop();
			$this->image_lib->clear();



            unlink($webinar_tmp_path.$image['file_name']); // удалили временную картинку-квадратик
            unlink($webinar_tmp2_path.$image['file_name']); // удалили временную картинку-квадратик
                     



            //$this->load->view('upload_success', $data);

//echo "goooddd<br>";
            $id=$_POST['id'];
            $imageR = $image['file_name'];
            $this->cp_model->updateWebinarImg($id, $imageR);


			$data['alert_title']='Готово';
			$data['alert_text']='Фото успешно обновлено';
			$this->load->view('alert_green_view', $data);

			//////
		}





		//echo "upd";

	}


	public function destroy_img($id=0, $tr=0)
	{	
		$data['webinar'] = $this->cp_model->getWebinarById($id);
		$webinar = $data['webinar'];

		$webinar_path = root_url() . "/" . edu_path() . "/upload/webinars/"; //папка с фотками вебинаров
		unlink($webinar_path.$webinar['image']); //удаляем


		$this->cp_model->updateWebinarImg($id);
		$a1 = base_url() . "webinars/edit_img/" . $id;
		redirect($a1, 'refresh');
	}



	public function update()
	{
		//echo "update";
		if (!empty($_POST["title"]))
		{
			$id = $_POST['id'];
			$title = form2db($_POST['title']);
			$alias = form2db($_POST['alias']);
			$date_start = form2db($_POST['date_start']);
			$date_end = form2db($_POST['date_end']);

			$time_length = form2db($_POST['time_length']);
			$price = form2db($_POST['price']);
			$content = form2db($_POST['content']);
			$content_b1 = form2db($_POST['content_b1']);
			$content_b2 = form2db($_POST['content_b2']);
			$teacher_1 = form2db($_POST['teacher_1']);

			$time_start = form2db($_POST['time_start']);
			$document = form2db($_POST['document']);
			$keywords = form2db($_POST['keywords']); //ключевики
			
			$this->cp_model->updateWebinar($id, $title, $alias, $date_start, $date_end, $time_length, $price, $content, $content_b1, $content_b2, $teacher_1, $time_start, $document, $keywords);


			$data['alert_title']='Готово';
			$data['alert_text']='Данные успешно обновлены';
			$this->load->view('alert_green_view', $data);
			
		}
		else
		{
			echo "Error: zapolneni ne vse obyazatelnie polya";
		}
	}


	public function build() //new (можно make)
	{	
		$data = '';
		$this->load->view('webinars_build_view', $data);
	}


	public function create()
	{	
		if (!empty($_POST["title"]))
		{
			$title = form2db($_POST['title']);
			$alias = form2db($_POST['alias']);
			$date_start = form2db($_POST['date_start']);
			$date_end = form2db($_POST['date_end']);

			$time_length = form2db($_POST['time_length']);
			$price = form2db($_POST['price']);
			$content = form2db($_POST['content']);
			$content_b1 = form2db($_POST['content_b1']);
			$content_b2 = form2db($_POST['content_b2']);
			$teacher_1 = form2db($_POST['teacher_1']);

			$time_start = form2db($_POST['time_start']);
			$document = form2db($_POST['document']);
			$keywords = form2db($_POST['keywords']); //ключевики

			$this->cp_model->addWebinar($title, $alias, $date_start, $date_end, $time_length, $price, $content, $content_b1, $content_b2, $teacher_1, $time_start, $document, $keywords);

			//echo "good update";
			$a1 = base_url() . "webinars";
			redirect($a1, 'refresh');
		}
		else
		{
			echo "Error: zapolneni ne vse polya";
		}
	}


	public function destroy($id=0, $tr=0)
	{	
		if ($tr==0)
		{
			$data = '';
			$data['id'] = $id;
			$this->load->view('webinars_destroy_view', $data);
		}
		else
		{
			$this->cp_model->deleteWebinar($id);
			//echo "Udalen";
			$a1 = base_url() . "webinars";
			redirect($a1, 'refresh');
		}
	}



	public function search()
	{	
		//$this->load->view('ex_view');
		$q = $_GET['q'];
	}


}

/* End of file citys.php */
/* Location: ./application/controllers/citys.php */