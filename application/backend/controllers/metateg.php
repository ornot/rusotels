<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Metateg extends RO_Controller {

    public function setRules() {
        return array(
            '*' => 'admin',
            '#fallback' => '/login'
        );
        //{москва} {москвы} {москве} {тип} {имя}
    }


    public function index($page = 0) {	
        $this->load->model('metateg_model');
        $data['all_meta']=$this->metateg_model->get_all_meta();
        // redirect('manager/send_mails');
        $this->render('metateg_view',$data);
    }
    
    public function post() {	
        $this->load->model('metateg_model');
        $post=$this->input->post();
        /**
                Array
        (
            [title_1] => 
            [description_1] => 
            [keywords_1] => 
            [title_2] => 
            [description_2] => 
            [keywords_2] => 
            [title_3] => 
            [description_3] => 
            [keywords_3] => 
        )
         * **/	
        if($post){
           
             $meta=$this->metateg_model->get_all_meta();
             
             foreach ($meta as $item){
                $id= $item['id'];
                $dat['title']=$post['title_'.$id];
                $dat['description']=$post['description_'.$id];
                $dat['keywords']=$post['keywords_'.$id];
                $this->metateg_model->edit($dat,$id);    
             }   
        }
        
      redirect('metateg');
        
    }
        
        
}

/* End of file hotels.php */
/* Location: ./application/controllers/hotels.php */