<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Social extends RO_Controller {

    public function setRules() {
        return array(
            '*' => 'admin',
            '#fallback' => '/login'
        );
    }

	public function index() {
		$this->render('social_edit_view');
	}
}