<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Posts extends RO_Controller {

    public function setRules() {
        return array(
            '*' => 'admin',
            '#fallback' => '/login'
        );
    }

	public function index() {
		$data['posts'] = $this->post->findAll();
		$this->render('posts/index', $data);
	}

	public function edit($id = 0) {
		$data['post'] = $this->post->find(array("id" => $id));

        if(isset($_POST['post'])) {
            $post = new Post();
            $post->id = $id;

            if($post->validate("post")) {
                $post->save();

                redirect('posts/edit/' . $id);
            }
        }

		$this->render('posts/edit', $data);
	}

	public function build() {
        if(isset($_POST['post'])) {
            $post = new Post();

            if($post->validate("post")) {
                $hid = $post->save();

                redirect('posts/edit/' . $hid);
            }
        }

		$this->render('posts/build');
	}

	public function destroy($id = 0) {
        $this->post->remove($id);

        redirect('posts');
	}
}