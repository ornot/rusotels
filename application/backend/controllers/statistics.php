<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Statistics extends RO_Controller {

    public function setRules() {
        return array(
            '*' => 'admin',
            '#fallback' => '/login'
        );
    }

	public function index() {
		$data = array();
		$data['count_webinars'] = $this->cp_model->getCountWebinars();
		$data['count_hotels_all'] = $this->cp_model->getCountHotels(0);
		$data['count_hotels_active'] = $this->cp_model->getCountHotels(1);

		$this->render('statistics_index_view', $data);
	}
}