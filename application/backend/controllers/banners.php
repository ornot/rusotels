<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banners extends RO_Controller {

    public function setRules() {
        return array(
            '*' => 'admin',
            '#fallback' => '/login'
        );
    }

	public function index() {

        $data['banners'] = $this->banner->findAll();

        $this->render("banners/index", $data);
	}


	public function edit($id) {

        if(isset($_POST['banner'])) {
            $banner = new Banner();

            if($banner->validate("banner")) {
                $banner->id = $id;
                $banner->save();

                redirect('banners/edit/' . $id);
            }
        }

        $data['banner'] = $this->banner->find(array("id" => $id));

        $this->render("banners/edit", $data);
    }

    public function add() {

        if(isset($_POST['banner'])) {
            $banner = new Banner();

            if($banner->validate("page")) {
                $banner->save();
                redirect('banners/index');
            }
        }

        $this->render("banners/add");
    }

    public function remove($id) {
        $this->banner->remove($id);

        redirect('banners/index');
    }

}