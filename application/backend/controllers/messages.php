<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Messages extends RO_Controller {

    public function setRules() {
        return array(
            '*' => 'admin',
            '#fallback' => '/login'
        );
    }

	public function index() {

        $data['messages'] = $this->message->findAll();
        foreach($data['messages'] as &$message) {
            $message['user'] = $this->user->find(array('id' => $message['user']));
        }

        $this->render("messages/index", $data);
	}


    public function remove($id) {
        $this->message->remove($id);

        redirect('messages/index');
    }

}