<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Teachers extends CI_Controller {


	public function index()
	{	
		$data['teachers'] = $this->cp_model->getAllTeachers();
		$this->load->view('teachers_index_view', $data);
	}


	public function show($id=0)
	{	
		$data['teacher'] = $this->cp_model->getTeacherById($id);
		$this->load->view('teachers_show_view', $data);
	}


	public function edit($id=0)
	{	
		$data['teacher'] = $this->cp_model->getTeacherById($id);
		$this->load->view('teachers_edit_view', $data);
	}


	public function update()
	{	
		if (!empty($_POST["firstname"]))
		{
			$id = $_POST['id'];
			$alias = form2db($_POST['alias']);
			$firstname = form2db($_POST['firstname']);
			$lastname = form2db($_POST['lastname']);
			$middlename = form2db($_POST['middlename']);
			$email = form2db($_POST['email']);
			//$photo_small = $_POST['photo_small'];
			//$photo_big = $_POST['photo_big'];
			$content = form2db($_POST['content']);
			$content_short = form2db($_POST['content_short']);
			$position = form2db($_POST['position']);
			$this->cp_model->updateTeacher($id, $alias, $firstname, $lastname, $middlename, $email, $content, $content_short, $position);

			//echo "good update";
			$data['alert_title']='Готово';
			$data['alert_text']='Данные успешно обновлены';
			$this->load->view('alert_green_view', $data);
		}
		else
		{
			echo "Error: zapolneni ne vse polya (zapolnite imya lektora)";
		}
	}


	public function build() //new (можно make)
	{	
		$data = '';
		$this->load->view('teachers_build_view', $data);
	}


	public function create()
	{
		if (!empty($_POST["firstname"]))
		{
			if (! empty ($_POST['alias'])) { $alias = form2db($_POST['alias']); } else { $alias = substr(md5(rand()), 0, 8); }
			$firstname = form2db($_POST['firstname']);
			$lastname = form2db($_POST['lastname']);
			$middlename = form2db($_POST['middlename']);
			$email = form2db($_POST['email']);
			$photo_small = '';	//$photo_small = $_POST['photo_small'];
			$photo_big = '';	//$photo_big = $_POST['photo_big'];
			$content = form2db($_POST['content']);
			$content_short = form2db($_POST['content_short']);
			if (! empty ($_POST['position'])) { $position = $_POST['position']; } else { $position=999; }

			$this->cp_model->addTeacher($alias, $firstname, $lastname, $middlename, $email, $photo_small, $photo_big, $content, $content_short, $position);

			$url = base_url() . "teachers";
			redirect($url, 'refresh');
		}
		else
		{
			echo "Error: zapolnite imya lektora";
		}
	}


	public function destroy($id=0, $tr=0)
	{	
		if ($tr==0)
		{
			$data = '';
			$data['id'] = $id;
			$this->load->view('teachers_destroy_view', $data);
		}
		else
		{
			$this->cp_model->deleteTeacher($id);
			
			$url = base_url() . "teachers";
			redirect($url, 'refresh');
		}
	}



	public function search()
	{	
		//$this->load->view('ex_view');
		$q = $_GET['q'];
	}


}

/* End of file citys.php */
/* Location: ./application/controllers/citys.php */