<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hotels extends RO_Controller {

    public function setRules() {
        return array(
            '*' => 'admin',
            '#fallback' => '/login'
        );
    }

    public function clients($page = 1) {
        $this->load->library('pagination');

        $filter = array('published' => 1, 'is_client' => 1);

        if(isset($_GET['search'])) {
            $filter['%title'] = $_GET['search'];
        }

        if(isset($_GET['city']) && $_GET['city'] > 0) {
            $filter['city_id'] = $_GET['city'];
        } else {
            if(isset($_GET['region']) && $_GET['region'] > 0) {
                $cities = $this->city->findAll(array('region_id' => $_GET['region']));
                $filter['@city_id'] = array_map(function($e) {
                    return $e['id'];
                }, $cities);

                $data['cities'] = $cities;
            }
        }


        $total = $this->hotel->count(array('published' => 1, 'is_client' => 1));


        $data['total'] = array(
            "clients" => $total,
            "active" => $this->hotel->count(array('published ' => 1, 'is_client' => 0)),
            "other" => $this->hotel->count(array('published !=' => 1))
        );

        $data['hotels'] = $this->hotel->findAll($filter, array("start" => ($page-1) * 50, "count" => 50));

        $data['pages_total'] = $this->hotel->count($filter);
        $data['page'] = $page;

        $data['regions'] = $this->region->findAll();
        foreach($data['hotels'] as &$hotel) {
            $hotel['photos'] = $this->hotel_photo->findAll(array('hotel' => $hotel['id']));
            $hotel['city'] = $this->city->find(array('id' => $hotel['city_id']));
            $hotel['user'] = $this->user->find(array('id' => $hotel['user_id']));
        }

        $this->render('hotels/clients', $data);
    }
public function index($page = 1) {
        $this->load->library('pagination');

        $filter = array('published' => 1, 'is_client' => 0);

        if(isset($_POST['action']) && isset($_POST['hotels'])) {

            switch ($_POST['action']) {
                case "remove": {
                    foreach(array_keys($_POST['hotels']) as $hid) {
                        $this->hotel->remove($hid);
                    }
                }; break;

                case "unpublish": {
                    foreach(array_keys($_POST['hotels']) as $hid) {
                        $hotel = new Hotel();
                        $hotel->id = $hid;
                        $hotel->published = 0;
                        $hotel->save();
                    }
                }; break;

                case "make-client": {
                    foreach(array_keys($_POST['hotels']) as $hid) {
                        $hotel = new Hotel();
                        $hotel->id = $hid;
                        $hotel->is_client = 1;
                        $hotel->save();
                    }
                }; break;
            }

            redirect('/hotels/index');
        }

        if(isset($_GET['search'])) {
            $filter['%title'] = $_GET['search'];
        }

        if(isset($_GET['city']) && $_GET['city'] > 0) {
            $filter['city_id'] = $_GET['city'];
        } else {
            if(isset($_GET['region']) && $_GET['region'] > 0) {
                $cities = $this->city->findAll(array('region_id' => $_GET['region']));
                $filter['@city_id'] = array_map(function($e) {
                    return $e['id'];
                }, $cities);

                $data['cities'] = $cities;
            }
        }

        $total = $this->hotel->count($filter);

        $data['total'] = array(
            "clients" => $this->hotel->count(array('published ' => 1, "is_client" => 1)),
            "active" => $this->hotel->count(array('published' => 1, 'is_client' => 0)),
            "other" => $this->hotel->count(array('published !=' => 1))
        );
        $data['hotels'] = $this->hotel->findAll($filter, array("start" => ($page-1)*50, "count" => 50));
        $data['pages_total'] = $total;
        $data['page'] = $page;

        $data['regions'] = $this->region->findAll();
        foreach($data['hotels'] as &$hotel) {
            $hotel['photos'] = $this->hotel_photo->findAll(array('hotel' => $hotel['id']));
            $hotel['city'] = $this->city->find(array('id' => $hotel['city_id']));
            $hotel['user'] = $this->user->find(array('id' => $hotel['user_id']));
        }

		$this->render('hotels/index', $data);
	}

    public function others($page = 1) {
        $this->load->library('pagination');

        $filter = array('published !=' => 1);

        if(isset($_POST['action']) && isset($_POST['hotels'])) {

            switch ($_POST['action']) {
                case "remove": {
                    foreach(array_keys($_POST['hotels']) as $hid) {
                        $this->hotel->remove($hid);
                    }
                }; break;

                case "publish": {
                    foreach(array_keys($_POST['hotels']) as $hid) {
                        $hotel = new Hotel();
                        $hotel->id = $hid;
                        $hotel->published = 1;
                        $hotel->save(1);
                    }
                }; break;
            }

            redirect('/hotels/others');
        }

        if(isset($_GET['search'])) {
            $filter['%title'] = $_GET['search'];
        }

        if(isset($_GET['city']) && $_GET['city'] > 0) {
            $filter['city_id'] = $_GET['city'];
        } else {
            if(isset($_GET['region']) && $_GET['region'] > 0) {
                $cities = $this->city->findAll(array('region_id' => $_GET['region']));
                $filter['@city_id'] = array_map(function($e) {
                    return $e['id'];
                }, $cities);

                $data['cities'] = $cities;
            }
        }

        $data['total'] = array(
            "clients" => $this->hotel->count(array('published ' => 1, "is_client" => 1)),
            "other" => $this->hotel->count(array('published !=' => 1)),
            "active" => $this->hotel->count(array('published ' => 1, 'is_client' => 0))
        );
        $data['hotels'] = $this->hotel->findAll($filter, array("start" => $page, "count" => 50));
        $data['pages_total'] = $this->hotel->count($filter);
        $data['page'] = $page;

        $data['regions'] = $this->region->findAll();
        foreach($data['hotels'] as &$hotel) {
            $hotel['photos'] = $this->hotel_photo->findAll(array('hotel' => $hotel['id']));
            $hotel['city'] = $this->city->find(array('id' => $hotel['city_id']));
            $hotel['user'] = $this->user->find(array('id' => $hotel['user_id']));
        }

        $this->render('hotels/others', $data);
    }

	public function edit($id = 0) {
        if(isset($_POST['hotel'])) {
            $hotel = new Hotel();
            if($hotel->validate("hotel", "admin")) {
                $hotel->id = $id;
                $hotel->address = $_POST['addr']['street'] . "|" . $_POST['addr']['build'] . "|" . $_POST['addr']['section'];
                $hotel->phone = $_POST['phone']['code'] . "|" . $_POST['phone']['num'];

                $hid = $hotel->save(1);
                if(isset($_POST['hotel']['type']) && $hid) {
                    $hotel->add_types($_POST['hotel']['type'], $hid);
                }

                redirect('/hotels/edit/' . $id);
            }
        }

		$data['hotel'] = $this->hotel->find(array("id" => $id));
        foreach($this->hotel->get_type($data['hotel']['id']) as $type) {
            $data['hotel']['type'][$type['id']] = $type;
        }

        $data['hotel']['city'] = $this->city->find(array('id' => $data['hotel']['city_id']));

        if($data['hotel']['user_id'] != 0) {
            $data['user'] = $this->user->find(array("id" => $data['hotel']['user_id']));
        }

        $data['cities'] = $this->city->findAll(array(), array(), array('name', 'asc'));

        foreach($data['cities'] as &$city) {
            $city['region'] = $this->region->find(array('id' => $city['region_id']));
        }

        $data['types'] = $this->hotel_type->findAll(array("active" => 1));

		$this->render('hotels/edit_view', $data);
	}

	public function edit_img($id = 0) {
        if(isset($_FILES['userfile']) && $_FILES['userfile']['name']) {
            $photo_path = BASEPATH . "../upload/hotel_photos/";

            $new_name = $id . "_" . time();

            move_uploaded_file($_FILES['userfile']["tmp_name"], $photo_path . $new_name);

            $photo = new Hotel_Photo();
            $photo->hotel = $id;
            $photo->photo = $new_name;
            $photo->save();

            redirect('hotels/edit_img/' . $id);
        }

		$data['hotel'] = $this->hotel->find(array('id' => $id));
        $data['hotel']['photos'] = $this->hotel_photo->findAll(array('hotel' => $id));

		$this->render('hotels/edit_img_view', $data);
	}

    public function services($id) {

        $hotel =  $this->hotel->find(array('id' => $id));

        if(isset($_GET['remove'])) {
            $this->service->remove($_GET['remove']);
            redirect('/hotels/services/' . $id);
        }

        if(isset($_POST['service'])) {
            $service = new Service();
            $service->hotel = $id;
            $service->user = $hotel['user_id'];
            $service->type = $_POST['service'];

            if(!$this->service->find(array('hotel' => $id, 'status' => 1, 'type' => $_POST['service']))) {
                $service->status = 1;
                $service->start = time();
            } else {
                $service->status = 2;
            }

            $service->end = 2592000;
            $service->save();

            redirect('/hotels/services/' . $id);
        }

        $data['hotel'] = $hotel;
        $data['services'] = $this->service->findAll(array('hotel' => $id), array(), 'status ASC');

        $this->render('hotels/services', $data);
    }


	public function edit_req($id=0)
	{	
		$data['hotel'] = $this->hotel->find(array('id' => $id));
        $data['requisites'] = $this->requisite->find(array('user' => $data['hotel']['user_id']));
		$this->render('hotels/edit_req_view', $data);
	}

	public function build() {
        if(isset($_POST['hotel'])) {
           
            $hotel = new Hotel();
            if($hotel->validate("hotel", "admin")) 
            {
                $hotel->address = $_POST['addr']['street'] . "|" . $_POST['addr']['build'] . "|" . $_POST['addr']['section'];
                $hotel->phone = $_POST['phone']['code'] . "|" . $_POST['phone']['num'];


                if(!isset($_POST['hotel']['is_cliet'])) {
                    $hotel->is_client = 0;
                }

                $hid = $hotel->save();

                if(isset($_POST['hotel']['type']) && $hid) {
                    $hotel->add_types($_POST['hotel']['type'], $hid);
                }

                redirect('/hotels/edit/' . $hid);
            }
           
            
        }

        $data['cities'] = $this->city->findAll(array(), array(), array('name', 'asc'));

        foreach($data['cities'] as &$city) {
            $city['region'] = $this->region->find(array('id' => $city['region_id']));
        }

        $data['types'] = $this->hotel_type->findAll(array("active" => 1));

        $this->render('hotels/build_view', $data);
	}


	public function destroy($id=0, $tr=0)
	{	
		if ($tr==0)
		{
			$data = '';
			$data['id'] = $id;
			$this->render('hotels/destroy_view', $data);
		}
		else
		{
			$this->cp_model->deleteHotel($id);
			
			$url = base_url() . "hotels";
			redirect($url, 'refresh');
		}
	}

	public function destroy_img($id = 0) {
		$photo = $this->hotel_photo->find(array("id" => $id));
        $this->hotel_photo->remove($id);

		unlink( BASEPATH . "../upload/hotel_photos/".$photo['photo']);

		redirect("hotels/edit_img/". $photo['hotel']);
	}
}