<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Citys extends RO_Controller {

    public function setRules() {
        return array(
            '*' => 'admin',
            '#fallback' => '/login'
        );
    }


	public function index($page = 0) {
        $this->load->library('pagination');

        $config = array(
            'base_url' => '/citys/index',
            'total_rows' => $this->city->count(),
            'per_page' => '50',
            'full_tag_open' => '<div class="btn-group">',
            'full_tag_close' => '</div>',
            'num_tag_open' => '<button class="btn">',
            'num_tag_close' => '</button>',
            'cur_tag_open' => '<button class="btn">',
            'cur_tag_close' => '</button>',
            'prev_tag_open' => '<button class="btn">',
            'prev_tag_close' => '</button>',
            'next_tag_open' => '<button class="btn">',
            'next_tag_close' => '</button>',
            'first_tag_open' => '<button class="btn">',
            'first_tag_close' => '</button>',
            'last_tag_open' => '<button class="btn">',
            'last_tag_close' => '</button>',
        );

        $this->pagination->initialize($config);

		$data['citys'] = $this->city->findAll(array(), array('start' => $page, 'count' => 50), array('name', 'asc'));

        foreach($data['citys'] as &$city) {
            $city['region'] = $this->region->find(array('id' => $city['region_id']));
        }

        $data['pagination'] = $this->pagination->create_links();

		$this->render('citys/index', $data);
	}

	public function index_regions($region_id=0) {
		if ($region_id == '0')
		{
			$data['regions'] = $this->cp_model->getAllRegions();
			$this->render('citys/index_regions_view', $data);
		}
		else
		{
			$data['citys'] = $this->city->findAll(array('region_id' => $region_id));
            foreach($data['citys'] as &$city) {
                $city['region'] = $this->region->find(array('id' => $city['region_id']));
            }
			$this->render('citys/index_regions_citys_view', $data);
		}
		
	}

	public function edit($id) {
        if (isset($_POST["city"])) {

            $city = new City();

            if($city->validate('city')) {
                if(!isset($_POST['city']['major'])) {
                    $city->major = 0;
                }

                $city->id = $id;
                $city->save();
            }
        }

        $data['regions'] = $this->region->findAll();
		$data['city'] = $this->city->find(array("id" => $id));
		$this->render('citys/edit', $data);
	}

    public function add() {
        if (isset($_POST["city"])) {

            $city = new City();

            if($city->validate('city')) {

                if(!isset($_POST['city']['major'])) {
                    $city->major = 0;
                }

                $cid = $city->save();

                redirect('citys/edit/' . $cid);
            }
        }

        $data['regions'] = $this->region->findAll();
        $this->render('citys/add', $data);
    }

    public function remove($id) {
        $this->city->remove($id);

        redirect('/citys');
    }
}