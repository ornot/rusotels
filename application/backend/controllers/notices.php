<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notices extends RO_Controller {

    public function setRules() {
        return array(
            '*' => 'admin',
            '#fallback' => '/login'
        );
    }

    public function index($page = 1) {
        
  
        
        if (isset($_POST['action']) && isset($_POST['notice'])) {
          

            switch ($_POST['action']) {
                case "publish": {
                        foreach (array_keys($_POST['notice']) as $key) {
                            $this->confirm($key,false);
                        }
                    };
                    break;
                case "unpublish": {
                        foreach (array_keys($_POST['notice']) as $key) {
                           $this->reject($key,false);
                        }
                    };
                    break;
            }
                
          
        }

        $data['notices'] = $this->notice->findAll(array('status' => 1));

        foreach ($data['notices'] as &$notice) {
            $notice['hotel'] = $this->hotel->find(array("id" => $notice['hotel']));
            $notice['user'] = $this->user->find(array("id" => $notice['user']));
            if ($notice['data'] != "") {
                $notice['data'] = json_decode($notice['data']);
            }

            if ($notice['type'] == 102) {
                if (isset($notice['hotel']['id'])) {
                    $r_last = $this->hotel_revision->find(array('hotel' => $notice['hotel']['id']), array(), array('id', 'desc'));
                    $r_active = $this->hotel_revision->find(array('hotel' => $notice['hotel']['id'], "active" => 1));

                    $revision_fields = array("title", "content", "address", "email", "skype", "phone", "site", "room_count", "price_min", "price_max");

                    $notice['data'] = array();
                    foreach ($revision_fields as $field) {
                        if (trim($r_last[$field]) != trim($r_active[$field])) {
                            $notice['data'][$field] = array("old" => $r_active[$field], "new" => $r_last[$field]);
                        }
                    }
                }
            }
        }

        $this->render('notice/index', $data);
    }

    public function closed($page = 1) {
        $data['notices'] = $this->notice->findAll(array('status' => 0));
        foreach($data['notices'] as &$notice) {
            $notice['hotel'] = $this->hotel->find(array("id" => $notice['hotel']));
            $notice['user'] = $this->user->find(array("id" => $notice['user']));
            if($notice['data'] != "") {
                $notice['data'] = json_decode($notice['data']);
            }
        }
        
        
        if( isset($_POST['action'])&&isset($_POST['notice'])) {

            switch ($_POST['action']) {
                case "remove": {
                   foreach(array_keys($_POST['notice']) as $key ) {
                        $this->notice->remove($key); 
                   }  
                }; break;
            }
            redirect('/notices/closed');
            //return;
        }
              
        
        $this->render('notice/closed', $data);
    }

    public function remove($id) {
        $this->notice->remove($id);

        redirect('/notices');
    }

    public function confirm($id,$flag=true) {
        $notice = $this->notice->find(array('id' => $id));
        $notice['data'] = json_decode($notice['data']);
        $notice['user'] = $this->user->find(array("id" => $notice['user']));
        $notice['hotel'] = $this->hotel->find(array("id" => $notice['hotel']));
        $notice['hotel']['city'] = $this->city->find(array("id" => $notice['hotel']['city_id']));
        $hotel_id=$notice['hotel']['id'];
   
        switch($notice['type']) {
            case 101: {

                $service = new Service();
                $service->hotel = $notice['hotel']['id'];
                $service->user = $notice['user']['id'];
                $service->type = 1;

                if(!$this->service->find(array('hotel' => $notice['hotel']['id'], 'status' => 1))) {
                    $service->status = 1;
                    $service->start = time();
                } else {
                    $service->status = 2;
                }

                $service->end = 2592000 * $notice['data']->period;
                $service->save();

                $this->email->clear();

                $this->email->to($notice['user']['email']);
                $this->email->from('system@rusotels.ru');
                $this->email->subject('Услуга «Размещение на RUSOTEL.RU с подключением booking  engine на страницу каталога и на сайт отеля» подключена и работает.');
                $this->email->message("Уважаемый отельер! \n
                                        Вам подключена услуга «Размещение на RUSOTEL.RU с подключением booking  engine на страницу каталога и на сайт отеля» на срок  «" . $notice['data']->period . " месяцев» \n
                                        Надеемся на успешное сотрудничество и желаем Вашему бизнесу успехов и процветания! \n
                                        С уважением, команда Rusotels.ru");
                $this->email->send();

                $rpc = new BaseJsonRpcClient('http://www.litebooking.ru/api');
                $res = $rpc->register(array(
                    "email" => $notice['user']['email'],
                    "booking_url" => "http://rusotels.ru/hotels/" . $notice['hotel']['city']['alias'] . "/" . $notice['hotel']['alias'],
                    "cancelation_url" => "http://rusotels.ru/",
                    "phone" => isset($notice['hotel']['phone']) ? f_phone($notice['hotel']['phone']) : "",
                    "site_url" => "http://rusotels.ru/hotels/" . $notice['hotel']['city']['alias'] . "/" . $notice['hotel']['alias'],
                    "id" => $notice['hotel']['lb_id'],
                    "key" => "lE1iwFw1XEQs",
                ));

            }; break;

            case 102: {
                $r_last = $this->hotel_revision->find(array('hotel' => $notice['hotel']['id']), array(), array('id', 'desc'));
                $r_active = $this->hotel_revision->find(array('hotel' => $notice['hotel']['id'], "active" => 1));


                $r_a = new Hotel_Revision();
                $r_a->id = $r_active['id'];
                $r_a->active = 0;
                $r_a->save();

                $r_l = new Hotel_Revision();
                $r_l->id = $r_last['id'];
                $r_l->active = 1;
                $r_l->save();
                 //удаляем все тестовые ревизии отеля
                $this->hotel_revision->del_all_passive($hotel_id);
                

            }; break;

            case 103: {
                $hotel = new Hotel();
                $hotel->id = $notice['hotel']['id'];
                $hotel->certificate_status = 1;

                $hotel->save(1);

                $this->email->clear();

                $this->email->to($notice['user']['email']);
                $this->email->from('system@rusotels.ru');
                $this->email->subject(' Ваш Сертификат одобрен модератором на RUSOTEL.RU');
                $this->email->message("Уважаемый Отльер! \n
                                        Ваш Сертификат одобрен модератором \n
                                        Надеемся на успешное сотрудничество и желаем Вашему бизнесу успехов и процветания! \n\n
                                        С уважением, команда Rusotels.ru");
                $this->email->send();

            }; break;

            case 104: {
                $photo = new Hotel_Photo();
                $photo->id = $notice['data']->photo;
                $photo->status = 1;
                $photo->save();
            }; break;

            case 109: {
                $hotel = new Hotel();
                $hotel->id = $notice['hotel']['id'];
                $hotel->weight = time();

                $hotel->save(1);

                $this->email->clear();

                $this->email->to($notice['user']['email']);
                $this->email->from('system@rusotels.ru');
                $this->email->subject('Услуга размещения баннера на портале Rusotels.ru одобрена модератором.');
                $this->email->message("Уважаемый отельер!\n
                                       \n
                                       Благодарим Вас за покупку услуги 'Поднятие отеля наверх в каталоге в выдаче по своему городу' на сайте Rusotels.ru\n
                                       Ваш отель находится на 1 месте.\n
                                       С уважением, команда Rusotels.ru\n");
                $this->email->send();

            }; break;

            case 110: {
                $service = new Service();
                $service->hotel = $notice['hotel']['id'];
                $service->user = $notice['user']['id'];
                $service->type = 10;

                $service->status = 1;
                $service->start = time();

                $service->end = 2592000;
                $service->save();

                $this->email->clear();

                $this->email->to($notice['user']['email']);
                $this->email->from('system@rusotels.ru');
                $this->email->subject('Услуга размещения баннера на портале Rusotels.ru одобрена модератором.');
                $this->email->message("Уважаемый отельер! \n
                                    \n
                                    Благодарим Вас за покупку услуги 'Размещение баннера' на сайте Rusotels.ru\n
                                    Ваш баннер размещен на срок 1 месяц и находится на главной странице.\n
                                    С уважением, команда Rusotels.ru\n");
                $this->email->send();

            }; break;

            case 201: {
                $hotel = new Hotel();
                $hotel->id = $notice['hotel']['id'];
                $hotel->published = 1;

                $hotel->save(1);

                $this->email->clear();

                $this->email->to($notice['user']['email']);
                $this->email->from('system@rusotels.ru');
                $this->email->subject('Регистрация на портале Rusotels.ru одобрена модератором.');
                $this->email->message("Уважаемый отельер! \n
                                        Ваша заявка на размещение «" . $notice['id'] . "» на портале Rusotels.ru одобрена модератором.  Информация о Вас размещена в каталоге. \n
                                        \n
                                        Страница вашего отеля находится по адресу:  http://rusotels.ru/hotels/" . $notice['hotel']['city']['alias'] . "/" . $notice['hotel']['alias'] . " \n
                                        В личном кабинете Вам доступно: управление аккаунтом, платными услугами, регистрационными данными и прочими опциями. \n
                                        Надеемся на успешное сотрудничество и желаем Вашему бизнесу успехов и процветания! \n
                                        С уважением, команда Rusotels.ru");
                $this->email->send();

                $hotel = $this->hotel->find(array('id' => $notice['hotel']['id']));

                $data = array();
                $data['title'] = $hotel['title'];
                $data['description'] = $hotel['content'];
                $data['stars'] = intval($hotel['rank']);
                $data['total_rooms'] = intval($hotel['room_count']);

                $types = $this->hotel->get_type($hotel['id']);
                $data['types'] = array();
                foreach($types as $item) {
                    $data['types'][] = $item['type'];
                }

                $city = $this->city->find(array('id' => $hotel['city_id']));
                $data['city'] = array();
                $data['city']['name'] = $city['name'];

                $region = $this->region->find(array('id' => $city['region_id']));
                $data['city']['region'] = $region['name'];

                $photos = $this->hotel_photo->findAll(array('hotel' => $hotel['id']));
                $data['photos'] = array();
                foreach($photos as $photo) {
                    $data['photos'][] = 'http://rusotels.ru/cache/thumbs/hotels/113x72/' . $photo['photo'];
                }

                $ch = curl_init('http://api.litebooking.ru/hotel');
                curl_setopt_array($ch, array(
                    CURLOPT_POST => TRUE,
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_HTTPHEADER => array(
                        'Content-Type: application/json'
                    ),
                    CURLOPT_POSTFIELDS => json_encode($data)
                ));

                $response = curl_exec($ch);

                if($response === FALSE){
                    echo curl_error($ch);
                } else {

                    $res = json_decode($response);
                    if(isset($res->id)) {
                        $h = new Hotel();
                        $h->id = $hotel['id'];
                        $h->lb_id = $res->id;
                        $h->save();
                    }
                }
                
                //удаляем все тестовые ревизии отеля
                $this->hotel_revision->del_all_passive($hotel_id);

            }; break;
        }

        $notice = new Notice();
        $notice->id = $id;
        $notice->status = 0;
        $notice->processed = time();
        $notice->save();

        if($flag){
            redirect('/notices');    
        }
    }

    public function reject($id,$flag=true) {

        $notice = $this->notice->find(array('id' => $id));
        $notice['user'] = $this->user->find(array("id" => $notice['user']));
        $notice['data'] = json_decode($notice['data']);
        $hotel_id=$notice['hotel'];

        switch($notice['type']) {

            case 102: {
                //$r_last = $this->hotel_revision->find(array('hotel' => $notice['hotel'], 'active' => 0), array(), array('id', 'desc'));
                //$this->hotel_revision->remove($r_last['id']);
                 //удаляем все тестовые ревизии отеля
                $this->hotel_revision->del_all_passive($hotel_id);
            }; break;

            case 103: {
                $h = $this->hotel->find(array('id' => $notice['hotel']));

                $hotel = new Hotel();
                $hotel->id = $notice['hotel'];
                $hotel->certificate = "";
                $hotel->save();

                unlink(BASEPATH . '../upload/certificates/' . $h['certificate']);

                $this->email->clear();

                $this->email->to($notice['user']['email']);
                $this->email->from('system@rusotels.ru');
                $this->email->subject('Загруженный вами сертификат отклонен модератором.');
                $this->email->message("Уважаемый отельер! \n
                                        Загруженный вами сертификат отклонен модератором по причине не соответствия данных. \n
                                        Просим Вас еще раз проверить данные в личном кабинете! \n
                                        С уважением, команда Rusotels.ru");
                $this->email->send();

            }; break;

            case 104: {
                $photo = $this->hotel_photo->find(array("id" => $notice['data']->photo));
                if($photo) {
                    unlink(BASEPATH . '../upload/hotel_photos/' . $photo['photo']);

                    $this->hotel_photo->remove($notice['data']->photo);
                }
            }; break;

            case 201: {
                $this->email->clear();

                $this->email->to($notice['user']['email']);
                $this->email->from('system@rusotels.ru');
                $this->email->subject('Ваша регистрация на портале Rusotels.ru отклонена модератором.');
                $this->email->message("Уважаемый отельер! \n
                                        Ваша заявка на размещение «" . $notice['id'] ."» на портале Rusotels.ru отклонена модератором. \n
                                        Одной из причин может быть: \n
                                        Неверно указан адрес сайта \n
                                        Неверно указан номер телефона, e-mail \n
                                        Неверно указано количество звезд \n
                                        Некорректное описание \n
                                        Неверно указано количество номеров \n
                                        Просим Вас еще раз проверить данные в личном кабинете! \n
                                        С уважением, команда Rusotels.ru");
                $this->email->send();

            }; break;

        }

        $notice = new Notice();
        $notice->id = $id;
        $notice->status = 0;
        $notice->processed = time();
        $notice->save();

        if($flag){
            redirect('/notices');
            
        }
    }
}