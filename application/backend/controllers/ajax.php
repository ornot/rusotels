<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends RO_Controller {

    public function setRules() {
        return array(
            '*' => 'admin',
            '#fallback' => '/login'
        );
    }

	public function city($region) {
        $this->db->select('id, name');
		$citys = $this->city->findAll(array('region_id' => $region));

        die(json_encode($citys));
	}
}