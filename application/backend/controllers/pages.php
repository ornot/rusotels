<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends RO_Controller {

    public function setRules() {
        return array(
            '*' => 'admin',
            '#fallback' => '/login'
        );
    }

	public function index() {

        $data['pages'] = $this->page->findAll();

        $this->render("pages/index", $data);
	}


	public function edit($id) {

        if(isset($_POST['page'])) {
            $page = new Page();

            if($page->validate("page")) {
                $page->id = $id;
                $page->save();

                redirect('pages/edit/' . $id);
            }
        }

        $data['page'] = $this->page->find(array("id" => $id));

        $this->render("pages/edit", $data);
    }

    public function add() {

        if(isset($_POST['page'])) {
            $page = new Page();

            if($page->validate("page")) {
                $page->save();
                redirect('pages/index');
            }
        }

        $this->render("pages/add");
    }

    public function remove($id) {
        $this->page->remove($id);

        redirect('pages/index');
    }

}