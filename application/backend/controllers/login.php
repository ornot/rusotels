<?php

class Login extends RO_Controller {

    protected $layout = 'layout/login';

    public function setRules() {
        return array(
            '*' => 'guest',
            '#fallback' => '/hotels',
        );
    }

    public function index() {

        if(isset($_POST['email']) && isset($_POST['password'])) {
            $user = $this->user->find(array('email' => $_POST['email'], 'password' => md5($_POST['password'])));

            if($user && $user['id']) {
                $_SESSION['user'] = $user;
            }

            redirect('/hotels');
        }

        $this->render('login');
    }
}