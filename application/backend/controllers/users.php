<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends RO_Controller {

    public function setRules() {
        return array(
            '*' => 'admin',
            '#fallback' => '/login'
        );
    }

    public function index() {

        $data['users'] = $this->user->findAll();
        foreach($data['users'] as &$user) {
            $user['hotel'] = $this->hotel->find(array('user_id' => $user['id']));
        }

        $this->render("users/index", $data);
    }


    public function edit($id) {

        if(isset($_POST['user'])) {
            $user = new User();

            if($user->validate("user", "update_admin")) {
                $user->id = $id;
                $user->save();

                redirect('users/edit/' . $id);
            }
        }

        $data['user'] = $this->user->find(array("id" => $id));

        $this->render("users/edit", $data);
    }

    public function generate($hotel_id) {
        $hotel = $this->hotel->find(array("id" => $hotel_id));

        $u = $this->user->find(array("email" => $hotel['email']));

        if(!$u) {
            $user = new User();
            $user->email = $hotel['email'];

            $token = md5(time());
            $user->token = $token;

            $uid = $user->save();

            $h = new Hotel();
            $h->id = $hotel['id'];
            $h->user_id = $uid;
            $h->save();
        }


        redirect('hotels/edit/' . $hotel_id);
    }

    public function remove($id) {
        $this->user->remove($id);

        redirect('users/index');
    }
}

/* End of file citys.php */
/* Location: ./application/controllers/citys.php */